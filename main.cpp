#include <QCoreApplication>
#include <QDebug>
#include "ircbots/irctipbot/irctipbotsystem.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
	if (!IrcTipbotSystem::instance().start(&a))
	{
		qDebug() << "Irc Tipbot System failed to start";
		//return 1;
	}
	return a.exec();
}
