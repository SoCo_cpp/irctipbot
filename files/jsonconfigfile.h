#ifndef JSONCONFIGFILE_H
#define JSONCONFIGFILE_H

#include <QObject>
#include <QJsonDocument>

class JsonConfigFile : public QObject
{
	Q_OBJECT
public:
	explicit JsonConfigFile(QObject *parent = 0, const QString& sFileName = QString(), bool bCompactJson = true);
	JsonConfigFile(const JsonConfigFile& cpy);
	JsonConfigFile& operator =(const JsonConfigFile& cpy);
	void copy(const JsonConfigFile& cpy);

	QJsonDocument& json();
	const QJsonDocument& json() const;

	void setFileName(const QString& sFileName);
	const QString& fileName() const;

	void setCompactJson(bool bCompact = true);
	bool compactJson() const;

	bool exists() const;
	bool save(bool bClearOnSuccess = true);
	bool load();

	void clear();

protected:
	QJsonDocument _jsonDoc;
	QString _fileName;
	bool _compactJson;

signals:

public slots:

};

#endif // JSONCONFIGFILE_H
