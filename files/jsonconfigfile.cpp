#include "files/jsonconfigfile.h"
#include "logging/debuglogger.h"
#include <QFile>

JsonConfigFile::JsonConfigFile(QObject *parent /*= 0*/, const QString& sFileName /*= QString()*/, bool bCompactJson /*= true*/) :
	QObject(parent),
	_fileName(sFileName),
	_compactJson(bCompactJson)
{
}

JsonConfigFile::JsonConfigFile(const JsonConfigFile& cpy) :
	QObject(0)
{
	copy(cpy);
}

JsonConfigFile& JsonConfigFile::operator =(const JsonConfigFile& cpy)
{
	copy(cpy);
	return *this;
}

void JsonConfigFile::copy(const JsonConfigFile& cpy)
{
	_fileName		= cpy._fileName;
	_compactJson	= cpy._compactJson;
	_jsonDoc		= cpy._jsonDoc;
}

QJsonDocument& JsonConfigFile::json()
{
	return _jsonDoc;
}

const QJsonDocument& JsonConfigFile::json() const
{
	return _jsonDoc;
}

void JsonConfigFile::setFileName(const QString& sFileName)
{
	_fileName = sFileName;
}

const QString& JsonConfigFile::fileName() const
{
	return _fileName;
}

void JsonConfigFile::setCompactJson(bool bCompact /*= true*/)
{
	_compactJson = bCompact;
}

bool JsonConfigFile::compactJson() const
{
	return _compactJson;
}

bool JsonConfigFile::exists() const
{
	QFile file;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonConfigFile::exists file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	return file.exists();
}

bool JsonConfigFile::save(bool bClearOnSuccess /*= true*/)
{
	QFile file;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonConfigFile::save file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
	{
		DebugLog(dlWarn, "JsonConfigFile::save open file for writing failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	if (file.write(_jsonDoc.toJson( (_compactJson ? QJsonDocument::Compact : QJsonDocument::Indented) )) == -1)
	{
		DebugLog(dlWarn, "JsonConfigFile::save ile write failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	if (bClearOnSuccess)
		clear();
	return true;
}

bool JsonConfigFile::load()
{
	QFile file;
	QJsonParseError jsonParseError;
	if (_fileName.isEmpty())
	{
		DebugLog(dlWarn, "JsonConfigFile::load file name is empty");
		return false;
	}
	file.setFileName(_fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		DebugLog(dlWarn, "JsonConfigFile::load open file for reading failed. Error: %s, File name: %s", qPrintable(file.errorString()), qPrintable(_fileName));
		return false;
	}
	_jsonDoc = QJsonDocument::fromJson(file.readAll(), &jsonParseError);
	if (jsonParseError.error != QJsonParseError::NoError)
	{
		DebugLog(dlWarn, "JsonConfigFile::load parse json file failed: %s File name: %s", qPrintable(jsonParseError.errorString()), qPrintable(_fileName));
		return false;
	}
	return true;
}

void JsonConfigFile::clear()
{
	_jsonDoc = QJsonDocument();
}
