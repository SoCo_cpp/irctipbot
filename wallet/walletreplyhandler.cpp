#include "wallet/walletreplyhandler.h"
#include "logging/debuglogger.h"

WalletReplyHandler::WalletReplyHandler(QObject *parent, CoinWalletClient& walletClient) :
	QObject(parent),
	_walletClient(walletClient)
{
}

CoinWalletRequest* WalletReplyHandler::addWalletRequest(int requestType)
{
	CoinWalletRequest* pRequest = new CoinWalletRequest(this, _walletClient);
	pRequest->params().append(QVariant(requestType));
	connect(pRequest, SIGNAL(finished(bool, CoinWalletRequest*)), this, SLOT(walletRequestFinished(bool, CoinWalletRequest*)));
	return pRequest;
}

void WalletReplyHandler::walletRequestFinished(bool success, CoinWalletRequest* pRequest)
{
	Q_ASSERT(pRequest);
	if (!pRequest->params().size())
		DebugLog(dlError, "WalletReplyHandler::walletRequestFinished  missing request type parameter");
	else
	{
		int requestType = pRequest->params().at(0).toInt();
		if (!handleWalletRequestReply(requestType, success, pRequest))
			DebugLog(dlDebug, "WalletReplyHandler::walletRequestFinished failed handleWalletRequestReply for request type: %d", requestType);
	}
	pRequest->deleteLater();
}
