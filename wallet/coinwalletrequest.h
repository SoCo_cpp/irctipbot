#ifndef COINWALLETREQUEST_H
#define COINWALLETREQUEST_H

#include <QObject>
#include <QJsonValue>
#include <QUuid>
#include <QVariantList>
#include "networking/httpjsonclient.h"
#include "wallet/coinwalletapi.h"

class CoinWalletClient; // predeclared
class CoinWalletRequest : public QObject
{
	Q_OBJECT
public:
	explicit CoinWalletRequest(QObject *parent, CoinWalletClient& coinWalletClient);
	CoinWalletRequest(const CoinWalletRequest& cpy);
	CoinWalletRequest& operator =(const CoinWalletRequest& cpy);
	void copy(const CoinWalletRequest& cpy);

	const CoinWalletClient& client() const;
	CoinWalletClient& client();

	const QUuid& id() const;

	const HttpJsonClientReply& jsonClientReply() const;
	HttpJsonClientReply& jsonClientReply();

	const QString& rpcString();
	void setParams(const QVariantList& params);
	const QVariantList& params() const;
	QVariantList& params();

	bool isDone() const;
	bool isError() const;

	void setErrorString(const QString& errorString);
	const QString& errorString() const;

	QJsonValue jsonValue() const;
	QString dumpJsonValue(const QString& name = "result") const;

	void setFailed(const QString& errorString);

	void rawRequest(const QString& rpcString);

	void dumpPrivKey(const QString& sAddress);
	void importPrivKey(const QString& privKey, const QString& label, bool rescan = true);

	void getAccount(const QString& address);
	void getAccountAddress(const QString& account);
	void getAccountAddresses(const QString& account);
	void moveAccount(const QString& fromAccount, const QString& toAccount, double amount, const QString& comment = QString(), int minConf = -1);
	void listAccounts(int minConf = -1);
	void setAccount(const QString& address, const QString& account);

	void decryptWallet(const QString& passphrase);
	void encryptWallet(const QString& passphrase);

	void getBalance(const QString& account = QString(), int minConf = -1);
	void getInfo();
	void getConnectionCount();
	void getDifficulty();
	void getTransaction(const QString& txid);
	void setTxFee(double value);
	void validateAddress(const QString& sAddress);
	void stopWalletServer();

	void getReceivedByAccount(const QString& account, int minConf = -1);
	void listReceivedByAccount(int minConf = -1, bool includeEmpty = false);
	void listReceivedByAddress(int minConf = -1, bool includeEmpty = false);
	void listTransactions(const QString& sAccount = QString(), int iCount = -1, int iSkipCount = -1);

	void getNewAddress(const QString& account = QString());

	void sendToAddress(const QString& toAddress, double amount, const QString& comment = QString(), const QString& commentTo = QString());

	void signMessage(const QString& address, const QString& message);
	void verifyMessage(const QString& address, const QString& signature, const QString& message);

protected:
	CoinWalletClient& _coinWalletClient;
	QUuid _id;
	bool _done;
	QString _rpcString;
	QVariantList _params;

	HttpJsonClientReply _jsonClientReply;
	CoinWalletAPI _api;

	bool isValidReply();

signals:
	void setRequest(QString rpcString);
	void finished(bool success, CoinWalletRequest* pRequest);

public slots:
	void replyReady(HttpJsonClientReply* preply, bool success);

};

#endif // COINWALLETREQUEST_H
