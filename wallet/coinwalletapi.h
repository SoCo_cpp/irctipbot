#ifndef COINWALLETAPI_H
#define COINWALLETAPI_H

#include <QObject>
#include <QList>
#include <QVariant>
#include <QJsonObject>
#include <QJsonArray>

class CoinWalletAPI : public QObject
{
	Q_OBJECT
public:
	explicit CoinWalletAPI(QObject *parent = 0);


	QString buildAPI(const QString& sId, const QString& sMethod, const QString& sArgs = QString(), const QString& sSplitDelimiter = QString());
	QString buildAPI(const QString& sId, const QString& sMethod, const QStringList& slArgs);
	QString buildAPI(const QString& sId, const QString& sMethod, const QVariantList& vlArgs);
	QString buildAPI(const QString& sId, const QString& sMethod, const QJsonArray& jsonArgs);

	QString dumpPrivKey(const QString& sId, const QString& address);
	QString importPrivKey(const QString& sId, const QString& privKey, const QString& label, bool rescan = true);

	QString getAccount(const QString& sId, const QString& address);
	QString getAccountAddress(const QString& sId, const QString& account);
	QString getAccountAddresses(const QString& sId, const QString& account);
	QString moveAccount(const QString& sId, const QString& fromAccount, const QString& toAccount, double amount, const QString& comment = QString(), int minConf = -1);
	QString listAccounts(const QString& sId, int minConf = -1);
	QString setAccount(const QString& sId, const QString& address, const QString& account);

	QString decryptWallet(const QString& sId, const QString& passphrase);
	QString encryptWallet(const QString& sId, const QString& passphrase);

	QString getBalance(const QString& sId, const QString& account = QString(), int minConf = -1);
	QString getInfo(const QString& sId);
	QString getConnectionCount(const QString& sId);
	QString getDifficulty(const QString& sId);
	QString getTransaction(const QString& sId, const QString& txid);
	QString setTxFee(const QString& sId, double value);
	QString validateAddress(const QString& sId, const QString& address);
	QString stopWalletServer(const QString& sId);


	QString getReceivedByAccount(const QString& sId, const QString& account, int minConf = -1);
	QString listReceivedByAccount(const QString& sId, int minConf = -1, bool includeEmpty = false);
	QString listReceivedByAddress(const QString& sId, int minConf = -1, bool includeEmpty = false);
	QString listTransactions(const QString& sId, const QString& account = QString(), int count = -1, int skipCount = -1);

	QString getNewAddress(const QString& sId, const QString& account = QString());

	QString sendMany(const QString& sId, const QString& fromAccount, const QMap<QString, double>& addressValueMap, const QString& comment = QString(), int minConf = 1);
	QString sendToAddress(const QString& sId, const QString& toAddress, double amount, const QString& comment = QString(), const QString& commentTo = QString());

	QString signMessage(const QString& sId, const QString& address, const QString& message);
	QString verifyMessage(const QString& sId, const QString& address, const QString& signature, const QString& message);

signals:

public slots:

};

#endif // COINWALLETAPI_H
