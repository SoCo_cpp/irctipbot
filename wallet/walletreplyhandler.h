#ifndef WALLETREPLYHANDLER_H
#define WALLETREPLYHANDLER_H

#include <QObject>
#include "wallet/coinwalletclient.h"
#include "wallet/coinwalletrequest.h"

class WalletReplyHandler : public QObject
{
	Q_OBJECT
public:
	explicit WalletReplyHandler(QObject *parent, CoinWalletClient& walletClient);

	CoinWalletRequest* addWalletRequest(int requestType);

protected:
	CoinWalletClient& _walletClient;
	virtual bool handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest) = 0;

signals:

public slots:
	void walletRequestFinished(bool success, CoinWalletRequest* pRequest);

};

#endif // WALLETREPLYHANDLER_H
