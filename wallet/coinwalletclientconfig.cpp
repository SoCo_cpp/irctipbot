#include "wallet/coinwalletclientconfig.h"
#include "logging/debuglogger.h"

CoinWalletClientConfig::CoinWalletClientConfig(QObject *parent) :
	ProxyServerConfig(parent)
{
}

CoinWalletClientConfig::CoinWalletClientConfig(const CoinWalletClientConfig& cpy) :
	ProxyServerConfig(0)
{
	copy(cpy, true /*noChangeSignal*/);
}

CoinWalletClientConfig& CoinWalletClientConfig::operator =(const CoinWalletClientConfig& cpy)
{
	copy(cpy);
	return *this;
}

void CoinWalletClientConfig::copy(const CoinWalletClientConfig& cpy, bool noChangeSignal /*= false*/)
{
	ProxyServerConfig::copy( (*dynamic_cast<const ProxyServerConfig*>(&cpy)), true/*noChangeSignal*/);
	if (!noChangeSignal)
		emit changed();
}
