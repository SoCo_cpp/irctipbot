#ifndef COINWALLETCLIENT_H
#define COINWALLETCLIENT_H

#include <QObject>
#include <QList>
#include "networking/httpjsonclient.h"
#include "wallet/coinwalletclientconfig.h"
#include "networking/httpjsonclientreply.h"
#include "wallet/coinwalletrequest.h"
#include "wallet/coinwalletrequest.h"

class CoinWalletClient : public QObject
{
	Q_OBJECT
public:
	explicit CoinWalletClient(QObject *parent = 0);

	static QString dumpJSONObject(const QString& name, const QJsonObject& jsonObj, int indent = 0);
	static QString dumpJSONValue(const QString& name, const QJsonValue& jsonValue, int indent = 0);
	static QString dumpJSONArray(const QString& name, const QJsonArray& jsonArray, int indent = 0);

	void setConfig(const CoinWalletClientConfig& cfg);
	const CoinWalletClientConfig& config() const;
	CoinWalletClientConfig& config();

protected:

	HttpJsonClient _jsonClient;
	CoinWalletClientConfig _config;

signals:

public slots:

	void request(CoinWalletRequest& walletRequest);
	void configChanged();
};

#endif // COINWALLETCLIENT_H
