#include "wallet/coinwalletrequest.h"
#include "wallet/coinwalletclient.h"
#include "logging/debuglogger.h"

CoinWalletRequest::CoinWalletRequest(QObject *parent, CoinWalletClient& coinWalletClient) :
	QObject(parent),
	_coinWalletClient(coinWalletClient),
	_id(QUuid::createUuid()),
	_done(false)
{
	_jsonClientReply.setScrubURLErrors(true); // scrube URL with simple auth from error messages
	connect(&_jsonClientReply, SIGNAL(finished(HttpJsonClientReply*,bool)), this, SLOT(replyReady(HttpJsonClientReply*, bool)));
}

CoinWalletRequest::CoinWalletRequest(const CoinWalletRequest& cpy) :
	QObject(cpy.parent()),
	_coinWalletClient(cpy._coinWalletClient)
{
	copy(cpy);
}

CoinWalletRequest& CoinWalletRequest::operator =(const CoinWalletRequest& cpy)
{
	copy(cpy);
	return *this;
}

void CoinWalletRequest::copy(const CoinWalletRequest& cpy)
{
	_id					= cpy._id;
	_done				= cpy._done;
	_jsonClientReply	= cpy._jsonClientReply;
	_params				= cpy._params;
}

const CoinWalletClient& CoinWalletRequest::client() const
{
	return _coinWalletClient;
}

CoinWalletClient& CoinWalletRequest::client()
{
	return _coinWalletClient;
}

const QUuid& CoinWalletRequest::id() const
{
	return _id;
}

const HttpJsonClientReply& CoinWalletRequest::jsonClientReply() const
{
	return _jsonClientReply;
}

HttpJsonClientReply& CoinWalletRequest::jsonClientReply()
{
	return _jsonClientReply;
}

const QString& CoinWalletRequest::rpcString()
{
	return _rpcString;
}

void CoinWalletRequest::setParams(const QVariantList& params)
{
	_params	= params;
}

const QVariantList& CoinWalletRequest::params() const
{
	return _params;
}

QVariantList& CoinWalletRequest::params()
{
	return _params;
}

bool CoinWalletRequest::isDone() const
{
	return _done;
}

bool CoinWalletRequest::isError() const
{
	return !errorString().isNull();
}

void CoinWalletRequest::setErrorString(const QString& errorString)
{
	_jsonClientReply.setErrorString(errorString);
}

const QString& CoinWalletRequest::errorString() const
{
	return _jsonClientReply.errorString();
}

QJsonValue CoinWalletRequest::jsonValue() const
{
	return _jsonClientReply.json().object().value("result");
}

QString CoinWalletRequest::dumpJsonValue(const QString& name /*= "result"*/) const
{
	QJsonValue jsonValue = _jsonClientReply.json().object().value("result");
	return CoinWalletClient::dumpJSONValue(name, jsonValue);
}

bool CoinWalletRequest::isValidReply()
{
	if (_jsonClientReply.json().isEmpty())
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json is empty");
		_jsonClientReply.setErrorString("reply is empty");
		return false;
	}
	if (!_jsonClientReply.json().isObject())
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json is not an object");
		_jsonClientReply.setErrorString("reply is not json object");
		return false;
	}
	const QJsonObject& jsonObj = _jsonClientReply.json().object();
	if (!jsonObj.contains("error"))
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json object, required field 'error' missing");
		_jsonClientReply.setErrorString("invalid reply: 'error' field missing");
		return false;
	}
	if (!jsonObj.contains("id"))
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json object, required field 'id' missing");
		_jsonClientReply.setErrorString("invalid reply: 'error 'id' field missing");
		return false;
	}
	if (jsonObj.value("id").isNull())
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json object, required field 'id' is null");
		_jsonClientReply.setErrorString("invalid reply: 'error 'id' field is null");
		return false;
	}
	if (!jsonObj.contains("result"))
	{
		DebugLog(dlWarn, "CoinWalletRequest::isValidReply reply json object, required field 'result' missing, id: %s", qPrintable(jsonObj.value("id").toString()));
		_jsonClientReply.setErrorString("invalid reply: 'error 'result' field missing");
		return false;
	}
	if (!jsonObj.value("error").isNull())
	{
		DebugLog(dlInfo, "CoinWalletRequest::isValidReply replied with error: %s", qPrintable(jsonObj.value("error").toString()));
		_jsonClientReply.setErrorString(jsonObj.value("error").toString());
		return false;
	}
	return true;
}

void CoinWalletRequest::replyReady(HttpJsonClientReply* preply, bool success)
{
	Q_ASSERT(preply == &_jsonClientReply);
	if (!success)
	{
		DebugLog(dlInfo, "CoinWalletRequest::replyReady replied failure: %s", qPrintable(_jsonClientReply.errorString()));
		emit finished(false, this);
		_done = true;
		return;
	}
	if (!isValidReply())
	{
		DebugLog(dlWarn, "CoinWalletRequest::replyReady replied failure or invalid: %s", qPrintable(_jsonClientReply.errorString()));
		emit finished(false, this);
		_done = true;
		return;
	}
	DebugLog(dlDebug, "CoinWalletRequest::replyReady got reply id: %s, reply: %s", qPrintable(_jsonClientReply.json().object().value("id").toString()), qPrintable(CoinWalletClient::dumpJSONObject(_jsonClientReply.json().object().value("id").toString(), _jsonClientReply.json().object())));
	emit finished(true, this);
	_done = true;
}

void CoinWalletRequest::setFailed(const QString& errorString)
{
	setErrorString(errorString);
	emit finished(false, this);
}

void CoinWalletRequest::rawRequest(const QString& rpcString)
{
	_done = false;
	_rpcString = rpcString;
	emit _coinWalletClient.request(*this);
}

void CoinWalletRequest::dumpPrivKey(const QString& sAddress)
{
	rawRequest(_api.dumpPrivKey(_id.toString(), sAddress));
}

void CoinWalletRequest::importPrivKey(const QString& privKey, const QString& label, bool rescan /*= true*/)
{
	rawRequest(_api.importPrivKey(_id.toString(), privKey, label, rescan));
}

void CoinWalletRequest::getAccount(const QString& address)
{
	rawRequest(_api.getAccount(_id.toString(), address));
}

void CoinWalletRequest::getAccountAddress(const QString& account)
{
	rawRequest(_api.getAccountAddress(_id.toString(), account));
}

void CoinWalletRequest::getAccountAddresses(const QString& account)
{
	rawRequest(_api.getAccountAddresses(_id.toString(), account));
}

void CoinWalletRequest::moveAccount(const QString& fromAccount, const QString& toAccount, double amount, const QString& comment /*= QString()*/, int minConf /*= -1*/)
{
	rawRequest(_api.moveAccount(_id.toString(), fromAccount, toAccount, amount, comment, minConf));
}

void CoinWalletRequest::listAccounts(int iMinConf /*= -1*/)
{
	rawRequest(_api.listAccounts(_id.toString(), iMinConf));
}

void CoinWalletRequest::setAccount(const QString& address, const QString& account)
{
	rawRequest(_api.setAccount(_id.toString(), address, account));
}

void CoinWalletRequest::decryptWallet(const QString& passphrase)
{
	rawRequest(_api.decryptWallet(_id.toString(), passphrase));
}

void CoinWalletRequest::encryptWallet(const QString& passphrase)
{
	rawRequest(_api.encryptWallet(_id.toString(), passphrase));
}

void CoinWalletRequest::getBalance(const QString& account /*= QString()*/, int minConf /*= -1*/)
{
	rawRequest(_api.getBalance(_id.toString(), account, minConf));
}

void CoinWalletRequest::getInfo()
{
	rawRequest(_api.getInfo(_id.toString()));
}

void CoinWalletRequest::getConnectionCount()
{
	rawRequest(_api.getConnectionCount(_id.toString()));
}

void CoinWalletRequest::getDifficulty()
{
	rawRequest(_api.getDifficulty(_id.toString()));
}

void CoinWalletRequest::getTransaction(const QString& txid)
{
	rawRequest(_api.getTransaction(_id.toString(), txid));
}

void CoinWalletRequest::setTxFee(double value)
{
	rawRequest(_api.setTxFee(_id.toString(), value));
}

void CoinWalletRequest::validateAddress(const QString& sAddress)
{
	rawRequest(_api.validateAddress(_id.toString(), sAddress));
}

void CoinWalletRequest::stopWalletServer()
{
	rawRequest(_api.stopWalletServer(_id.toString()));
}

void CoinWalletRequest::getReceivedByAccount(const QString& account, int minConf /*= -1*/)
{
	rawRequest(_api.getReceivedByAccount(_id.toString(), account, minConf));
}

void CoinWalletRequest::listReceivedByAccount(int minConf /*= -1*/, bool includeEmpty /*= false*/)
{
	rawRequest(_api.listReceivedByAccount(_id.toString(), minConf, includeEmpty));
}

void CoinWalletRequest::listReceivedByAddress(int minConf /*= -1*/, bool includeEmpty /*= false*/)
{
	rawRequest(_api.listReceivedByAddress(_id.toString(), minConf, includeEmpty));
}

void CoinWalletRequest::listTransactions(const QString& sAccount /*= QString()*/, int iCount /*= -1*/, int iSkipCount /*= -1*/)
{
	rawRequest(_api.listTransactions(_id.toString(), sAccount, iCount, iSkipCount));
}

void CoinWalletRequest::getNewAddress(const QString& account /*= QString()*/)
{
	rawRequest(_api.getNewAddress(_id.toString(), account));
}

void CoinWalletRequest::sendToAddress(const QString& toAddress, double amount, const QString& comment /*= QString()*/, const QString& commentTo /*= QString()*/)
{
	rawRequest(_api.sendToAddress(_id.toString(), toAddress, amount, comment, commentTo));
}

void CoinWalletRequest::signMessage(const QString& address, const QString& message)
{
	rawRequest(_api.signMessage(_id.toString(), address, message));
}

void CoinWalletRequest::verifyMessage(const QString& address, const QString& signature, const QString& message)
{
	rawRequest(_api.verifyMessage(_id.toString(), address, signature, message));
}
