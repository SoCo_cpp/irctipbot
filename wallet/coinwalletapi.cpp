#include "wallet/coinwalletapi.h"
#include <QJsonArray>
#include <QJsonDocument>
#include "logging/debuglogger.h"

CoinWalletAPI::CoinWalletAPI(QObject *parent) :
	QObject(parent)
{
}

QString CoinWalletAPI::buildAPI(const QString& sId, const QString& sMethod, const QString& sArgs /*= QString()*/, const QString& sSplitDelimiter /*= QString()*/)
{
	if (sArgs.isNull())
		return buildAPI(sId, sMethod, QJsonArray());
	else if (sSplitDelimiter.isNull())
	{
		QJsonArray jsonArgs;
		jsonArgs.append(sArgs);
		return buildAPI(sId, sMethod, jsonArgs);
	}
	else
		return buildAPI(sId, sMethod, sArgs.split(sSplitDelimiter, QString::SkipEmptyParts));
}

QString CoinWalletAPI::buildAPI(const QString& sId, const QString& sMethod, const QStringList& slArgs)
{
	QJsonArray jsonArgs;
	for (int i = 0;i < slArgs.size();i++)
		jsonArgs.append(QJsonValue(slArgs.at(i)));
	return buildAPI(sId, sMethod, jsonArgs);
}

QString CoinWalletAPI::buildAPI(const QString& sId, const QString& sMethod, const QVariantList& vlArgs)
{
	QJsonArray jsonArgs;
	for (int i = 0;i < vlArgs.size();i++)
		jsonArgs.append(QJsonValue::fromVariant(vlArgs.at(i)));
	return buildAPI(sId, sMethod, jsonArgs);
}

QString CoinWalletAPI::buildAPI(const QString& sId, const QString& sMethod, const QJsonArray& jsonArgs)
{
	QJsonObject jsonObj;
	jsonObj["jsonrpc"] = QString("1.0");
	jsonObj["method"] = sMethod;
	jsonObj["params"] = jsonArgs;
	jsonObj["id"] = sId;
	return QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
}

QString CoinWalletAPI::dumpPrivKey(const QString& sId, const QString& address)
{
	return buildAPI(sId, "dumpprivkey", address);
}

QString CoinWalletAPI::importPrivKey(const QString& sId, const QString& privKey, const QString& label, bool rescan /*= true*/)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(privKey));
	vlArgs.append(QVariant(label));
	if (rescan != -1)
		vlArgs.append(QVariant(rescan));
	return buildAPI(sId, "importprivkey", vlArgs);
}

QString CoinWalletAPI::getAccount(const QString& sId, const QString& address)
{
	return buildAPI(sId, "getaccount", address);
}

QString CoinWalletAPI::getAccountAddress(const QString& sId, const QString& account)
{
	return buildAPI(sId, "getaccountaddress", account);
}

QString CoinWalletAPI::getAccountAddresses(const QString& sId, const QString& account)
{
	return buildAPI(sId, "getaccountaddress", account);
}

QString CoinWalletAPI::moveAccount(const QString& sId, const QString& fromAccount, const QString& toAccount, double amount, const QString& comment /*= QString()*/, int minConf /*= -1*/)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(fromAccount));
	vlArgs.append(QVariant(toAccount));
	vlArgs.append(QVariant(amount));
	if (!comment.isNull() || minConf != -1)
		vlArgs.append(QVariant(minConf));
	if (!comment.isNull())
		vlArgs.append(QVariant(comment)); // comment is really last, not minConf
	return buildAPI(sId, "move", vlArgs);
}

QString CoinWalletAPI::listAccounts(const QString& sId, int minConf /*= -1*/)
{
	if (minConf != -1)
	{
		QVariantList vlArgs;
		vlArgs.append(QVariant("minConf"));
		return buildAPI(sId, "listaccounts", vlArgs);
	}
	else
		return buildAPI(sId, "listaccounts");
}

QString CoinWalletAPI::setAccount(const QString& sId, const QString& address, const QString& account)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(address));
	vlArgs.append(QVariant(account));
	return buildAPI(sId, "setaccount", vlArgs);
}

QString CoinWalletAPI::decryptWallet(const QString& sId, const QString& passphrase)
{
	return buildAPI(sId, "decryptwallet", passphrase);
}

QString CoinWalletAPI::encryptWallet(const QString& sId, const QString& passphrase)
{
	return buildAPI(sId, "encryptwallet", passphrase);
}

QString CoinWalletAPI::getBalance(const QString& sId, const QString& account /*= QString()*/, int minConf /*= -1*/)
{
	QVariantList vlArgs;
	if (!account.isNull() || minConf != -1)
		vlArgs.append(QVariant(account));
	if (minConf != -1)
		vlArgs.append(QVariant(minConf));
	return buildAPI(sId, "encryptwallet", vlArgs);
}

QString CoinWalletAPI::getInfo(const QString& sId)
{
	return buildAPI(sId, "getinfo");
}

QString CoinWalletAPI::getConnectionCount(const QString& sId)
{
	return buildAPI(sId, "getconnectioncount");
}

QString CoinWalletAPI::getDifficulty(const QString& sId)
{
	return buildAPI(sId, "getdifficulty");
}

QString CoinWalletAPI::getTransaction(const QString& sId, const QString& txid)
{
	return buildAPI(sId, "gettransaction", txid);
}

QString CoinWalletAPI::setTxFee(const QString& sId, double value)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(value));
	return buildAPI(sId, "settxfee", vlArgs);
}

QString CoinWalletAPI::validateAddress(const QString& sId, const QString& address)
{
	return buildAPI(sId, "validateaddress", address);
}

QString CoinWalletAPI::stopWalletServer(const QString& sId)
{
	return buildAPI(sId, "stop");
}

QString CoinWalletAPI::getReceivedByAccount(const QString& sId, const QString& account, int minConf /*= -1*/)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(account));
	if (minConf != -1)
		vlArgs.append(QVariant(minConf));
	return buildAPI(sId, "getreceivedbyaccount", vlArgs);
}

QString CoinWalletAPI::listReceivedByAccount(const QString& sId, int minConf /*= -1*/, bool includeEmpty /*= false*/)
{
	QVariantList vlArgs;
	if (minConf != -1 || includeEmpty)
		vlArgs.append(QVariant(minConf));
	if (includeEmpty)
		vlArgs.append(QVariant(includeEmpty));
	return buildAPI(sId, "listreceivedbyaccount", vlArgs);
}

QString CoinWalletAPI::listReceivedByAddress(const QString& sId, int minConf /*= -1*/, bool includeEmpty /*= false*/)
{
	QVariantList vlArgs;
	if (minConf != -1 || includeEmpty)
		vlArgs.append(QVariant(minConf));
	if (includeEmpty)
		vlArgs.append(QVariant(includeEmpty));
	return buildAPI(sId, "listreceivedbyaddress ", vlArgs);
}

QString CoinWalletAPI::listTransactions(const QString& sId, const QString& account /*= QString()*/, int count /*= -1*/, int skipCount /*= -1*/)
{
	QVariantList vlArgs;
	if (!account.isNull() || count != -1 || skipCount != -1)
		vlArgs.append(QVariant(account));
	if (count != -1 || skipCount != -1)
		vlArgs.append(QVariant(count));
	if (skipCount != -1)
		vlArgs.append(QVariant(skipCount));
	return buildAPI(sId, "listtransactions", vlArgs);
}

QString CoinWalletAPI::getNewAddress(const QString& sId, const QString& account /*= QString()*/)
{
	return buildAPI(sId, "getnewaddress", account);
}

QString CoinWalletAPI::sendMany(const QString& sId, const QString& fromAccount, const QMap<QString, double>& addressValueMap, const QString& comment /*= QString()*/, int minConf /*= 1*/)
{
	Q_ASSERT(false); // not yet implemented
	Q_UNUSED(fromAccount);
	Q_UNUSED(addressValueMap);
	Q_UNUSED(comment);
	Q_UNUSED(minConf);
	//@TODO <fromaccount> <tobitcoinaddress> <amount> [minconf=1] [comment] [comment-to]
	//QVariantList vlArgs;
	//vlArgs.append(QVariant(value));
	return buildAPI(sId, "sendmany");
}

QString CoinWalletAPI::sendToAddress(const QString& sId, const QString& toAddress, double amount, const QString& comment /*= QString()*/, const QString& commentTo /*= QString()*/)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(toAddress));
	vlArgs.append(QVariant(amount));
	if (!comment.isNull() || !commentTo.isNull())
		vlArgs.append(QVariant(comment));
	if (!commentTo.isNull())
		vlArgs.append(QVariant(commentTo));
	return buildAPI(sId, "sendtoaddress", vlArgs);
}

QString CoinWalletAPI::signMessage(const QString& sId, const QString& address, const QString& message)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(address));
	vlArgs.append(QVariant(message));
	return buildAPI(sId, "signmessage", vlArgs);
}

QString CoinWalletAPI::verifyMessage(const QString& sId, const QString& address, const QString& signature, const QString& message)
{
	QVariantList vlArgs;
	vlArgs.append(QVariant(address));
	vlArgs.append(QVariant(signature));
	vlArgs.append(QVariant(message));
	return buildAPI(sId, "verifymessage", vlArgs);
}

