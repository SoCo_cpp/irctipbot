#ifndef COINWALLETCLIENTCONFIG_H
#define COINWALLETCLIENTCONFIG_H

#include <QObject>
#include "networking/proxyserverconfig.h"

class CoinWalletClientConfig : public ProxyServerConfig
{
	Q_OBJECT
public:
	explicit CoinWalletClientConfig(QObject *parent = 0);
	CoinWalletClientConfig(const CoinWalletClientConfig& cpy);
	CoinWalletClientConfig& operator =(const CoinWalletClientConfig& cpy);
	void copy(const CoinWalletClientConfig& cpy, bool noChangeSignal = false);

/*** Inheritted:
#ifdef JSON_CONFIG
	virtual void saveJson(QJsonObject& jsonObj) const;
	virtual bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif
*/

protected:

signals:
	//Inheritted: void changed();

public slots:

};

#endif // COINWALLETCLIENTCONFIG_H
