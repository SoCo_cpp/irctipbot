#include "wallet/coinwalletclient.h"
#include "logging/debuglogger.h"

CoinWalletClient::CoinWalletClient(QObject *parent) :
	QObject(parent)
{
	connect(&_config, SIGNAL(changed()), this, SLOT(configChanged()));
}

void CoinWalletClient::setConfig(const CoinWalletClientConfig& cfg)
{
	_config = cfg;
}

const CoinWalletClientConfig& CoinWalletClient::config() const
{
	return _config;
}

CoinWalletClientConfig& CoinWalletClient::config()
{
	return _config;
}

void CoinWalletClient::configChanged()
{
	_jsonClient.setProxyConfig(_config.proxyConfig());
	_jsonClient.setUrl(_config.serverConfig().httpUrl());
}

void CoinWalletClient::request(CoinWalletRequest& walletRequest)
{
	DebugLog(dlDebug, "CoinWalletClient::onRawRequest got request: %s", qPrintable(walletRequest.rpcString()));
	if (!_jsonClient.request(walletRequest.jsonClientReply(), walletRequest.rpcString().toLatin1(), HttpJsonClient::Post))
	{
		DebugLog(dlWarn, "CoinWalletClient::onRawRequest jsonClient request failed");
		walletRequest.setFailed("HTTP/JSON Client failed to make request");
	}
}

/*static*/
QString CoinWalletClient::dumpJSONObject(const QString& name, const QJsonObject& jsonObj, int indent /*= 0*/)
{
	QString s;
	QString sIndent;
	sIndent.fill('\t', indent);
	if (jsonObj.isEmpty())
		s += sIndent + QString("---<'%1' = Empty Object />---\n").arg(name);
	else
	{
		s += sIndent + QString("---<'%1' = Object (%2)>---\n").arg(name).arg(jsonObj.size());
		for (int i = 0;i < jsonObj.keys().size();i++)
			s += dumpJSONValue(jsonObj.keys().at(i), jsonObj.value(jsonObj.keys().at(i)), indent + 1);
		s += sIndent + QString("---</'%1' = Object (%2)>---\n").arg(name).arg(jsonObj.size());
	}
	return s;
}

/*static*/
QString CoinWalletClient::dumpJSONValue(const QString& name, const QJsonValue& jsonValue, int indent /*= 0*/)
{
	QString s;
	QString sIndent;
	sIndent.fill('\t', indent);
	if (jsonValue.isArray())
		s += dumpJSONArray(name, jsonValue.toArray(), indent);
	else if (jsonValue.isObject())
		s += dumpJSONObject(name, jsonValue.toObject(), indent);
	else if (jsonValue.isNull())
		s += sIndent + QString("'%1' = (null)\n").arg(name);
	else if (jsonValue.isUndefined())
		s += sIndent + QString("'%1' = (undefined)\n").arg(name);
	else if (jsonValue.isBool())
		s += sIndent + QString("'%1' = (bool) %2\n").arg(name).arg(QChar(BoolYN(jsonValue.toBool())));
	else if (jsonValue.isDouble())
		s += sIndent + QString("'%1' = (double) %2\n").arg(name).arg(jsonValue.toDouble());
	else if (jsonValue.isString())
		s += sIndent + QString("'%1' = (string)'%2'\n").arg(name).arg(jsonValue.toString());
	else
		s += sIndent + QString("'%1' = (unknown type)\n").arg(name);
	return s;
}

/*static*/
QString CoinWalletClient::dumpJSONArray(const QString& name, const QJsonArray& jsonArray, int indent /*= 0*/)
{
	QString s;
	QString sIndent;
	sIndent.fill('\t', indent);
	if (jsonArray.empty())
		s += sIndent + QString("---<'%1' = Empty Array />---\n").arg(name);
	else
	{
		s += sIndent + QString("---<'%1' = Array (%2)>---\n").arg(name).arg(jsonArray.size());
		for (int i = 0;i < jsonArray.size();i++)
			s += dumpJSONValue(QString("[%1]").arg(i), jsonArray.at(i), indent + 1);
		s += sIndent + QString("---</'%1' = Array (%2)>---\n").arg(name).arg(jsonArray.size());
	}
	return s;
}
