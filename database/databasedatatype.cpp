#include "database/databasedatatype.h"
#include "logging/debuglogger.h"

DatabaseDataType::DatabaseDataType(QObject *parent, BaseDataType baseType /*= tInt32*/, int size /*= 0*/, bool bUnsigned /*= false*/, bool bUnique /*= false*/, bool bNotNull /*= false*/, bool bAutoInc /*= false*/, const QVariant& defaultValue /*= QVariant()*/) :
	QObject(parent),
	_baseType(baseType),
	_size(size),
	_unsigned(bUnsigned),
	_unique(bUnique),
	_notnull(bNotNull),
	_autoInc(bAutoInc),
	_defaultValue(defaultValue)
{
}

DatabaseDataType::DatabaseDataType(const DatabaseDataType& cpy) :
	QObject(0)
{
	copy(cpy);
}

DatabaseDataType& DatabaseDataType::operator =(const DatabaseDataType& cpy)
{
	copy(cpy);
	return *this;
}

void DatabaseDataType::copy(const DatabaseDataType& cpy)
{
	_baseType		= cpy._baseType;
	_size			= cpy._size;
	_unsigned		= cpy._unsigned;
	_unique			= cpy._unique;
	_notnull		= cpy._notnull;
	_autoInc		= cpy._autoInc;
	_defaultValue	= cpy._defaultValue;
}

void DatabaseDataType::setBaseType(BaseDataType type)
{
	_baseType = type;
}

DatabaseDataType::BaseDataType DatabaseDataType::baseType() const
{
	return _baseType;
}

void DatabaseDataType::setSize(int size)
{
	_size = size;
}

int DatabaseDataType::size() const
{
	return _size;
}

void DatabaseDataType::setUnsigned(bool bUnsigned)
{
	_unsigned = bUnsigned;
}

bool DatabaseDataType::isUnsigned() const
{
	return _unsigned;
}

void DatabaseDataType::setDefaultValue(const QVariant& value /*= QVariant()*/)
{
	_defaultValue = value;
}

const QVariant& DatabaseDataType::defaultValue() const
{
	return _defaultValue;
}

bool DatabaseDataType::hasDefaultValue() const
{
	return !_defaultValue.isNull();
}

void DatabaseDataType::setUnique(bool bUnique)
{
	_unique = bUnique;
}

bool DatabaseDataType::isUnique() const
{
	return _unique;
}

void DatabaseDataType::setNotNull(bool bNotNull)
{
	_notnull = bNotNull;
}

bool DatabaseDataType::isNotNull() const
{
	return _notnull;
}

void DatabaseDataType::setAutoInc(bool bAuto)
{
	_autoInc = bAuto;
}

bool DatabaseDataType::isAutoInc() const
{
	return _autoInc;
}

void DatabaseDataType::set(BaseDataType baseType /*= tInt32*/, int size /*= 0*/, bool bUnsigned /*= false*/, bool bUnique /*= false*/, bool bNotNull /*= false*/, bool bAutoInc /*= false*/, const QVariant& defaultValue /*= QVariant()*/)
{
	_baseType		= baseType;
	_size			= size;
	_unsigned		= bUnsigned;
	_unique			= bUnique;
	_notnull		= bNotNull;
	_autoInc		= bAutoInc;
	_defaultValue	= defaultValue;
}

QString DatabaseDataType::typeString(const QString& databaseType) const
{
	QString sDef;
	if (databaseType == "QMYSQL")
	{
		int size;
		switch (_baseType)
		{
			case tBool:			sDef = "TINYINT";			break;
			case tInt8:			sDef = "TINYINT";			break;
			case tInt16:		sDef = "SMALLINT";			break;
			case tInt32:		sDef = "INT";				break;
			case tInt64:		sDef = "BIGINT";			break;
			case tDecimal32:	sDef = "FLOAT";				break;
			case tDecimal63:	sDef = "DOUBLE";			break;
			case tDate:			sDef = "DATE";				break;
			case tTime:			sDef = "TIME";				break;
			case tDateTime:		sDef = "DATETIME";			break;
			case tChar:			sDef = "CHAR";				break;
			case tVarChar:		sDef = "VARCHAR";			break;
			case tText:			sDef = "TEXT";				break;
			default:
				DebugLog(dlError, "DatabaseDataType::typeString unsupported baseType: %d", (int)_baseType);
				return "";
		} // switch
		size = _size;
		if (_baseType == tBool)
			size = 1;
		if (size != 0)
			sDef += QString("(%1)").arg(size);
		if (_unsigned)
			sDef += " UNSIGNED";
		if (_notnull)
			sDef += " NOT NULL";
		if (_autoInc)
			sDef += " AUTO_INCREMENT";
		if (_unique)
			sDef += " UNIQUE";
		if (hasDefaultValue())
		{
			sDef += " DEFAULT '";
			sDef += _defaultValue.toString();
			sDef += "'";
		}
	} // QMYSQL
	else DebugLog(dlError, "DatabaseDataType::typeString database type not supported: ", qPrintable(databaseType));
	return sDef;
}

bool DatabaseDataType::isNumberType() const
{
	switch (_baseType)
	{
		case tBool:
		case tInt8:
		case tInt16:
		case tInt32:
		case tInt64:
		case tDecimal32:
		case tDecimal63:
			return true;
		default:
			return false;
	} // switch
}

bool DatabaseDataType::isStringType(bool bIncludeDateTime /*= false*/) const
{
	switch (_baseType)
	{
		case tChar:
		case tVarChar:
		case tText:
			return true;
		case tDate:
		case tTime:
		case tDateTime:
			return bIncludeDateTime;
		default:
			return false;
	} // switch
}

bool DatabaseDataType::isDateTimeType() const
{
	switch (_baseType)
	{
		case tDate:
		case tTime:
		case tDateTime:
		return true;
		default:
			return false;
	} // switch
}
