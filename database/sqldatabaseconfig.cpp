#include "database/sqldatabaseconfig.h"
#include "logging/debuglogger.h"

SqlDatabaseConfig::SqlDatabaseConfig(QObject *parent) :
	QObject(parent)
{
	connect(&_serverConfig, SIGNAL(changed()), this, SIGNAL(changed()));
}

SqlDatabaseConfig::SqlDatabaseConfig(const SqlDatabaseConfig& cpy) :
	QObject(0)
{
	copy(cpy, true /*noChangeSignal*/);
}

SqlDatabaseConfig& SqlDatabaseConfig::operator =(const SqlDatabaseConfig& cpy)
{
	copy(cpy);
	return *this;
}

void SqlDatabaseConfig::copy(const SqlDatabaseConfig& cpy, bool noChangeSignal /*= false*/)
{
	_serverConfig.copy(cpy._serverConfig, true/*noChangeSignal*/);
	_driverName			= cpy._driverName;
	_databaseName		= cpy._databaseName;
	_connectionString	= cpy._connectionString;
	if (!noChangeSignal)
		emit changed();
}

void SqlDatabaseConfig::setServerConfig(const ServerConfig& cfg)
{
	_serverConfig = cfg;
	emit changed();
}

ServerConfig& SqlDatabaseConfig::serverConfig()
{
	return _serverConfig;
}

const ServerConfig& SqlDatabaseConfig::serverConfig() const
{
	return _serverConfig;
}

void SqlDatabaseConfig::setDriverName(const QString& sDriver)
{
	_driverName = sDriver;
	emit changed();
}

const QString& SqlDatabaseConfig::driverName() const
{
	return _driverName ;
}

void SqlDatabaseConfig::setDatabaseName(const QString& sDatabase)
{
	_databaseName = sDatabase;
	emit changed();
}

const QString& SqlDatabaseConfig::databaseName() const
{
	return _databaseName;
}

void SqlDatabaseConfig::setConnectionString(const QString& sConnectionString)
{
	_connectionString = sConnectionString;
	emit changed();
}

const QString& SqlDatabaseConfig::connectionString() const
{
	return _connectionString;
}

bool SqlDatabaseConfig::isValid() const
{
	if (_driverName.isEmpty())
		return false;
	if (_databaseName.isEmpty())
		return false;
	if (!_serverConfig.isValid())
		return false;
	return true;
}

#ifdef JSON_CONFIG
void SqlDatabaseConfig::saveJson(QJsonObject& jsonObj) const
{
	QJsonObject jsonServerObj;
	_serverConfig.saveJson(jsonServerObj);

	jsonObj["ver"]			= 1;
	jsonObj["driver"]		= _driverName;
	jsonObj["database"]		= _databaseName;
	jsonObj["server"]		= jsonServerObj;
	jsonObj["con-sting"]	= _connectionString;
}
#endif // #ifdef JSON_CONFIG

#ifdef JSON_CONFIG
bool SqlDatabaseConfig::loadJson(const QJsonObject& jsonObj, bool bRequireAllFields /*= false*/, bool noChangeSignal /*= false*/)
{
	copy(SqlDatabaseConfig(), true/*noChangeSignal*/);
	if (bRequireAllFields)
		if ( !jsonObj.contains("driver") || !jsonObj.contains("database") || !jsonObj.contains("server") || !jsonObj.contains("con-sting") )
		{
			DebugLog(dlWarn, "SqlDatabaseConfig::loadJson all fields required, but not all found");
			return false;
		}

	if (!jsonObj.contains("ver"))
	{
		DebugLog(dlWarn, "SqlDatabaseConfig::loadJson version field missing");
		return false;
	}

	if (jsonObj.contains("driver"))
		_driverName = jsonObj["driver"].toString();

	if (jsonObj.contains("database"))
		_databaseName = jsonObj["database"].toString();

	if (jsonObj.contains("con-sting"))
		_connectionString = jsonObj["con-sting"].toString();

	if (jsonObj.contains("server"))
	{
		if (!jsonObj["server"].isObject())
		{
			DebugLog(dlWarn, "SqlDatabaseConfig::loadJson server field is not an object");
			return false;
		}
		if (!_serverConfig.loadJson(jsonObj["server"].toObject(), bRequireAllFields, true/*noChangeSignal*/))
		{
			DebugLog(dlWarn, "SqlDatabaseConfig::loadJson server config failed ot loadJson");
			return false;
		}
	}

	if (!noChangeSignal)
		emit changed();
	return true;
}
#endif // #ifdef JSON_CONFIG
