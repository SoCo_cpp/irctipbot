#include "database/sqldatabase.h"
#include "logging/debuglogger.h"
#include <QSqlError>
#include <QCoreApplication>

SqlDatabase::SqlDatabase(QObject*, const QString&  sConnectionName /*= QString()*/) :
	QObject(0), // no parent with thread
	_connectionName(sConnectionName),
	_initted(false),
	_wantConnection(false),
	_connectionStatus(cNone)
{
	moveToThread(&_thread);
	_thread.start();

	connect(&_connectionTimer, SIGNAL(timeout()), this, SLOT(connectionTimerTick()));
	_connectionTimer.start(cConnectionTimerInterval);
}

SqlDatabase::~SqlDatabase()
{
	_thread.quit();
	_thread.wait();
}

void SqlDatabase::quitWait()
{
	_thread.quit();
	_thread.wait();
}

void SqlDatabase::setConnectionName(const QString& name)
{
	_connectionName = name;
}

const QString& SqlDatabase::connectionName() const
{
	return _connectionName;
}

void SqlDatabase::setDatabaseConfig(const SqlDatabaseConfig& config)
{
	_databaseConfig = config;
}

const SqlDatabaseConfig& SqlDatabase::databaseConfig() const
{
	return _databaseConfig;
}

SqlDatabaseConfig& SqlDatabase::databaseConfig()
{
	return _databaseConfig;
}

void SqlDatabase::setConnectionStatus(ConnectionStatus cStatus)
{
	_connectionStatus = cStatus;
	emit connectionStatusChanged();
}

SqlDatabase::ConnectionStatus SqlDatabase::connectionStatus() const
{
	return _connectionStatus;
}

QString SqlDatabase::connectionStatusString() const
{
	switch (_connectionStatus)
	{
		case cNone:				return "None";
		case cDisconnected:		return "Disconnected";
		case cDisconnecting:	return "Disconnecting";
		case cConnecting:		return "Connecting";
		case cConnected:		return "Connected";
		case cError:			return "Error";
		case cReconnecting:		return "Reconnecting";
		default:				return QString("Unknown(%1)").arg(_connectionStatus);
	} // switch
}

bool SqlDatabase::isConnected() const
{
	return (_connectionStatus == cConnected);
}

bool SqlDatabase::isDisconnected(bool bFullDisconnect /*= false*/) const
{
	switch (_connectionStatus)
	{
		case cDisconnecting:
			return !bFullDisconnect;
		case cNone:
		case cDisconnected:
		case cError:
			return true;
		default:
			return false;
	} // switch (_connectionStatus)
}

bool SqlDatabase::isCleanDisconnect() const
{
	return (_connectionStatus == cNone || _connectionStatus == cDisconnected);
}

void SqlDatabase::connectionTimerTick()
{
	if (!_initted)
		return;
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::connectionTimerTick  database is invalid");
		return;
	}
	if (database.isOpen())
	{
		 if (_connectionStatus == cConnecting)
			 setConnectionStatus(cConnected);
		 if (!_wantConnection && (_connectionStatus == cConnected || _connectionStatus == cError))
		 {
			 DebugLog(dlDebug, "SqlDatabase::connectionTimerTick don't want connection, connected, disconnecting");
			 if (!closeDatabase()) // ignore failure and keep trying
				 DebugLog(dlDebug, "SqlDatabase::connectionTimerTick closeDatabase failed");
			 return;
		 }
	}
	else // if (database.isOpen())
	{
		if (_connectionStatus == cDisconnecting)
			setConnectionStatus(cDisconnected);
		if (_wantConnection && isDisconnected())
		{
			if (!isCleanDisconnect())
			{
				DebugLog(dlDebug, "SqlDatabase::connectionTimerTick want connection, disconnected, connecting on dirty disconnect");
				if (!closeDatabase()) // ignore failure and keep trying before openingDatabase
					return;
			}
			else DebugLog(dlDebug, "SqlDatabase::connectionTimerTick want connection, disconnected, connecting on clean disconnect");

			if (!openDatabase())
			{
				DebugLog(dlWarn, "SqlDatabase::connectionTimerTick openDatabase failed");
				closeDatabase();  // ignore failure
				return;
			}
		} // if (_wantConnection && isDisconnected())
	} // else // if (database.isOpen())
}

bool SqlDatabase::initializeDatabase(bool failOnInitted /*= false*/)
{
	if (_initted)
	{
		if (failOnInitted)
		{
			DebugLog(dlWarn, "SqlDatabase::initializeDatabase wanted fail on already initted");
			return false;
		}
		DebugLog(dlWarn, "SqlDatabase::initializeDatabase already initted, no fail, releaseDatabase");
		closeDatabase();
		releaseDatabase();
	} // if (_initted)
	if (!_databaseConfig.isValid())
	{
		DebugLog(dlWarn, "SqlDatabase::initializeDatabase validate database config failed");
		return false;
	}
	if (!QSqlDatabase::isDriverAvailable(_databaseConfig.driverName()))
	{
		DebugLog(dlWarn, "SqlDatabase::initializeDatabase database driver not available: %s", qPrintable(_databaseConfig.driverName()));
		return false;
	}
	QSqlDatabase database = QSqlDatabase::addDatabase(_databaseConfig.driverName(), _connectionName);
	database.setHostName(		_databaseConfig.serverConfig().host());
	database.setPort(			_databaseConfig.serverConfig().port());
	database.setUserName(		_databaseConfig.serverConfig().user());
	database.setPassword(		_databaseConfig.serverConfig().password());
	database.setConnectOptions(_databaseConfig.connectionString());
	database.setDatabaseName(_databaseConfig.databaseName());

	if (!database.isValid())
	{
		DebugLog(dlWarn, "SqlDatabase::initializeDatabase database isValid check failed");
		return false;
	}
	DebugLog(dlDebug, "SqlDatabase::initializeDatabase initted");
	_initted = true;
	return true;
}

bool SqlDatabase::releaseDatabase()
{
	if (_initted)
	{
		if (!closeDatabase())
		{
			DebugLog(dlWarn, "SqlDatabase::releaseDatabase closeDatabase failed");
			return false;
		}
		QSqlDatabase::removeDatabase(_connectionName);
		_initted = false;
	}
	return true;
}

bool SqlDatabase::connectDatabase(bool forceReconnect /*= false*/, bool failOnConnected /*= false*/)
{
	if (!initializeDatabase())
	{
		DebugLog(dlWarn, "SqlDatabase::connectDatabase initializeDatabase failed");
		return false;
	}
	if (!isDisconnected())
	{
		DebugLog(dlDebug, "SqlDatabase::connectDatabase not disconnected status: %s", qPrintable(connectionStatusString()));
		if (forceReconnect && failOnConnected)
		{
			DebugLog(dlError, "SqlDatabase::connectDatabase parameter error, cannot use both forceReconnect and failOnConnected");
			return false;
		}
		if (failOnConnected)
			return false; // already connected, fail expected, no error
		if (!forceReconnect)
			return true; // nothing to do, already connected
		if (!closeDatabase())
		{
			DebugLog(dlError, "SqlDatabase::connectDatabase closeDatabase failed");
			return false;
		}
	} // if (!isDisconnected())
	_wantConnection = true;
	return true;
}

bool SqlDatabase::disconnectDatabase()
{
	_wantConnection = false;
	return true;
}

bool SqlDatabase::openDatabase()
{
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::openDatabase  database is invalid");
		return false;
	}
	if (!_initted)
	{
		DebugLog(dlError, "SqlDatabase::openDatabase database not initted");
		return false; // invalid state
	}
	if (!isDisconnected())
	{
		DebugLog(dlError, "SqlDatabase::openDatabase invalid connection state: %s", qPrintable(connectionStatusString()));
		return false; // invalid state
	}
	setConnectionStatus(cConnecting);
	if (!database.open())
	{
		DebugLog(dlWarn, "SqlDatabase::openDatabase open database connection failed: %s", qPrintable(database.lastError().text()));
		return false;
	}
	return true;
}

bool SqlDatabase::closeDatabase()
{
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::closeDatabase  database is invalid");
		return false;
	}
	_wantConnection = false;
	setConnectionStatus(cDisconnecting);
	database.close();
	return true;
}

QStringList SqlDatabase::tables()
{
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::tables  database is invalid");
		return QStringList();
	}
	return database.tables();
}

QSqlQuery SqlDatabase::query()
{
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::query  database is invalid");
		return QSqlQuery();
	}
	return QSqlQuery(database);
}

bool SqlDatabase::execQuery(const QString& sql)
{
	if (!isConnected())
	{
		DebugLog(dlWarn,  "SqlDatabase::execQuery database not connected");
		return false;
	}
	QSqlDatabase database = QSqlDatabase::database(_connectionName, false/*open*/);
	if (!database.isValid())
	{
		DebugLog(dlError, "SqlDatabase::execQuery  database is invalid");
		return false;
	}
	QSqlQuery qry(database);
	if (!qry.exec(sql))
	{
		DebugLog(dlWarn,  "SqlDatabase::execQuery exec failed error: %s", qPrintable(qry.lastError().text()));
		DebugLog(dlDebug, "  - sql: %s", qPrintable(qry.lastQuery()));
		return false;
	}
	return true;
}
