#ifndef DATABASEDATATYPE_H
#define DATABASEDATATYPE_H

#include <QObject>
#include <QVariant>

class DatabaseDataType : public QObject
{
	Q_OBJECT
public:
	typedef enum {tBool = 0, tInt8, tInt16, tInt32, tInt64, tDecimal32, tDecimal63, tDate, tTime, tDateTime, tChar, tVarChar, tText} BaseDataType;

	explicit DatabaseDataType(QObject *parent = 0, BaseDataType baseType = tInt32, int size = 0, bool bUnsigned = false, bool bUnique = false, bool bNotNull = false, bool bAutoInc = false, const QVariant& defaultValue = QVariant());
	DatabaseDataType(const DatabaseDataType& cpy);
	DatabaseDataType& operator =(const DatabaseDataType& cpy);
	void copy(const DatabaseDataType& cpy);

	void setBaseType(BaseDataType type);
	BaseDataType baseType() const;

	void setSize(int size);
	int size() const;

	void setUnsigned(bool bUnsigned);
	bool isUnsigned() const;

	void setDefaultValue(const QVariant& value = QVariant());
	const QVariant& defaultValue() const;
	bool hasDefaultValue() const;

	void setUnique(bool bUnique);
	bool isUnique() const;

	void setNotNull(bool bNotNull);
	bool isNotNull() const;

	void setAutoInc(bool bAuto);
	bool isAutoInc() const;

	void set(BaseDataType baseType, int size = 0, bool bUnsigned = false, bool bUnique = false, bool bNotNull = false, bool bAutoInc = false, const QVariant& defaultValue = QVariant());

	QString typeString(const QString& databaseType) const;

	bool isNumberType() const;
	bool isStringType(bool bIncludeDateTime = false) const;
	bool isDateTimeType() const;

protected:
	BaseDataType _baseType;
	int _size;
	bool _unsigned;
	bool _unique;
	bool _notnull;
	bool _autoInc;
	QVariant _defaultValue;

signals:

public slots:

};

#endif // DATABASEDATATYPE_H
