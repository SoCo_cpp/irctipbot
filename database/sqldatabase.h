#ifndef SQLDATABASE_H
#define SQLDATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QTimer>
#include <QThread>
#include <QSqlQuery>
#include <QStringList>
#include "database/sqldatabaseconfig.h"

class SqlDatabase : public QObject
{
	Q_OBJECT
public:
	const static int cConnectionTimerInterval = 1000;	// msec

	typedef enum {cNone = 0, cDisconnected, cDisconnecting, cConnecting, cConnected, cError, cReconnecting} ConnectionStatus;

	explicit SqlDatabase(QObject *parent = 0, const QString&  sConnectionName = QString());
	~SqlDatabase();

	bool releaseDatabase();
	void quitWait();

	void setConnectionName(const QString& name);
	const QString& connectionName() const;

	void setDatabaseConfig(const SqlDatabaseConfig& config);
	const SqlDatabaseConfig& databaseConfig() const;
	SqlDatabaseConfig& databaseConfig();

	void setConnectionStatus(ConnectionStatus cStatus);
	ConnectionStatus connectionStatus() const;
	QString connectionStatusString() const;

	bool isConnected() const;
	bool isDisconnected(bool bFullDisconnect = false) const;
	bool isCleanDisconnect() const;

	bool initializeDatabase(bool failOnInitted = false);

	bool isValid() const;

	bool connectDatabase(bool forceReconnect = false, bool failOnConnected = false);
	bool disconnectDatabase();

	QStringList tables();
	QSqlQuery query();

	bool execQuery(const QString& sql);

protected:
	QString _connectionName;
	bool _initted;
	bool _wantConnection;
	ConnectionStatus _connectionStatus;
	//QSqlDatabase _database;
	SqlDatabaseConfig _databaseConfig;
	QTimer _connectionTimer;
	QThread _thread;

	bool openDatabase();
	bool closeDatabase();

signals:
	void connectionStatusChanged();

public slots:
	void connectionTimerTick();

};

#endif // SQLDATABASE_H
