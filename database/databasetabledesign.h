#ifndef DATABASETABLEDESIGN_H
#define DATABASETABLEDESIGN_H

#include <QObject>
#include <QList>
#include <QHash>
#include "database/databasedatatype.h"

class DatabaseTableDesign : public QObject
{
	Q_OBJECT
public:
	typedef struct
	{
		QString name;
		DatabaseDataType datatype;
		bool isSet;
		QString value;
		bool noQuoteValue;
	} SField;

	explicit DatabaseTableDesign(QObject *parent = 0, const QString& databaseType = QString(), const QString& tableName = QString());

	void setDatabaseType(const QString& databaseType);

	void setTableName(const QString& name);
	const QString& tableName() const;

	void clearFields();
	void appendFieldDef(const QString& fieldName, const DatabaseDataType& dataType);
	int indexOfField(const QString& fieldName) const;

	void clearValues();
	void setValues();
	void setValue(const QString& fieldName, const QString& sValue = QString(), bool noQuoteValue = false);
	void unsetValue(const QString& fieldName);

	QString setFieldNameList();
	QString setFieldValueList();
	QString setFieldNameValueList();


	QString sqlCreateTable(bool bIfNotExists = false) const;
	QString sqlDropTable(bool bIfExists = false) const;

	QString sqlInsert();
	QString sqlUpdate(const QString& sqlWhere = QString(), int limit = 0, const QString& sqlOrderLimitBy = QString());
	QString sqlSelect(const QString& sqlWhere = QString(), const QString& sqlGroupBy = QString(), const QString& sqlOrderBy = QString(), int limit = 0, int offest = 0);
	QString sqlDelete(const QString& sqlWhere = QString(), int limit = 0, const QString& sqlOrderLimitBy = QString());

protected:
	QString _dbType;
	QString _tableName;
	QList<SField> _fields;

signals:

public slots:

};

#endif // DATABASETABLEDESIGN_H
