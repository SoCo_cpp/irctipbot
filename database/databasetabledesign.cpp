#include "database/databasetabledesign.h"
#include "logging/debuglogger.h"

DatabaseTableDesign::DatabaseTableDesign(QObject *parent, const QString& databaseType /*= QString()*/, const QString& tableName /*= QString()*/) :
	QObject(parent),
	_dbType(databaseType),
	_tableName(tableName)
{
}

void DatabaseTableDesign::setDatabaseType(const QString& databaseType)
{
	_dbType = databaseType;
}

void DatabaseTableDesign::setTableName(const QString& name)
{
	_tableName = name;
}

const QString& DatabaseTableDesign::tableName() const
{
	return _tableName;
}

void DatabaseTableDesign::clearFields()
{
	_fields.clear();
}

void DatabaseTableDesign::appendFieldDef(const QString& fieldName, const DatabaseDataType& dataType)
{
	SField newField;
	newField.name = fieldName;
	newField.datatype = dataType;
	newField.isSet = false;
	_fields.append(newField);
}

int DatabaseTableDesign::indexOfField(const QString& fieldName) const
{
	for (int i = 0;i < _fields.size();i++)
		if (_fields.at(i).name == fieldName)
			return i;
	return -1;
}

void DatabaseTableDesign::clearValues()
{
	for (int i = 0;i < _fields.size();i++)
		_fields[i].isSet = false;
}

void DatabaseTableDesign::setValues()
{
	for (int i = 0;i < _fields.size();i++)
		_fields[i].isSet = true;
}

void DatabaseTableDesign::setValue(const QString& fieldName, const QString& sValue /*= QString()*/, bool noQuoteValue /*= false*/)
{
	int idx = indexOfField(fieldName);
	if (idx == -1)
		DebugLog(dlError, "DatabaseTableDesign::setValue field name doesn't exist in table. Table: %s, Field name: %s, Field count: %d", qPrintable(_tableName), qPrintable(fieldName), _fields.size());
	else
	{
		_fields[idx].isSet = true;
		_fields[idx].value = sValue;
		_fields[idx].noQuoteValue = noQuoteValue;
	}
}

void DatabaseTableDesign::unsetValue(const QString& fieldName)
{
	int idx = indexOfField(fieldName);
	if (idx == -1)
		DebugLog(dlError, "DatabaseTableDesign::unsetValue field name doesn't exist in table. Table: %s, Field name: %s, Field count: %d", qPrintable(_tableName), qPrintable(fieldName), _fields.size());
	else
		_fields[idx].isSet = false;
}

QString DatabaseTableDesign::setFieldNameList()
{
	bool first = true;
	QString s;
	for (int i = 0;i < _fields.size();i++)
		if (_fields.at(i).isSet)
		{
			if (!first)
				s += ",";
			else
				first = false;
			if (_dbType == "QMYSQL")
				s += _fields.at(i).name;
			else
				DebugLog(dlError, "DatabaseTableDesign::setFieldNameList database type not supported: ", qPrintable(_dbType));
		}
	return s;
}

QString DatabaseTableDesign::setFieldValueList()
{
	bool first = true;
	QString s;
	for (int i = 0;i < _fields.size();i++)
		if (_fields.at(i).isSet)
		{
			if (!first)
				s += ",";
			else
				first = false;
			if (_dbType == "QMYSQL")
			{
				if (!_fields.at(i).noQuoteValue && _fields.at(i).datatype.isStringType(true/*bIncludeDateTime*/))
					s += QString("'%1'").arg(_fields.at(i).value);
				else
					s += QString("%1").arg(_fields.at(i).value);
			}
			else
				DebugLog(dlError, "DatabaseTableDesign::setFieldValueList database type not supported: ", qPrintable(_dbType));
		}
	return s;
}

QString DatabaseTableDesign::setFieldNameValueList()
{
	bool first = true;
	QString s;
	for (int i = 0;i < _fields.size();i++)
		if (_fields.at(i).isSet)
		{
			if (!first)
				s += ",";
			else
				first = false;
			if (_dbType == "QMYSQL")
			{
				if (_fields.at(i).value.isNull())
					s += QString("%1=NULL").arg(_fields.at(i).name);
				else if (!_fields.at(i).noQuoteValue && _fields.at(i).datatype.isStringType(true/*bIncludeDateTime*/))
					s += QString("%1='%2'").arg(_fields.at(i).name).arg(_fields.at(i).value);
				else
					s += QString("%1=%2").arg(_fields.at(i).name).arg(_fields.at(i).value);
			}
			else
				DebugLog(dlError, "DatabaseTableDesign::setFieldNameValueList database type not supported: ", qPrintable(_dbType));
		}
	return s;
}

QString DatabaseTableDesign::sqlCreateTable(bool bIfNotExists /*= false*/) const
{
	QString sql;
	if (_dbType == "QMYSQL")
	{
		sql = "CREATE TABLE ";
		if (bIfNotExists)
			sql += "IF NOT EXISTS ";
		sql += _tableName;
		sql += " (";
		for (int i = 0;i < _fields.size();i++)
		{
			if (i)
				sql += ", ";
			sql += _fields.at(i).name;
			sql += " ";
			sql += _fields.at(i).datatype.typeString(_dbType);
		}
		sql += ");";
		return sql;
	}
	else DebugLog(dlError, "DatabaseTableDesign::sqlCreateTable database type not supported: ", qPrintable(_dbType));
	return sql;
}

QString DatabaseTableDesign::sqlDropTable(bool bIfExists /*= false*/) const
{
	QString sql;
	if (_dbType == "QMYSQL")
	{
		sql = "DROP TABLE ";
		if (bIfExists)
			sql += "IF EXISTS ";
		sql += _tableName;
		sql += ";";
		return sql;
	}
	else DebugLog(dlError, "DatabaseTableDesign::sqlDropTable database type not supported: ", qPrintable(_dbType));
	return sql;
}

QString DatabaseTableDesign::sqlInsert()
{
	return QString("INSERT INTO %1 (%2) VALUES (%3)").arg(_tableName).arg(setFieldNameList()).arg(setFieldValueList());
}

QString DatabaseTableDesign::sqlUpdate(const QString& sqlWhere /*= QString()*/, int limit /*= 0*/, const QString& sqlOrderLimitBy /*= QString()*/)
{
	QString sql = QString("UPDATE %1 SET %2").arg(_tableName).arg(setFieldNameValueList());
	if (!sqlWhere.isNull())
		sql += QString(" WHERE %1").arg(sqlWhere);
	if (limit)
	{
		if (!sqlOrderLimitBy.isNull())
			sql += QString(" ORDER BY %1").arg(sqlOrderLimitBy);
		sql += QString(" LIMIT %1").arg(limit);
	}
	return sql;
}

QString DatabaseTableDesign::sqlSelect(const QString& sqlWhere /*= QString()*/, const QString& sqlGroupBy /*= QString()*/, const QString& sqlOrderBy /*= QString()*/, int limit /*= 0*/, int offest /*= 0*/)
{
	QString sql = QString("SELECT %1 FROM %2").arg(setFieldNameList()).arg(_tableName);
	if (!sqlWhere.isNull())
		sql += QString(" WHERE %1").arg(sqlWhere);
	if (!sqlGroupBy.isNull())
		sql += QString(" GROUP BY %1").arg(sqlGroupBy);
	if (!sqlOrderBy.isNull())
		sql += QString(" ORDER BY %1").arg(sqlOrderBy);
	if (limit)
		sql += QString(" LIMIT %1").arg(limit);
	if (offest)
		sql += QString(" OFFSET %1").arg(offest);
	return sql;
}

QString DatabaseTableDesign::sqlDelete(const QString& sqlWhere /*= QString()*/, int limit /*= 0*/, const QString& sqlOrderLimitBy /*= QString()*/)
{
	QString sql = QString("DELETE FROM %1").arg(_tableName);
	if (!sqlWhere.isNull())
		sql += QString(" WHERE %1").arg(sqlWhere);
	if (limit)
	{
		if (!sqlOrderLimitBy.isNull())
			sql += QString(" ORDER BY %1").arg(sqlOrderLimitBy);
		sql += QString(" LIMIT %1").arg(limit);
	}
	return sql;
}
