#ifndef SQLDATABASECONFIG_H
#define SQLDATABASECONFIG_H

#include <QObject>
#include "networking/serverconfig.h"
#ifdef JSON_CONFIG
	#include <QJsonObject>
#endif

class SqlDatabaseConfig : public QObject
{
	Q_OBJECT
public:
	explicit SqlDatabaseConfig(QObject *parent = 0);
	SqlDatabaseConfig(const SqlDatabaseConfig& cpy);
	SqlDatabaseConfig& operator =(const SqlDatabaseConfig& cpy);
	void copy(const SqlDatabaseConfig& cpy, bool noChangeSignal = false);

	void setServerConfig(const ServerConfig& cfg);
	ServerConfig& serverConfig();
	const ServerConfig& serverConfig() const;

	void setDriverName(const QString& sDriver);
	const QString& driverName() const;

	void setDatabaseName(const QString& sDatabase);
	const QString& databaseName() const;

	void setConnectionString(const QString& sConnectionString);
	const QString& connectionString() const;

	bool isValid() const;

#ifdef JSON_CONFIG
	void saveJson(QJsonObject& jsonObj) const;
	bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif

protected:
	ServerConfig _serverConfig;
	QString _driverName;
	QString _databaseName;
	QString _connectionString; // contains file name and extra config for some DB driver types

signals:
	void changed();

public slots:

};

#endif // SQLDATABASECONFIG_H
