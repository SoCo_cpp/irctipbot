#ifndef PROXYSERVERCONFIG_H
#define PROXYSERVERCONFIG_H

#include <QObject>
#include "networking/serverconfig.h"
#include "networking/proxyconfig.h"

#ifdef JSON_CONFIG
	#include <QJsonObject>
#endif

class ProxyServerConfig : public QObject
{
	Q_OBJECT
public:
	explicit ProxyServerConfig(QObject *parent = 0);
	ProxyServerConfig(const ProxyServerConfig& cpy);
	ProxyServerConfig& operator =(const ProxyServerConfig& cpy);
	void copy(const ProxyServerConfig& cpy, bool noChangeSignal = false);

	void setServerConfig(const ServerConfig& cfg);
	ServerConfig& serverConfig();
	const ServerConfig& serverConfig() const;

	void setProxyConfig(const ProxyConfig& cfg);
	ProxyConfig& proxyConfig();
	const ProxyConfig& proxyConfig() const;

#ifdef JSON_CONFIG
	virtual void saveJson(QJsonObject& jsonObj) const;
	virtual bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif

protected:
	ServerConfig _serverConfig;
	ProxyConfig _proxyConfig;

signals:
	void changed();

public slots:

};

#endif // PROXYSERVERCONFIG_H
