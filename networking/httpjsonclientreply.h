#ifndef HTTPJSONCLIENTREPLY_H
#define HTTPJSONCLIENTREPLY_H

#include <QObject>
#include <QNetworkReply>
#include <QJsonDocument>

class HttpJsonClientReply : public QObject
{
	Q_OBJECT
public:
	explicit HttpJsonClientReply(QObject *parent = 0, QNetworkReply* netReply = 0, bool bAutoDeleteReply = true, bool bAutoCloseReply = true, bool bScrubURLErrors = false);
	HttpJsonClientReply(const HttpJsonClientReply& cpy);
	HttpJsonClientReply& operator =(const HttpJsonClientReply& cpy);
	void copy(const HttpJsonClientReply& cpy);

	void setNetworkReply(QNetworkReply* netReply);
	QNetworkReply* networkReply();
	const QNetworkReply* networkReply() const;

	void setAuto(bool bAutoDeleteReply = true, bool bAutoCloseReply = true);
	bool autoCloseReply() const;
	bool autoDeleteReply() const;

	void setScrubURLErrors(bool bScrub = true);
	bool scrubURLErrors() const;

	bool parseJson(const QByteArray& data);

	QJsonDocument& json();
	const QJsonDocument& json() const;

	void setErrorString(const QString& errorString);
	const QString& errorString() const;

protected:
	QNetworkReply* _netReply;
	QJsonDocument _jsonDoc;
	QString _errorString;
	bool _autoDeleteReply;
	bool _autoCloseReply;
	bool _scrubURLErrors;

signals:
	void downloadProgress(qint64 bytes, qint64 bytesTotal);
	void uploadProgress(qint64 bytes, qint64 bytesTotal);
	void finished(HttpJsonClientReply* pReply, bool success);

public slots:
	void replyFinished();

};

#endif // HTTPJSONCLIENTREPLY_H
