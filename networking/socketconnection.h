#ifndef SOCKETCONNECTION_H
#define SOCKETCONNECTION_H

#include <QObject>
#include <QtNetwork/QSslSocket>
#include "networking/proxyconfig.h"
#include "networking/serverconfig.h"

class SocketConnection : public QObject
{
	Q_OBJECT
public:
	typedef enum {None = 0, Connecting, Connected, Ready, Disconnected, Error} SocketConnectionStatus;
	explicit SocketConnection(QObject *parent = 0);

	void setStatus(SocketConnectionStatus newStatus);
	SocketConnection::SocketConnectionStatus status() const;
	QString statusString() const;

	QSslSocket& socket();

	bool connectHost();
	void disconnectHost();

	void write(const QString& sOut);
	bool canReadLine() const;
	const QByteArray& readLine(qint64 maxLen = 0);

	void setServerConfig(const ServerConfig& cfg);
	ServerConfig& serverConfig();
	const ServerConfig& serverConfig() const;

	void setProxyConfig(const ProxyConfig& cfg);
	ProxyConfig& proxyConfig();
	const ProxyConfig& proxyConfig() const;

protected:
	SocketConnectionStatus _status;
	QSslSocket _socket;

	ServerConfig _serverConfig;
	ProxyConfig _proxyConfig;

	QByteArray _readBuffer;

signals:
	void statusChanged();
	void error(QString sMessage);
	void connected();
	void disconnected();
	void ready();
	void readyRead();

protected slots:
	void onSocketConnected();
	void onSocketEncrypted();
	void onSocketError(QAbstractSocket::SocketError socketError);
	void onSocketPeerVerifyError(const QSslError & sslError);
	void onSocketDisconnected();
};

#endif // SOCKETCONNECTION_H
