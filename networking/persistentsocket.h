#ifndef PERSISTENTSOCKET_H
#define PERSISTENTSOCKET_H

#include <QObject>
#include <QTimer>
#include "networking/socketconnection.h"

class PersistentSocket : public QObject
{
	Q_OBJECT
public:
	const static int cConnectionTimerInterval		= 1000;	// msec

	typedef enum {cNone = 0, cDisconnected, cDisconnecting, cConnecting, cReady, cError, cReconnecting} ConnectionStatus;
	explicit PersistentSocket(QObject *parent = 0);

	void setConnectionStatus(ConnectionStatus cStatus);
	ConnectionStatus connectionStatus() const;
	QString connectionStatusString() const;

	bool isConnected() const;
	bool isDisconnected() const;
	bool isCleanDisconnect() const;

	void setServerConfig(const ServerConfig& cfg);
	ServerConfig& serverConfig();
	const ServerConfig& serverConfig() const;

	void setProxyConfig(const ProxyConfig& cfg);
	ProxyConfig& proxyConfig();
	const ProxyConfig& proxyConfig() const;

	bool connectClient(bool forceReconnect = false, bool failOnConnected = false);
	bool disconnectClient();

	void write(const QString& sOut);
	bool canReadLine() const;
	const QByteArray& readLine(qint64 maxLen = 0);

protected:
	bool _wantConnection;
	ConnectionStatus _connectionStatus;
	SocketConnection _connection;
	QTimer _connectionTimer;

	bool connectHost();
	void disconnectHost();

signals:
	void connectionStatusChanged();
	void readyRead();

protected slots:
	void socketConnectionStatusChanged();
	void socketConnectionConnected();
	void socketConnectionDisconnect();
	void socketConnectionError(QString strError);
	void socketConnectionReady();

	void connectionTimerTick();
};

#endif // PERSISTENTSOCKET_H
