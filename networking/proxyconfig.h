#ifndef PROXYCONFIG_H
#define PROXYCONFIG_H

#include <QNetworkProxy>
#ifdef JSON_CONFIG
	#include <QJsonObject>
#endif

class ProxyConfig : public QObject
{
	Q_OBJECT
public:
	typedef enum {None = 0, Default, Socks5, HTTP } ProxyType;
	explicit ProxyConfig(QObject *parent = 0);
	ProxyConfig(const ProxyConfig& cpy);
	ProxyConfig& operator =(const ProxyConfig& cpy);
	void copy(const ProxyConfig& cpy, bool noChangeSignal = false);

	ProxyType type() const;
	void setType(ProxyType proxyType);

	void setHost(QString sHost, quint16 iPort);
	const QString& host() const;
	quint16 port() const;

	void setUserPass(QString sUser, QString sPassword = "");
	const QString& user() const;
	const QString& password() const;

	void set(ProxyType proxyType, QString sHost, quint16 iPort, QString sUser = "", QString sPassword = "");

	bool isValid(bool requireAuth = false) const;

	const QNetworkProxy& networkProxy() const;

#ifdef JSON_CONFIG
	void saveJson(QJsonObject& jsonObj) const;
	bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif

protected:
	ProxyType _type;
	QString _host;
	quint16 _port;
	QString _user;
	QString _password;
	QNetworkProxy _networkProxy;

signals:
	void changed();

public slots:
	void updateNetworkProxy();

};

#endif // PROXYCONFIG_H
