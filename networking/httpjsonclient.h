#ifndef HTTPJSONCLIENT_H
#define HTTPJSONCLIENT_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "networking/serverconfig.h"
#include "networking/proxyconfig.h"
#include "networking/httpjsonclientreply.h"


class HttpJsonClient : public QObject
{
	Q_OBJECT
public:
	typedef enum {Default = 0, Get, Post, Put} RequestType;

	explicit HttpJsonClient(QObject *parent = 0);

	void setUrl(const QUrl& url);
	void setUrl(QString sUrl);
	const QUrl& url() const;
	QUrl& url();

	void setRequestType(RequestType type);
	RequestType requestType() const;

	void setProxyConfig(const ProxyConfig& cfg);
	ProxyConfig& proxyConfig();
	const ProxyConfig& proxyConfig() const;

	bool request(HttpJsonClientReply& jsonReply, const QByteArray& data = QByteArray(), RequestType type = Default);

protected:
	QNetworkAccessManager _netAccessManager;
	QUrl _url;
	RequestType _requestType;
	ProxyConfig _proxyConfig;

signals:

public slots:

};

#endif // HTTPJSONCLIENT_H
