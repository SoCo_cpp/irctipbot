#include <QJsonParseError>
#include "networking/httpjsonclientreply.h"
#include "logging/debuglogger.h"

HttpJsonClientReply::HttpJsonClientReply(QObject* parent /*= 0*/, QNetworkReply* netReply /*= 0*/, bool bAutoDeleteReply /*= true*/, bool bAutoCloseReply /*= true*/, bool bScrubURLErrors /*= false*/) :
	QObject(parent),
	_netReply(netReply),
	_autoDeleteReply(bAutoDeleteReply),
	_autoCloseReply(bAutoCloseReply),
	_scrubURLErrors(bScrubURLErrors)

{
	if (_netReply)
		setNetworkReply(_netReply);
}

HttpJsonClientReply::HttpJsonClientReply(const HttpJsonClientReply& cpy) :
	QObject(cpy.parent())
{
	copy(cpy);
}

HttpJsonClientReply& HttpJsonClientReply::operator =(const HttpJsonClientReply& cpy)
{
	copy(cpy);
	return *this;
}

void HttpJsonClientReply::copy(const HttpJsonClientReply& cpy)
{
	_netReply			= cpy._netReply;
	_jsonDoc			= cpy._jsonDoc;
	_errorString		= cpy._errorString;
	_autoDeleteReply	= cpy._autoDeleteReply;
	_autoCloseReply		= cpy._autoCloseReply;
	_scrubURLErrors		= cpy._scrubURLErrors;
}

void HttpJsonClientReply::setNetworkReply(QNetworkReply* netReply)
{
	_netReply = netReply;
	if (_netReply)
	{
		connect(_netReply, SIGNAL(finished()), this, SLOT(replyFinished()));
		connect(_netReply, SIGNAL(downloadProgress(qint64, qint64)), this, SIGNAL(downloadProgress(qint64, qint64)));
		connect(_netReply, SIGNAL(uploadProgress(qint64, qint64)), this, SIGNAL(uploadProgress(qint64, qint64)));
	}
}

QNetworkReply* HttpJsonClientReply::networkReply()
{
	return _netReply;
}

const QNetworkReply* HttpJsonClientReply::networkReply() const
{
	return _netReply;
}

void HttpJsonClientReply::setAuto(bool bAutoDeleteReply /*= true*/, bool bAutoCloseReply /*= true*/)
{
	_autoDeleteReply = bAutoDeleteReply;
	_autoCloseReply = bAutoCloseReply;
}

bool HttpJsonClientReply::autoCloseReply() const
{
	return _autoCloseReply;
}

bool HttpJsonClientReply::autoDeleteReply() const
{
	return _autoDeleteReply;
}

void HttpJsonClientReply::setScrubURLErrors(bool bScrub /*= true*/)
{
	_scrubURLErrors = bScrub;
}

bool HttpJsonClientReply::scrubURLErrors() const
{
	return _scrubURLErrors;
}

bool HttpJsonClientReply::parseJson(const QByteArray& data)
{
	QJsonParseError jsonParse;
	_jsonDoc = QJsonDocument::fromJson(data, &jsonParse);
	if (jsonParse.error != QJsonParseError::NoError)
	{
		_errorString = QString("ParseJson: ") + jsonParse.errorString();
		DebugLog(dlWarn, "HttpJsonClientReply::parseJson parse error: %s raw reply(%d): %s", qPrintable(jsonParse.errorString()), data.size(), qPrintable(QString(data)));
		return false;
	}
	return true;
}

void HttpJsonClientReply::replyFinished()
{
	if (_netReply)
	{
		if (_netReply->error() == QNetworkReply::NoError)
		{
			QByteArray replyData = _netReply->readAll();
			if (!parseJson(replyData))
			{
				DebugLog(dlWarn, "HttpJsonClientReply::replyFinished parseJson failed");
				_errorString = "parse replied Json failed";
				emit finished(this, false);
			}
			else emit finished(this, true);
		}
		else // if (_netReply->error() == QNetworkReply::NoError)
		{
			QString errorString = _netReply->errorString();
			if (_scrubURLErrors)
			{
				int idxStart = errorString.indexOf("http://");
				int idxEnd = errorString.indexOf(QChar(' '), idxStart);
				if (idxStart != -1 && idxEnd != -1) // strip URL from error message which could contain credentials
					errorString.remove(idxStart, idxEnd - idxStart + 1);
			}
			_errorString = QString("NetworkReply: ") + errorString;
			DebugLog(dlWarn, "HttpJsonClientReply::replyFinished reply error: %s", qPrintable(_errorString));
			emit finished(this, false);
		} // else // if (_netReply->error() == QNetworkReply::NoError)
		if (_autoDeleteReply)
			_netReply->deleteLater();
		else if (_autoCloseReply)
			_netReply->close();
	} // if (_netReply)
}

QJsonDocument& HttpJsonClientReply::json()
{
	return _jsonDoc;
}

const QJsonDocument& HttpJsonClientReply::json() const
{
	return _jsonDoc;
}

void HttpJsonClientReply::setErrorString(const QString& errorString)
{
	_errorString = errorString;
}

const QString& HttpJsonClientReply::errorString() const
{
	return _errorString;
}
