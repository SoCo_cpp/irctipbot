#include "networking/proxyserverconfig.h"
#include "logging/debuglogger.h"

ProxyServerConfig::ProxyServerConfig(QObject *parent) :
	QObject(parent)
{
	connect(&_serverConfig, SIGNAL(changed()), this, SIGNAL(changed()));
	connect(&_proxyConfig, SIGNAL(changed()), this, SIGNAL(changed()));
}

ProxyServerConfig::ProxyServerConfig(const ProxyServerConfig& cpy) :
	QObject(0)
{
	copy(cpy, true/*noChangeSignal*/);
}

ProxyServerConfig& ProxyServerConfig::operator =(const ProxyServerConfig& cpy)
{
	copy(cpy);
	return *this;
}

void ProxyServerConfig::copy(const ProxyServerConfig& cpy, bool noChangeSignal /*= false*/)
{
	_serverConfig.copy(cpy._serverConfig, true/*noChangeSignal*/);
	_proxyConfig.copy(cpy._proxyConfig, true/*noChangeSignal*/);
	if (!noChangeSignal)
		emit changed();
}

void ProxyServerConfig::setServerConfig(const ServerConfig& cfg)
{
	_serverConfig = cfg;
}

ServerConfig& ProxyServerConfig::serverConfig()
{
	return _serverConfig;
}

const ServerConfig& ProxyServerConfig::serverConfig() const
{
	return _serverConfig;
}


void ProxyServerConfig::setProxyConfig(const ProxyConfig& cfg)
{
	_proxyConfig = cfg;
}

ProxyConfig& ProxyServerConfig::proxyConfig()
{
	return _proxyConfig;
}

const ProxyConfig& ProxyServerConfig::proxyConfig() const
{
	return _proxyConfig;
}

#ifdef JSON_CONFIG
void ProxyServerConfig::saveJson(QJsonObject& jsonObj) const
{
	QJsonObject jsonServerObj;
	QJsonObject jsonProxyObj;

	_serverConfig.saveJson(jsonServerObj);
	_proxyConfig.saveJson(jsonProxyObj);

	jsonObj["ver-psc"]	= 1; // use non-standard version key because derived class will have its own
	jsonObj["server"]	= jsonServerObj;
	jsonObj["proxy"]	= jsonProxyObj;
}
#endif // #ifdef JSON_CONFIG

#ifdef JSON_CONFIG
bool ProxyServerConfig::loadJson(const QJsonObject& jsonObj, bool bRequireAllFields /*= false*/, bool noChangeSignal /*= false*/)
{
	copy(ProxyServerConfig(), true/*noChangeSignal*/);
	if (bRequireAllFields)
		if ( !jsonObj.contains("server") || !jsonObj.contains("proxy") )
		{
			DebugLog(dlWarn, "ProxyServerConfig::loadJson all fields required, bot not all found");
			return false;
		}

	if (!jsonObj.contains("ver-psc"))
	{
		DebugLog(dlWarn, "ProxyServerConfig::loadJson version field missing");
		return false;
	}

	if (jsonObj.contains("server"))
	{
		if (!jsonObj["server"].isObject())
		{
			DebugLog(dlWarn, "ProxyServerConfig::loadJson server field is not an object");
			return false;
		}
		if (!_serverConfig.loadJson(jsonObj["server"].toObject(), bRequireAllFields, true/*noChangeSignal*/))
		{
			DebugLog(dlWarn, "ProxyServerConfig::loadJson server config failed to loadJSon");
			return false;
		}
	}

	if (jsonObj.contains("proxy"))
	{
		if (!jsonObj["proxy"].isObject())
		{
			DebugLog(dlWarn, "ProxyServerConfig::loadJson proxy field is not an object");
			return false;
		}
		if (!_proxyConfig.loadJson(jsonObj["proxy"].toObject(), bRequireAllFields, true/*noChangeSignal*/))
		{
			DebugLog(dlWarn, "ProxyServerConfig::loadJson proxy config failed to loadJSon");
			return false;
		}
	}

	if (!noChangeSignal)
		emit changed();
	return true;

}
#endif // #ifdef JSON_CONFIG
