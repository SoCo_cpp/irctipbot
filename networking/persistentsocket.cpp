#include "networking/persistentsocket.h"
#include "logging/debuglogger.h"

PersistentSocket::PersistentSocket(QObject *parent) :
	QObject(parent),
	_wantConnection(false),
	_connectionStatus(PersistentSocket::cNone)
{
	connect(&_connection, SIGNAL(statusChanged()), this, SLOT(socketConnectionStatusChanged()));
	connect(&_connection, SIGNAL(error(QString)), this, SLOT(socketConnectionError(QString)));
	connect(&_connection, SIGNAL(connected()), this, SLOT(socketConnectionConnected()));
	connect(&_connection, SIGNAL(disconnected()), this, SLOT(socketConnectionDisconnect()));
	connect(&_connection, SIGNAL(ready()), this, SLOT(socketConnectionReady()));
	connect(&_connection, SIGNAL(readyRead()), this, SIGNAL(readyRead()));

	connect(&_connectionTimer, SIGNAL(timeout()), this, SLOT(connectionTimerTick()));
	_connectionTimer.start(cConnectionTimerInterval);
}

void PersistentSocket::setConnectionStatus(PersistentSocket::ConnectionStatus cStatus)
{
	_connectionStatus = cStatus;
	emit connectionStatusChanged();
}

PersistentSocket::ConnectionStatus PersistentSocket::connectionStatus() const
{
	return _connectionStatus;
}

QString PersistentSocket::connectionStatusString() const
{
	switch (_connectionStatus)
	{
		case cNone:				return "None";
		case cDisconnected:		return "Disconnected";
		case cDisconnecting:	return "Disconnecting";
		case cConnecting:		return "Connecting";
		case cReady:			return "Ready";
		case cError:			return "Error";
		case cReconnecting:		return "Reconnecting";
		default:				return QString("Unknown(%1)").arg(_connectionStatus);
	} // switch
}

bool PersistentSocket::isConnected() const
{
	return (_connectionStatus == cReady);
}

bool PersistentSocket::isDisconnected() const
{
	switch (_connectionStatus)
	{
		case cNone:
		case cDisconnected:
		case cError:
			return true;
		default:
			return false;
	} // switch (_connectionStatus)
}

bool PersistentSocket::isCleanDisconnect() const
{
	return (_connectionStatus == cNone || _connectionStatus == cDisconnected);
}


void PersistentSocket::setServerConfig(const ServerConfig& cfg)
{
	_connection.setServerConfig(cfg);
}

ServerConfig& PersistentSocket::serverConfig()
{
	return _connection.serverConfig();
}

const ServerConfig& PersistentSocket::serverConfig() const
{
	return _connection.serverConfig();
}

void PersistentSocket::setProxyConfig(const ProxyConfig& cfg)
{
	_connection.setProxyConfig(cfg);
}

ProxyConfig& PersistentSocket::proxyConfig()
{
	return _connection.proxyConfig();
}

const ProxyConfig& PersistentSocket::proxyConfig() const
{
	return _connection.proxyConfig();
}

bool PersistentSocket::connectClient(bool forceReconnect /*= false*/, bool failOnConnected /*= false*/)
{
	if (!isDisconnected())
	{
		if (forceReconnect && failOnConnected)
		{
			DebugLog(dlError, "PersistentSocket::connectClient parameter error, cannot use both forceReconnect and failOnConnected");
			return false;
		}
		if (failOnConnected)
			return false; // already connected, fail expected, no error
		if (!forceReconnect)
			return true; // nothing to do, already connected
		disconnectHost();
	} // if (!isDisconnected())
	_wantConnection = true;
	return true;
}

bool PersistentSocket::disconnectClient()
{
	_wantConnection = false;
	return true;
}

bool PersistentSocket::connectHost()
{
	if (!isDisconnected())
	{
		DebugLog(dlError, "PersistentSocket::connectHost invalid connection state: %s", qPrintable(connectionStatusString()));
		return false; // invalid state
	}
	setConnectionStatus(cConnecting);
	if (!_connection.connectHost())
	{
		DebugLog(dlWarn, "PersistentSocket::connectHost connection failed to connectHost");
		return false;
	}
	return true;
}

void PersistentSocket::disconnectHost()
{
	_wantConnection = false;
	setConnectionStatus(cDisconnecting);
	_connection.disconnectHost();
}

void PersistentSocket::write(const QString& sOut)
{
	_connection.write(sOut);
}

bool PersistentSocket::canReadLine() const
{
	return _connection.canReadLine();
}

const QByteArray& PersistentSocket::readLine(qint64 maxLen /*= 0*/)
{
	return _connection.readLine(maxLen);
}

void PersistentSocket::socketConnectionStatusChanged()
{
	// use this only for information, use individual slots instead for handling
	DebugLog(dlDebug, "PersistentSocket connection status changed to: %s", qPrintable(_connection.statusString()));
}

void PersistentSocket::socketConnectionConnected()
{
	// use as notification only, wait for socketConnectionReady
	DebugLog(dlDebug, "PersistentSocket connection: Connected");
}

void PersistentSocket::socketConnectionDisconnect()
{
	DebugLog(dlDebug, "PersistentSocket connection: Disconnected");
	setConnectionStatus(cDisconnected);
}

void PersistentSocket::socketConnectionError(QString strError)
{
	DebugLog(dlInfo, "PersistentSocket connection error: %s", qPrintable(strError));
	setConnectionStatus(cError);
}

void PersistentSocket::socketConnectionReady()
{
	DebugLog(dlDebug, "PersistentSocket connection: Ready");
	setConnectionStatus(cReady);
}

void PersistentSocket::connectionTimerTick()
{
	if (_wantConnection && isDisconnected())
	{
		if (isCleanDisconnect())
		{
			DebugLog(dlDebug, "PersistentSocket::connectionTimerTick want connection, disconnected, connecting on clean disconnect");

			if (!connectHost())
			{
				DebugLog(dlWarn, "PersistentSocket::connectionTimerTick connectHost failed");
				disconnectHost();
				return;
			}
		}
		else // if (isCleanDisconnect())
		{
			DebugLog(dlDebug, "PersistentSocket::connectionTimerTick want connection, disconnected, connecting on dirty disconnect");
			setConnectionStatus(cReconnecting);
			if (!connectClient(true /*forceReconnect*/))
			{
				DebugLog(dlWarn, "PersistentSocket::connectionTimerTick connectClient failed");
				return;
			}
		} // else // if (isCleanDisconnect())
	}
	else  if (!_wantConnection && isConnected())
	{
		DebugLog(dlDebug, "PersistentSocket::connectionTimerTick don't want connection, connected, disconnecting");
		disconnectHost();
	} // else // if (_wantConnection)
}

