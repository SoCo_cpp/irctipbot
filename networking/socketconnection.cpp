#include "networking/socketconnection.h"
#include "logging/debuglogger.h"

SocketConnection::SocketConnection(QObject *parent) :
	QObject(parent),
	_status(SocketConnection::None)
{
	connect(&_socket, SIGNAL(connected()), this, SLOT(onSocketConnected()));
	connect(&_socket, SIGNAL(encrypted()), this, SLOT(onSocketEncrypted()));
	connect(&_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onSocketError(QAbstractSocket::SocketError)));
	connect(&_socket, SIGNAL(peerVerifyError(const QSslError &)), this, SLOT(onSocketPeerVerifyError(const QSslError &)));
	connect(&_socket, SIGNAL(disconnected()), this, SLOT(onSocketDisconnected()));
	connect(&_socket, SIGNAL(readyRead()), this, SIGNAL(readyRead()));
}

void SocketConnection::setStatus(SocketConnectionStatus newStatus)
{
	_status = newStatus;
}

SocketConnection::SocketConnectionStatus SocketConnection::status() const
{
	return _status;
}

QString SocketConnection::statusString() const
{
	switch (_status)
	{
		case None:			return "None";
		case Connecting:	return "Connecting";
		case Connected:		return "Connected";
		case Ready:			return "Ready";
		case Disconnected:	return "Disconnected";
		case Error:			return "Error";
		default:			return QString("Unknown(%1)").arg((int)_status);
	} // switch
}

QSslSocket& SocketConnection::socket()
{
	return _socket;
}

bool SocketConnection::connectHost()
{
	if (_serverConfig.host().isEmpty())
	{
		DebugLog(dlWarn, "SocketConnection::connectHost server host is empty");
		return false;
	}
	_socket.setProxy(_proxyConfig.networkProxy());
	if (_serverConfig.isSSL())
		_socket.connectToHostEncrypted(_serverConfig.host(), _serverConfig.port());
	else
		_socket.connectToHost(_serverConfig.host(), _serverConfig.port());
	return true;
}

void SocketConnection::disconnectHost()
{
	_socket.disconnectFromHost();
}

void SocketConnection::write(const QString& sOut)
{
	qint64 l = _socket.write(sOut.toUtf8());
	if (l == -1)
	{
		DebugLog(dlWarn, "SocketConnection::write write failed");
		emit error("write failed");
	}
}

bool SocketConnection::canReadLine() const
{
	return _socket.canReadLine();
}

const QByteArray& SocketConnection::readLine(qint64 maxLen /*= 0*/)
{
	_readBuffer = _socket.readLine(maxLen);
	return _readBuffer;
}

void SocketConnection::onSocketConnected()
{
	if (_serverConfig.isSSL())
	{
		setStatus(Connected);
		emit connected();
	}
	else
	{
		setStatus(Ready);
		emit connected();
		emit ready();
	}
}

void SocketConnection::onSocketEncrypted()
{
	if (_serverConfig.isSSL())
	{
		setStatus(Ready);
		emit ready();
	}
}

void SocketConnection::onSocketError(QAbstractSocket::SocketError socketError)
{
	Q_UNUSED(socketError);
	setStatus(Error);
	emit error(_socket.errorString());
	emit disconnected();
}

void SocketConnection::onSocketPeerVerifyError(const QSslError & sslError)
{
	if (_serverConfig.ignoreSSLInvalid())
	{
		_socket.ignoreSslErrors();
	}
	else emit error(sslError.errorString());
}

void SocketConnection::onSocketDisconnected()
{
	setStatus(Disconnected);
	emit disconnected();
}

void SocketConnection::setServerConfig(const ServerConfig& cfg)
{
	_serverConfig = cfg;
}

ServerConfig& SocketConnection::serverConfig()
{
	return _serverConfig;
}

const ServerConfig& SocketConnection::serverConfig() const
{
	return _serverConfig;
}

void SocketConnection::setProxyConfig(const ProxyConfig& cfg)
{
	_proxyConfig = cfg;
}

ProxyConfig& SocketConnection::proxyConfig()
{
	return _proxyConfig;
}

const ProxyConfig& SocketConnection::proxyConfig() const
{
	return _proxyConfig;
}
