#ifndef SERVERCONFIG_H
#define SERVERCONFIG_H

#include <QObject>
#include <QUrl>
#ifdef JSON_CONFIG
	#include <QJsonObject>
#endif

class ServerConfig : public QObject
{
	Q_OBJECT
public:
	explicit ServerConfig(QObject *parent = 0);
	ServerConfig(const ServerConfig& cpy);
	ServerConfig& operator =(const ServerConfig& cpy);
	void copy(const ServerConfig& cpy, bool noChangeSignal = false);

	bool isSSL() const;
	void setSSL(bool bEnabled);

	bool ignoreSSLInvalid() const;
	void setIgnoreSSLInvalid(bool bIgnore = true);

	void setHost(QString sHost, quint16 iPort);
	const QString& host() const;
	quint16 port() const;

	void set(const QString& sHost, quint16 iPort, bool bSSL = false, const QString& user = "", const QString& password = "", bool bIgnoreSSLInvalid = false);

	void setUser(const QString& user);
	const QString& user() const;

	void setPassword(const QString& password);
	const QString& password() const;

	bool isValid(bool requireAuth = false) const;

	QUrl httpUrl() const;

#ifdef JSON_CONFIG
	void saveJson(QJsonObject& jsonObj) const;
	bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif

protected:
	bool _SSL;
	bool _ignoreInvalidSSL;
	QString _host;
	quint16 _port;
	QString _user;
	QString _password;

signals:
	void changed();

public slots:

};

#endif // SERVERCONFIG_H
