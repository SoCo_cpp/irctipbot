#include "networking/serverconfig.h"
#include "logging/debuglogger.h"

ServerConfig::ServerConfig(QObject *parent) :
	QObject(parent),
	_SSL(false),
	_ignoreInvalidSSL(false),
	_host(""),
	_port(80),
	_user(""),
	_password("")
{
}

ServerConfig::ServerConfig(const ServerConfig& cpy) :
	QObject(0)
{
	copy(cpy, true /*noChangeSignal*/);
}

ServerConfig& ServerConfig::operator =(const ServerConfig& cpy)
{
	copy(cpy);
	return *this;
}

void ServerConfig::copy(const ServerConfig& cpy, bool noChangeSignal /*= false*/)
{
	_SSL				= cpy._SSL;
	_ignoreInvalidSSL	= cpy._ignoreInvalidSSL;
	_host				= cpy._host;
	_port				= cpy._port;
	_user				= cpy._user;
	_password			= cpy._password;
	if (!noChangeSignal)
		emit changed();
}

bool ServerConfig::isSSL() const
{
	return _SSL;
}

void ServerConfig::setSSL(bool bEnabled)
{
	_SSL = bEnabled;
}

bool ServerConfig::ignoreSSLInvalid() const
{
	return _ignoreInvalidSSL;
}

void ServerConfig::setIgnoreSSLInvalid(bool bIgnore /*= true*/)
{
	_ignoreInvalidSSL = bIgnore;
}

void ServerConfig::setHost(QString sHost, quint16 iPort)
{
	_host = sHost;
	_port = iPort;
	emit changed();
}

const QString& ServerConfig::host() const
{
	return _host;
}

quint16 ServerConfig::port() const
{
	return _port;
}

void ServerConfig::set(const QString& sHost, quint16 iPort, bool bSSL /*= false*/, const QString& user /*= ""*/, const QString& password /*= ""*/, bool bIgnoreSSLInvalid /*= false*/)
{
	_host = sHost;
	_port = iPort;
	_SSL = bSSL;
	_user = user;
	_password = password;
	_ignoreInvalidSSL = bIgnoreSSLInvalid;
	emit changed();
}

void ServerConfig::setUser(const QString& user)
{
	_user = user;
	emit changed();
}

const QString& ServerConfig::user() const
{
	return _user;
}

void ServerConfig::setPassword(const QString& password)
{
	_password = password;
	emit changed();
}

const QString& ServerConfig::password() const
{
	return _password;
}

bool ServerConfig::isValid(bool requireAuth /*= false*/) const
{
	if (_host.isEmpty())
		return false;
	if (requireAuth && (_user.isEmpty() || _password.isEmpty()))
		return false;
	return true;
}

QUrl ServerConfig::httpUrl() const
{
	QUrl url;
	url.setHost(_host);
	url.setPort(_port);
	url.setUserName(_user);
	url.setPassword(_password);
	if (_SSL)
		url.setScheme("https");
	else
		url.setScheme("http");
	return url;
}

#ifdef JSON_CONFIG
void ServerConfig::saveJson(QJsonObject& jsonObj) const
{
	jsonObj["ver"]					= 1;
	jsonObj["host"]					= _host;
	jsonObj["port"]					= (int)_port;
	jsonObj["ssl"]					= _SSL;
	jsonObj["ignore-invalid-ssl"]	= _ignoreInvalidSSL;
	jsonObj["user"]					= _user;
	jsonObj["password"]				= _password;
}
#endif // #ifdef JSON_CONFIG

#ifdef JSON_CONFIG
bool ServerConfig::loadJson(const QJsonObject& jsonObj, bool bRequireAllFields /*= false*/, bool noChangeSignal /*= false*/)
{
	copy(ServerConfig(), true/*noChangeSignal*/);
	if (bRequireAllFields)
		if ( !jsonObj.contains("host") || !jsonObj.contains("port") || !jsonObj.contains("ssl") || !jsonObj.contains("ignore-invalid-ssl")  || !jsonObj.contains("user")  || !jsonObj.contains("password") )
		{
			DebugLog(dlWarn, "ServerConfig::loadJson all fields required, bot not all found");
			return false;
		}

	if (!jsonObj.contains("ver"))
	{
		DebugLog(dlWarn, "ServerConfig::loadJson version field missing");
		return false;
	}

	if (jsonObj.contains("host"))
		_host = jsonObj["host"].toString();

	if (jsonObj.contains("port"))
		_port = (unsigned int)jsonObj["port"].toInt();

	if (jsonObj.contains("ssl"))
		_user = jsonObj["ssl"].toBool();

	if (jsonObj.contains("ignore-invalid-ssl"))
		_password = jsonObj["ignore-invalid-ssl"].toBool();

	if (jsonObj.contains("user"))
		_user = jsonObj["user"].toString();

	if (jsonObj.contains("password"))
		_password = jsonObj["password"].toString();

	if (!noChangeSignal)
		emit changed();
	return true;
}
#endif // #ifdef JSON_CONFIG
