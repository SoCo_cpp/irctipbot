#include "networking/proxyconfig.h"
#include "logging/debuglogger.h"

ProxyConfig::ProxyConfig(QObject *parent) :
	QObject(parent),
	_type(Default),
	_host(""),
	_port(9050),
	_user(""),
	_password("")

{
	updateNetworkProxy();
	connect(this, SIGNAL(changed()), this, SLOT(updateNetworkProxy()));
}

ProxyConfig::ProxyConfig(const ProxyConfig& cpy) :
	QObject(0)
{
	copy(cpy, true /*noChangeSignal*/);
	updateNetworkProxy();
	connect(this, SIGNAL(changed()), this, SLOT(updateNetworkProxy()));
}

ProxyConfig& ProxyConfig::operator =(const ProxyConfig& cpy)
{
	copy(cpy);
	return *this;
}

void ProxyConfig::copy(const ProxyConfig& cpy, bool noChangeSignal /*= false*/)
{
	_type		= cpy._type;
	_host		= cpy._host;
	_port		= cpy._port;
	_user		= cpy._user;
	_password	= cpy._password;
	if (!noChangeSignal)
		emit changed();
}

ProxyConfig::ProxyType ProxyConfig::type() const
{
	return _type;
}

void ProxyConfig::setType(ProxyType proxyType)
{
	_type = proxyType;
	emit changed();
}

void ProxyConfig::setHost(QString sHost, quint16 iPort)
{
	_host = sHost;
	_port = iPort;
	emit changed();
}

const QString& ProxyConfig::host() const
{
	return _host;
}

quint16 ProxyConfig::port() const
{
	return _port;
}

void ProxyConfig::setUserPass(QString sUser, QString sPassword /*= ""*/)
{
	_user = sUser;
	_password = sPassword;
	emit changed();
}

const QString& ProxyConfig::user() const
{
	return _user;
}

const QString& ProxyConfig::password() const
{
	return _password;
}

void ProxyConfig::set(ProxyType proxyType, QString sHost, quint16 iPort, QString sUser /*= ""*/, QString sPassword /*= ""*/)
{
	_type = proxyType;
	_host = sHost;
	_port = iPort;
	_user = sUser;
	_password = sPassword;
	emit changed();

}

const QNetworkProxy& ProxyConfig::networkProxy() const
{
	return _networkProxy;
}

bool ProxyConfig::isValid(bool requireAuth /*= false*/) const
{
	if (_type == None || _type == Default)
		return true;
	if (requireAuth && (_user.isEmpty() || _password.isEmpty()))
		return false;
	if (_host.isEmpty())
		return false;
	return true;
}

void ProxyConfig::updateNetworkProxy()
{
	switch (_type)
	{
		case Default:
			_networkProxy = QNetworkProxy::applicationProxy();
			return; // change nothing else
		case None:		_networkProxy.setType(QNetworkProxy::NoProxy);			break;
		case Socks5:	_networkProxy.setType(QNetworkProxy::Socks5Proxy);		break;
		case HTTP:		_networkProxy.setType(QNetworkProxy::HttpProxy);		break;
	} // switch
	_networkProxy.setHostName(_host);
	_networkProxy.setPort(_port);
	_networkProxy.setUser(_user);
	_networkProxy.setPassword(_password);
}

#ifdef JSON_CONFIG
void ProxyConfig::saveJson(QJsonObject& jsonObj) const
{
	jsonObj["ver"]		= 1;
	jsonObj["type"]		= (int)_type;
	jsonObj["host"]		= _host;
	jsonObj["port"]		= (int)_port;
	jsonObj["user"]		= _user;
	jsonObj["password"] = _password;
}
#endif // #ifdef JSON_CONFIG

#ifdef JSON_CONFIG
bool ProxyConfig::loadJson(const QJsonObject& jsonObj, bool bRequireAllFields /*= false*/, bool noChangeSignal /*= false*/)
{
	copy(ProxyConfig(), true/*noChangeSignal*/);
	if (bRequireAllFields)
		if ( !jsonObj.contains("type") || !jsonObj.contains("host") || !jsonObj.contains("port") || !jsonObj.contains("user") || !jsonObj.contains("password") )
		{
			DebugLog(dlWarn, "ProxyConfig::loadJson all fields required, bot not all found");
			return false;
		}

	if (!jsonObj.contains("ver"))
	{
		DebugLog(dlWarn, "ProxyConfig::loadJson version field missing");
		return false;
	}


	if (jsonObj.contains("type"))
	{
		_type = (ProxyType)jsonObj["type"].toInt();
		if (_type < 0 || _type > HTTP)
		{
			DebugLog(dlWarn, "ProxyConfig::loadJson invalid type specified: %d, setting to Default", (int)_type);
			_type = Default;
		}
	}

	if (jsonObj.contains("host"))
		_host = jsonObj["host"].toString();

	if (jsonObj.contains("port"))
		_port = (unsigned int)jsonObj["port"].toInt();

	if (jsonObj.contains("user"))
		_user = jsonObj["user"].toString();

	if (jsonObj.contains("password"))
		_password = jsonObj["password"].toString();

	if (!noChangeSignal)
		emit changed();
	return true;

}
#endif // #ifdef JSON_CONFIG
