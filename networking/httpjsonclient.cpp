#include "networking/httpjsonclient.h"
#include "logging/debuglogger.h"

HttpJsonClient::HttpJsonClient(QObject *parent) :
	QObject(parent)
{
}

void HttpJsonClient::setUrl(const QUrl& url)
{
	_url = url;
}

void HttpJsonClient::setUrl(QString sUrl)
{
	_url = sUrl;
}

const QUrl& HttpJsonClient::url() const
{
	return _url;
}

QUrl& HttpJsonClient::url()
{
	return _url;
}

void HttpJsonClient::setRequestType(RequestType type)
{
	_requestType = type;
}

HttpJsonClient::RequestType HttpJsonClient::requestType() const
{
	return _requestType;
}

void HttpJsonClient::setProxyConfig(const ProxyConfig& cfg)
{
	_proxyConfig = cfg;
}

ProxyConfig& HttpJsonClient::proxyConfig()
{
	return _proxyConfig;
}

const ProxyConfig& HttpJsonClient::proxyConfig() const
{
	return _proxyConfig;
}

bool HttpJsonClient::request(HttpJsonClientReply& jsonReply, const QByteArray& data /*= QByteArray()*/, RequestType type /*= Default*/)
{
	if (!_url.isValid())
	{
		DebugLog(dlWarn, "HttpJsonClient::request url is invalid: %s", qPrintable(_url.toString()));
		return false;
	}
	QNetworkRequest netRequest(_url);
	netRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json-rpc");

	_netAccessManager.setProxy(_proxyConfig.networkProxy());

	if (type == Default)
		type = _requestType;

	switch (type)
	{
		case Get:	jsonReply.setNetworkReply(_netAccessManager.get(netRequest));			break;
		case Post:	jsonReply.setNetworkReply(_netAccessManager.post(netRequest, data));	break;
		case Put:	jsonReply.setNetworkReply(_netAccessManager.put(netRequest, data));		break;
		case Default:
			DebugLog(dlError, "HttpJsonClient::request inheritted default request type, no base to inherit");
			return false;
		default:
			DebugLog(dlError, "HttpJsonClient::request invalid request type: %d", (int)type);
			return false;
	} // switch
	return true;
}
