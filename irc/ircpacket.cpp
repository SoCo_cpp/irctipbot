#include "irc/ircpacket.h"
#include "logging/debuglogger.h"

IrcPacket::IrcPacket(QObject *parent, const QString& packet /*= QString()*/) :
	QObject(parent),
	_commandInt(-1)
{
	if (!packet.isNull())
		parse(packet);
}

void IrcPacket::parse(const QString& packet)
{
	_packet = packet; // must keep copy of original to have all references
	if (_packet.endsWith("\r\n"))
		_packet.chop(2);

	_commandInt = -1;
	_nick.clear();
	_prefix.clear();
	_command.clear();
	_middle.clear();
	_params.clear();
	_trailing.clear();


	bool bOk;
	int startIdx = 0;
	int nextIdx = 0;
	int len = _packet.length();

	if (len == 0)
		return; // empty packet

	if (_packet[0] == QChar(':')) // has prefix
	{
		nextIdx = _packet.indexOf(QChar(' '));
		_prefix = _packet.midRef(1, nextIdx - 1);
		startIdx = nextIdx + 1;
		nextIdx = _prefix.indexOf(QChar('!'));
		if (nextIdx != -1)
			_nick = _packet.midRef(1, nextIdx);
	}
	nextIdx = _packet.indexOf(QChar(' '), startIdx);
	_command = _packet.midRef(startIdx, nextIdx - startIdx);
	startIdx = nextIdx + 1;

	nextIdx = _packet.indexOf(QChar(' '), startIdx);
	if (nextIdx == -1)
		_middle = _packet.midRef(startIdx, len - startIdx);
	else
	{
		_middle = _packet.midRef(startIdx, nextIdx - startIdx);
		startIdx = nextIdx + 1;

		if (_packet[startIdx] == QChar(':')) // has no params, just trailing
		{
			startIdx++; // skip over :
			_trailing = _packet.midRef(startIdx, len - startIdx);
		}
		else
		{
			nextIdx = _packet.indexOf(QChar(':'), startIdx);
			if (nextIdx == -1)
			{
				_params = _packet.midRef(startIdx, len - startIdx);
				nextIdx = len;
			}
			else
			{
				nextIdx--; // trim space before :
				_params = _packet.midRef(startIdx, nextIdx - startIdx);
				startIdx = nextIdx + 2; // skip space and :
				_trailing = _packet.midRef(startIdx, len - startIdx);
			}
		}
	}
	_commandInt = _command.toInt(&bOk);
	if (!bOk)
		_commandInt = -1;
}

const QString& IrcPacket::packet() const
{
	return _packet;
}

const QStringRef& IrcPacket::nick() const
{
	return _nick;
}

const QStringRef& IrcPacket::prefix() const
{
	return _prefix;
}

const QStringRef& IrcPacket::command() const
{
	return _command;
}

int IrcPacket::commandInt(int def /*= -1*/) const
{
	if (_commandInt == -1)
		return def;
	return _commandInt;
}

const QStringRef& IrcPacket::middle() const
{
	return _middle;
}

const QStringRef& IrcPacket::trailing() const
{
	return _trailing;
}

const QStringRef& IrcPacket::params() const
{
	return _params;
}

bool IrcPacket::hasNick() const
{
	return !_nick.isNull();
}

bool IrcPacket::hasPrefix() const
{
	return !_prefix.isNull();
}

bool IrcPacket::hasTrailing() const
{
	return _trailing.isNull();
}

bool IrcPacket::hasParams() const
{
	return _params.isNull();
}

QStringList IrcPacket::paramList() const
{
	return _params.string()->split(QChar(' '), QString::SkipEmptyParts);
}

bool IrcPacket::isEmpty() const
{
	return _packet.isNull();
}

