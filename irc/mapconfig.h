#ifndef MAPCONFIG_H
#define MAPCONFIG_H

#include <QObject>
#include <QMap>

class MapConfig : public QObject
{
	Q_OBJECT
public:
	explicit MapConfig(QObject *parent = 0);
	MapConfig& operator =(const MapConfig& cpy);
	void copy(const MapConfig& cpy);

	void beginUpdate();
	void endUpdate();
	bool isUpdating() const;

	void setModified();
	bool isModified() const;

	QString config(const QString& sName) const;
	int configInt(const QString& sName, int def = -1) const;
	bool configBool(const QString& sName, bool def = false) const;

	void setConfig(const QString& sName, const QString& sValue);
	void setConfig(const QString& sName, int iValue);
	void setConfig(const QString& sName, bool bValue);
	bool hasConfig(const QString& sName) const;

protected:
	bool _modified;
	unsigned int _updatingRef;
	QMap<QString, QString> _configs;

signals:
	void modified();

public slots:

};

#endif // MAPCONFIG_H
