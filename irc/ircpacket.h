#ifndef IRCPACKET_H
#define IRCPACKET_H

#include <QObject>
#include <QStringList>

class IrcPacket : public QObject
{
	Q_OBJECT
public:
	explicit IrcPacket(QObject *parent = 0, const QString& packet = QString());

	void parse(const QString& packet);

	const QString& packet() const;
	const QStringRef& nick() const;
	const QStringRef& prefix() const;
	const QStringRef& command() const;
	int commandInt(int def = -1) const;
	const QStringRef& middle() const;
	const QStringRef& trailing() const;
	const QStringRef& params() const;
	QStringList paramList() const;

	bool hasNick() const;
	bool hasPrefix() const;
	bool hasTrailing() const;
	bool hasParams() const;
	bool isEmpty() const;

protected:
	QString		_packet;
	QStringRef  _nick;
	QStringRef	_prefix;
	QStringRef	_command;
	QStringRef	_middle;
	QStringRef	_trailing;
	QStringRef	_params;
	int			_commandInt;

signals:

public slots:

};

#endif // IRCPACKET_H
