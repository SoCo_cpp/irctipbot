#ifndef IRCHANDLER_H
#define IRCHANDLER_H

#include <QObject>
#include <QTimer>
#include <QThread>
#include "irc/ircpacket.h"

class IrcClient; // predeclare
class IrcHandler : public QObject
{
	Q_OBJECT
public:
	const static int cHandleInputTimerInterval = 30;	// msec
	const static qint64 cMaxReadLineCharacters = 512;	// IRC protocol specification

	explicit IrcHandler(QObject*, IrcClient& ircClient);
	~IrcHandler();

	void sendLine(const QString& str);

protected:
	IrcClient& _ircClient;
	QTimer _handleInputTimer;
	IrcPacket _ircPacket;
	QThread _thread;

	void handlePacket();
	void handleNumericCommandPacket();
	void handleTextCommandPacket();

signals:
	// Internal/Handler-to-Client Signals
	void setLoginStatus(int);
	void send(QString str);
	void sendNickUserLogin();
	void sendAuthenticateSASL();
	void sendJoin(QString sChannel);
	void sendPart(QString sChannel, QString sMessage);
	void sendPrivateMessage(QString sNickChannel, QString sMessage);
	void sendWhois(QString sNick);
	void sendQuit(QString sMessage);
	// External Signals
	void receivedPong(QString nick, QString user, QString target, QString message);
	void receivedNotice(QString nick, QString user, QString target, QString message);
	void receivedPrivmsg(QString nick, QString user, QString target, QString message);
	void receivedJoin(QString nick, QString user, QString channel);
	void receivedPart(QString nick, QString user, QString channel, QString message);
	void receivedQuit(QString nick, QString user, QString message);
	void receivedNick(QString nick, QString user,QString newNick);
	void receivedMode(QString nick, QString user, QString target, QString params);
	void receivedTopic(QString nick, QString user, QString target, QString message);
	void receivedKick(QString nick, QString user, QString channel, QString target, QString message);

public slots:
	void handleInputTimerTick();

};

#endif // IRCHANDLER_H
