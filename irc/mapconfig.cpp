#include "mapconfig.h"

MapConfig::MapConfig(QObject *parent) :
	QObject(parent),
	_modified(false),
	_updatingRef(0)
{
}

MapConfig& MapConfig::operator =(const MapConfig& cpy)
{
	copy(cpy);
	return *this;
}

void MapConfig::copy(const MapConfig& cpy)
{
	QMapIterator<QString, QString> itr(cpy._configs);
	beginUpdate();
	_modified = true;
	_configs.clear();
	while (itr.hasNext())
	{
		itr.next();
		_configs[itr.key()] = itr.value();
	}
	endUpdate();
}

void MapConfig::beginUpdate()
{
	_updatingRef++;
}

void MapConfig::endUpdate()
{
	if (_updatingRef)
		_updatingRef--;
	if (!_updatingRef && _modified)
	{
		_modified = false;
		emit modified();
	}
}

bool MapConfig::isUpdating() const
{
	return (_updatingRef != 0);
}

void MapConfig::setModified()
{
	if (!_updatingRef)
	{
		_modified = false;
		emit modified();
	}
	else _modified = true;

}

bool MapConfig::isModified() const
{
	return _modified;
}

QString MapConfig::config(const QString& sName) const
{
	return _configs[sName];
}

int MapConfig::configInt(const QString& sName, int def /*= -1*/) const
{
	bool isOk;
	int i =  _configs[sName].toInt(&isOk);
	return (isOk ? i : def);
}

bool MapConfig::configBool(const QString& sName, bool def /*= false*/) const
{
	return (_configs[sName] == "1" ? true : def);
}

void MapConfig::setConfig(const QString& sName, const QString& sValue)
{
	_configs[sName] = sValue;
	setModified();
}

void MapConfig::setConfig(const QString& sName, int iValue)
{
	setConfig(sName, QString(iValue));
}

void MapConfig::setConfig(const QString& sName, bool bValue)
{
	setConfig(sName, (bValue ? "1" : "0"));
}

bool MapConfig::hasConfig(const QString& sName) const
{
	return _configs.contains(sName);
}

