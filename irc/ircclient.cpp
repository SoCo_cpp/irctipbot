#include "irc/ircclient.h"
#include "logging/debuglogger.h"

IrcClient::IrcClient(QObject *parent) :
	QObject(parent),
	_handler(0, *this),
	_clientStatus(cNone),
	_loginStatus(lNone)
{
	moveToThread(&_thread);
	_thread.start();


	connect(&_config, SIGNAL(changed()), this, SLOT(configChanged()));
	connect(&_connection, SIGNAL(connectionStatusChanged()), this, SLOT(connectionStatusChanged()));
	connect(&_connection, SIGNAL(readyRead()), this, SLOT(connectionReadyRead()));
	connect(&_outputTimer, SIGNAL(timeout()), this, SLOT(outputTimerTick()));
	connect(&_loginTimer, SIGNAL(timeout()), this, SLOT(loginTimerTick()));
	_outputTimer.start(cNotReadyOutputTimerInterval);
}

IrcClient::~IrcClient()
{
	_thread.quit();
	_thread.wait();
}

IrcHandler& IrcClient::handler()
{
	return _handler;
}

const IrcHandler& IrcClient::handler() const
{
	return _handler;
}

void IrcClient::setClientStatus(ClientStatus cStatus)
{
	_clientStatus = cStatus;
	emit clientStatusChanged();
}

IrcClient::ClientStatus IrcClient::clientStatus() const
{
	return _clientStatus;
}

QString IrcClient::clientStatusString() const
{
	switch (_clientStatus)
	{
		case cNone:				return "None";
		case cDisconnected:		return "Disconnected";
		case cDisconnecting:	return "Disconnecting";
		case cConnecting:		return "Connecting";
		case cConnected:		return "Connected";
		case cLogin:			return "Login";
		case cReady:			return "Ready";
		case cError:			return "Error";
		default:				return QString("Unknown(%1)").arg(_clientStatus);
	} // switch
}

void IrcClient::setLoginStatus(int iStatus)
{
	_loginStatus = (LoginStatus)iStatus;
	emit loginStatusChanged();
}

IrcClient::LoginStatus IrcClient::loginStatus() const
{
	return _loginStatus;
}

QString IrcClient::loginStatusString() const
{
	switch (_loginStatus)
	{
		case lNone:				return "None";
		case lWaitFirstReply:	return "WaitFirstReply";
		case lFirstReplyDone:	return "FirstReplyDone";
		case lPreAuth:			return "PreAuth";
		case lPreAuthWait:		return "PreAuthWait";
		case lPreAuthDone:		return "PreAuthDone";
		case lMotdWait:			return "MotdWait";
		case lMotdDone:			return "MotdDone";
		case lNickservAuth:		return "NickservAuth";
		case lNickservAuthWait:	return "NickservAuthWait";
		case lNickservAuthDone:	return "NickservAuthDone";
		case lAutoJoin:			return "AutoJoin";
		case lReady:			return "Ready";
		default:				return QString("Unknown(%1)").arg(_loginStatus);
	} // switch
}

bool IrcClient::isReady() const
{
	return (_clientStatus == cReady);
}

bool IrcClient::isConnected() const
{
	switch (_clientStatus)
	{
		case cConnected:
		case cLogin:
		case cReady:
			return true;
		default:
			return false;

	} // switch (_clientStatus)
}

bool IrcClient::isDisconnected() const
{
	switch (_clientStatus)
	{
		case cNone:
		case cDisconnected:
		case cDisconnecting:
		case cError:
			return true;
		default:
			return false;
	} // switch (_clientStatus)
}

PersistentSocket& IrcClient::connection()
{
	return _connection;
}

void IrcClient::setConfig(const IrcClientConfig& cfg)
{
	_config = cfg;
}

IrcClientConfig& IrcClient::config()
{
	return _config;
}

const IrcClientConfig& IrcClient::config() const
{
	return _config;
}

bool IrcClient::connectClient(bool forceReconnect /*= false*/, bool failOnConnected /*= false*/)
{
	if (!_config.accountConfig().isValid())
	{
		DebugLog(dlWarn, "IrcClient::connectClient account config isValid check failed");
		return false;
	}
	if (!_connection.connectClient(forceReconnect, failOnConnected))
	{
		DebugLog(dlWarn, "IrcClient::connectClient connection connectClient failed");
		return false;
	}
	return true;
}

bool IrcClient::disconnectClient(const QString& sQuitMessage /*= ""*/)
{
	sendQuit(sQuitMessage); // ignored if not isRead()
	if (!_connection.disconnectClient())
	{
		DebugLog(dlWarn, "IrcClient::disconnectClient connection disconnectClient failed");
		return false;
	}
	return true;
}

void IrcClient::sendLine(const QString& sMsg)
{
	//DebugLog(dlDebug, "IrcClient::sendLine  output: %s", qPrintable(sMsg));
	sendRaw(sMsg + "\r\n");
}

void IrcClient::sendRaw(const QString& sMsg)
{
	_outputQueue.enqueue(sMsg);
}

void IrcClient::outputTimerTick()
{
	if (!isConnected())
	{
		if (_outputTimer.interval() != cNotReadyOutputTimerInterval)
			_outputTimer.start(cNotReadyOutputTimerInterval);
	}
	else if (_outputQueue.isEmpty())
	{
		if (_outputTimer.interval() != cIdleOutputTimerInterval)
			_outputTimer.start(cIdleOutputTimerInterval);
	}
	else
	{
		_connection.write(_outputQueue.dequeue());

		if (_outputQueue.isEmpty())
			_outputTimer.start(cIdleOutputTimerInterval);
		else if (_outputTimer.interval() != cReadyOutputTimerInterval)
			_outputTimer.start(cReadyOutputTimerInterval);
	}
}

void IrcClient::connectionReadyRead()
{
	_inputQueueLock.lock();
	while (_connection.canReadLine())
		_inputQueue.enqueue(QString(_connection.readLine(cMaxReadLineCharacters)));
	_inputQueueLock.unlock();
}

void IrcClient::loginTimerTick()
{
	switch (_loginStatus)
	{
		case lNone:
			if (!isConnected())
				_loginTimer.stop();
			break;
		case lWaitFirstReply:
			// waiting until incremented by connectionReadyRead
			break;
		case lFirstReplyDone:
			DebugLog(dlDebug, "IrcClient::loginTimerTick beginning login process");
			setLoginStatus(lPreAuth);
			break;
		case lPreAuth:
			if (!_config.accountConfig().useSASL())
				setLoginStatus(lPreAuthDone);
			else
			{
				setLoginStatus(lPreAuthWait);
				sendLine("CAP LS");
				sendNickUserLogin();
			}
			break;
		case lPreAuthWait:
			break;
		case lPreAuthDone:
			setLoginStatus(lMotdWait);
			break;
		case lMotdWait:
			break;
		case lMotdDone:
			if (!_config.accountConfig().useSASL())
				setLoginStatus(lNickservAuthDone); // SASL skips Nickserv Authentication
			else
				setLoginStatus(lNickservAuth);
			break;
		case lNickservAuth:
			if (!_config.accountConfig().hasNickservPassword())
				setLoginStatus(lNickservAuthDone); // no Nickserv password
			else
			{
				setLoginStatus(lNickservAuthWait);
				sendLine(QString("NS IDENTIFY %1").arg(_config.accountConfig().nickservPassword()));
			}
			break;
		case lNickservAuthWait:
			break;
		case lNickservAuthDone:
			if (_config.accountConfig().hasAutoJoinChannels())
				setLoginStatus(lAutoJoin);
			else
				setLoginStatus(lReady);
			break;
		case lAutoJoin:
			sendAutoJoins();
			setLoginStatus(lReady);
			break;
		case lReady:
			setClientStatus(cReady);
			DebugLog(dlDebug, "IrcClient::loginTimerTick login process complete, stoping loginTimer");
			_loginTimer.stop();
			break;
	} // switch
}

void IrcClient::connectionStatusChanged()
{
	if (_connection.isConnected())
	{
		if (!isConnected())
		{
			setLoginStatus(lWaitFirstReply);
			_loginTimer.start(cLoginTimerInterval);
			setClientStatus(cConnected);
			emit connected();
		}
	}
	else if (_connection.isDisconnected())
	{
		if (isConnected())
		{
			setLoginStatus(lNone);
			setClientStatus(cDisconnected);
			emit disconnected();
		}
	}
}

void IrcClient::send(QString str)
{
	sendLine(str);
}

void IrcClient::sendNickUserLogin()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendNickUserLogin not connected");
		return;
	}
	sendLine(QString("NICK %1").arg(_config.accountConfig().nick()));
	sendLine(QString("USER %1 %1 %2 :%3")
			 .arg(_config.accountConfig().user())
			 .arg(_connection.serverConfig().host())
			 .arg(_config.accountConfig().realName()));
}

void IrcClient::sendAuthenticateSASL()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendAuthenticateSASL not connected");
		return;
	}
	if (!_config.accountConfig().useSASL())
	{
		DebugLog(dlError, "IrcClient::sendAuthenticateSASL account not set to use SASL");
		return;
	}
	if (!_config.accountConfig().saslIsValid())
	{
		DebugLog(dlError, "IrcClient::sendAuthenticateSASL account SASL config not valid");
		return;
	}
	QString authHash = _config.accountConfig().saslAuthHash();
	//DebugLog(dlDebug, "IrcClient::sendAuthenticateSASL prepared hash: '%s'", qPrintable(authHash));
	while (authHash.size() >= 400)
	{
		QString authHashPart = authHash.left(400);
		authHash = authHash.remove(0, 400);
		sendLine(QString("AUTHENTICATE %1").arg(authHashPart));
	}
	if (!authHash.isEmpty()) // send what is left
		sendLine(QString("AUTHENTICATE %1").arg(authHash));
	else
		sendLine("AUTHENTICATE +"); // last part was exactly 400 bytes, inform the server that there's nothing more
}

void IrcClient::sendAutoJoins()
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendAutoJoins not connected");
		return;
	}
	QStringList chanList = _config.accountConfig().autoJoinChannels().split(QChar(','));
	for (int i = 0;i < chanList.size();i++)
		sendLine(QString("JOIN %1").arg(chanList[i]));
}

void IrcClient::sendJoin(QString sChannel)
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendJoin not connected");
		return;
	}
	sendLine(QString("JOIN %1").arg(sChannel));
}

void IrcClient::sendPart(QString sChannel, QString sMessage)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendPart client not ready");
		return;
	}
	sendLine(QString("PART %1 :%2").arg(sChannel).arg(sMessage));
}

void IrcClient::sendPrivateMessage(QString sNickChannel, QString sMessage)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendPrivateMessage client not ready");
		return;
	}
	sendLine(QString("PRIVMSG %1 :%2").arg(sNickChannel).arg(sMessage));
}

void IrcClient::sendWhois(QString sNick)
{
	if (!isReady())
	{
		DebugLog(dlWarn, "IrcClient::sendWhois client not ready");
		return;
	}
	sendLine(QString("WHOIS %1").arg(sNick));
}

void IrcClient::sendQuit(QString sMessage)
{
	if (!isConnected())
	{
		DebugLog(dlWarn, "IrcClient::sendQuit not connected");
		return;
	}
	sendLine(QString("QUIT :%1").arg(sMessage));
}

void IrcClient::configChanged()
{
	_connection.setServerConfig(_config.serverConfig());
	_connection.setProxyConfig(_config.proxyConfig());
}

bool IrcClient::hasInputQueue()
{
	bool isEmpty;
	_inputQueueLock.lock();
	isEmpty = _inputQueue.isEmpty();
	_inputQueueLock.unlock();
	return !isEmpty;
}

QQueue<QString>& IrcClient::lockInputQueue()
{
	_inputQueueLock.lock();
	return _inputQueue;
}

void IrcClient::unlockInputQueue()
{
	_inputQueueLock.unlock();
}

