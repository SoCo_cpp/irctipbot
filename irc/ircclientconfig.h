#ifndef IRCCLIENTCONFIG_H
#define IRCCLIENTCONFIG_H

#include <QObject>
#include "irc/ircaccountconfig.h"
#include "networking/proxyserverconfig.h"

#ifdef JSON_CONFIG
	#include <QJsonObject>
#endif

class IrcClientConfig : public ProxyServerConfig
{
	Q_OBJECT
public:
	explicit IrcClientConfig(QObject *parent = 0);
	IrcClientConfig(const IrcClientConfig& cpy);
	IrcClientConfig& operator =(const IrcClientConfig& cpy);
	void copy(const IrcClientConfig& cpy, bool noChangeSignal = false);

	IrcAccountConfig& accountConfig();
	const IrcAccountConfig& accountConfig() const;

#ifdef JSON_CONFIG
	virtual void saveJson(QJsonObject& jsonObj) const;
	virtual bool loadJson(const QJsonObject& jsonObj, bool bRequireAllFields = false, bool noChangeSignal = false);
#endif

protected:
	IrcAccountConfig _accountConfig;

signals:
	//Inheritted: void changed();

public slots:

};

#endif // IRCCLIENTCONFIG_H
