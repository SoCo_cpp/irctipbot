#ifndef IRCCLIENT_H
#define IRCCLIENT_H

#include <QObject>
#include <QQueue>
#include <QTimer>
#include <QThread>
#include <QMutex>
#include "networking/persistentsocket.h"
#include "irc/ircclientconfig.h"
#include "irc/irchandler.h"

class IrcClient : public QObject
{
	Q_OBJECT
public:
	const static qint64 cMaxReadLineCharacters = 512;	// IRC protocol specification
	const static int cReadyOutputTimerInterval		= 40;	// msec
	const static int cNotReadyOutputTimerInterval	= 1000;	// msec
	const static int cIdleOutputTimerInterval		= 300;	// msec
	const static int cLoginTimerInterval			= 200;	// msec

	typedef enum {cNone = 0, cDisconnected, cDisconnecting, cConnecting, cConnected, cLogin, cReady, cError} ClientStatus;
	typedef enum {lNone = 0,
				  lWaitFirstReply, lFirstReplyDone,
				  lPreAuth, lPreAuthWait, lPreAuthDone,
				  lMotdWait, lMotdDone,
				  lNickservAuth, lNickservAuthWait, lNickservAuthDone,
				  lAutoJoin,
				  lReady} LoginStatus;

	explicit IrcClient(QObject *parent = 0);
	~IrcClient();

	IrcHandler& handler();
	const IrcHandler& handler() const;

	void setClientStatus(ClientStatus cStatus);
	ClientStatus clientStatus() const;
	QString clientStatusString() const;

	//setLoginStatus is a slot
	LoginStatus loginStatus() const;
	QString loginStatusString() const;

	bool isReady() const;
	bool isConnected() const;
	bool isDisconnected() const;

	PersistentSocket& connection();

	void setConfig(const IrcClientConfig& cfg);
	IrcClientConfig& config();
	const IrcClientConfig& config() const;

	bool connectClient(bool forceReconnect = false, bool failOnConnected = false);
	bool disconnectClient(const QString& sQuitMessage = "");

	void sendRaw(const QString& str);
	void sendLine(const QString& str);

	bool hasInputQueue(); // does its own lock
	QQueue<QString>& lockInputQueue();
	void unlockInputQueue();

protected:
	PersistentSocket _connection;
	IrcHandler _handler;
	ClientStatus _clientStatus;
	LoginStatus _loginStatus;
	IrcClientConfig _config;

	QThread _thread;

	QTimer _outputTimer;
	QTimer _loginTimer;

	QQueue<QString> _outputQueue;
	QQueue<QString> _inputQueue;
	QMutex _inputQueueLock;

signals:
	void clientStatusChanged();
	void loginStatusChanged();
	void connected();
	void disconnected();
	//void connectionReadyRead();

public slots:
	void configChanged();

	void outputTimerTick();
	void connectionReadyRead();

	void loginTimerTick();
	void connectionStatusChanged();


	void setLoginStatus(int iStatus);

	void send(QString str);
	void sendNickUserLogin();
	void sendAuthenticateSASL();
	void sendAutoJoins();
	void sendJoin(QString sChannel);
	void sendPart(QString sChannel, QString sMessage);
	void sendPrivateMessage(QString sNickChannel, QString sMessage);
	void sendWhois(QString sNick);
	void sendQuit(QString sMessage);

};

#endif // IRCCLIENT_H
