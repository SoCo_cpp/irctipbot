#include "irc/irchandler.h"
#include "irc/ircclient.h"
#include "logging/debuglogger.h"

IrcHandler::IrcHandler(QObject*, IrcClient& ircClient) :
	QObject(0), // never have parent when moving to another thread
	_ircClient(ircClient)
{
	moveToThread(&_thread);
	_thread.start();

	connect(this, SIGNAL(setLoginStatus(int)), &_ircClient, SLOT(setLoginStatus(int)), Qt::QueuedConnection);
	connect(this, SIGNAL(send(QString)), &_ircClient, SLOT(send(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendNickUserLogin(void)), &_ircClient, SLOT(sendNickUserLogin(void)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendAuthenticateSASL(void)), &_ircClient, SLOT(sendAuthenticateSASL(void)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendJoin(QString)), &_ircClient, SLOT(sendJoin(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendPart(QString,QString)), &_ircClient, SLOT(sendPart(QString,QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendPrivateMessage(QString,QString)), &_ircClient, SLOT(sendPrivateMessage(QString,QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendWhois(QString)), &_ircClient, SLOT(sendWhois(QString)), Qt::QueuedConnection);
	connect(this, SIGNAL(sendQuit(QString)), &_ircClient, SLOT(sendQuit(QString)), Qt::QueuedConnection);

	connect(&_handleInputTimer, SIGNAL(timeout()), this, SLOT(handleInputTimerTick()));
	_handleInputTimer.start(cHandleInputTimerInterval);
}

IrcHandler::~IrcHandler()
{
	_thread.quit();
	_thread.wait();
}

void IrcHandler::handleInputTimerTick()
{
	bool haveInput;
	QQueue<QString>& inputQueue = _ircClient.lockInputQueue();
	haveInput = !inputQueue.isEmpty();
	if (haveInput)
		_ircPacket.parse(inputQueue.dequeue());
	_ircClient.unlockInputQueue(); // hurry and unlock before handling packet
	if (haveInput)
		handlePacket();
}

void IrcHandler::handlePacket()
{
	if (_ircPacket.isEmpty())
	{
		DebugLog(dlWarn, "IrcHandler::handlePakcet packet empty");
		return;
	}
	//DebugLog(dlDebug, "IrcHandler::handlePacket handling input: %s", qPrintable(_ircPacket.packet()));
	if (_ircClient.loginStatus() == IrcClient::lWaitFirstReply)
		emit setLoginStatus(IrcClient::lFirstReplyDone);
	if (_ircPacket.commandInt() != -1)
		handleNumericCommandPacket();
	else
		handleTextCommandPacket();
}

void IrcHandler::handleNumericCommandPacket()
{
	switch (_ircPacket.commandInt())
	{
	case 1: // RPL_WELCOME "Welcome to the Internet Relay Network	<nick>!<user>@<host>"
		break;
	case 2: // RPL_YOURHOST "Your host is <servername>, running version <ver>"
		break;
	case 3: // RPL_CREATED "This server was created <date>"
		break;
	case 4: // RPL_MYINFO "<servername> <version> <available user modes> <available channel modes>"
		break;
	case 5: // RPL_BOUNCE "Try server <server name>, port <port number>"
		break;
	case 200: // RPL_TRACELINK "Link <version & debug level> <destination>	<next server> V<protocol version> <link uptime in seconds> <backstream sendq>  <upstream sendq>"
		break;
	case 201: // RPL_TRACECONNECTING "Try. <class> <server>"
		break;
	case 202: // RPL_TRACEHANDSHAKE "H.S. <class> <server>"
		break;
	case 203: // RPL_TRACEUNKNOWN "???? <class> [<client IP address in dot form>]"
		break;
	case 204: // RPL_TRACEOPERATOR "Oper <class> <nick>"
		break;
	case 205: // RPL_TRACEUSER "User <class> <nick>"
		break;
	case 206: // RPL_TRACESERVER "Serv <class> <int>S <int>C <server> <nick!user|*!*>@<host|server> V<protocol version>"
		break;
	case 207: // RPL_TRACESERVICE "Service <class> <name> <type> <active type>"
		break;
	case 208: // RPL_TRACENEWTYPE "<newtype> 0 <client name>"
		break;
	case 209: // RPL_TRACECLASS "Class <class> <count>"
		break;
	case 210: // RPL_TRACERECONNECT Unused
		break;
	case 211: // RPL_STATSLINKINFO "<linkname> <sendq> <sent messages> <sent Kbytes> <received messages> <received Kbytes> <time open>"
		break;
	case 212: // RPL_STATSCOMMANDS "<command> <count> <byte count> <remote count>"
		break;
	case 219: // RPL_ENDOFSTATS "<stats letter> :End of STATS report"
		break;
	case 221: // RPL_UMODEIS "<user mode string>"
		break;
	case 234: // RPL_SERVLIST "<name> <server> <mask> <type> <hopcount> <info>"
		break;
	case 235: // RPL_SERVLISTEND "<mask> <type> :End of service listing"
		break;
	case 242: // RPL_STATSUPTIME ":Server Up %d days %d:%02d:%02d"
		break;
	case 243: // RPL_STATSOLINE  "O <hostmask> * <name>"
		break;
	case 251: // RPL_LUSERCLIENT ":There are <integer> users and <integer> services on <integer> servers"
		break;
	case 252: // RPL_LUSEROP "<integer> :operator(s) online"
		break;
	case 253: // RPL_LUSERUNKNOWN "<integer> :unknown connection(s)"
		break;
	case 254: // RPL_LUSERCHANNELS "<integer> :channels formed"
		break;
	case 255: // RPL_LUSERME ":I have <integer> clients and <integer> servers"
		break;
	case 256: // RPL_ADMINME "<server> :Administrative info"
		break;
	case 257: // RPL_ADMINLOC1 ":<admin info>"
		break;
	case 258: // RPL_ADMINLOC2 ":<admin info>"
		break;
	case 259: // RPL_ADMINEMAIL ":<admin info>"
		break;
	case 261: // RPL_TRACELOG "File <logfile> <debug level>"
		break;
	case 262: // RPL_TRACEEND  "<server name> <version & debug level> :End of TRACE"
		break;
	case 263: // RPL_TRYAGAIN "<command> :Please wait a while and try again."
		break;
	case 301: // RPL_AWAY "<nick> :<away message>"
		break;
	case 302: // RPL_USERHOST ":*1<reply> *( " " <reply> )" reply = nickname [ "*" ] "=" ( "+" / "-" ) hostname, * op, - away message, + no away message
		break;
	case 303: // RPL_ISON ":*1<nick> *( " " <nick> )"
		break;
	case 305: // RPL_UNAWAY ":You are no longer marked as being away"
		break;
	case 306: // RPL_NOWAWAY ":You have been marked as being away"
		break;
	case 311: // RPL_WHOISUSER "<nick> <user> <host> * :<real name>"
		break;
	case 312: // RPL_WHOISSERVER "<nick> <server> :<server info>"
		break;
	case 313: // RPL_WHOISOPERATOR  "<nick> :is an IRC operator"
		break;
	case 314: // RPL_WHOWASUSER "<nick> <user> <host> * :<real name>"
		break;
	case 315: // RPL_ENDOFWHO "<name> :End of WHO list"
		break;
	case 317: // RPL_WHOISIDLE "<nick> <integer> :seconds idle"
		break;
	case 318: // RPL_ENDOFWHOIS "<nick> :End of WHOIS list"
		break;
	case 319: // RPL_WHOISCHANNELS  "<nick> :*( ( "@" / "+" ) <channel> " " )"
		break;
	case 321: // RPL_LISTSTART obsolete
		break;
	case 322: // RPL_LIST "<channel> <# visible> :<topic>"
		break;
	case 323: // RPL_LISTEND ":End of LIST"
		break;
	case 324: // RPL_CHANNELMODEIS "<channel> <mode> <mode params>"
		break;
	case 325: // RPL_UNIQOPIS "<channel> <nickname>"
		break;
	case 331: // RPL_NOTOPIC "<channel> :No topic is set"
		break;
	case 332: // RPL_TOPIC "<channel> :<topic>"
		emit receivedPrivmsg(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
		break;
	case 341: // RPL_INVITING "<channel> <nick>" (successfully sending invite)
		break;
	case 342: // RPL_SUMMONING "<user> :Summoning user to IRC"
		break;
	case 346: // RPL_INVITELIST "<channel> <invitemask>"
		break;
	case 347: // RPL_ENDOFINVITELIST "<channel> :End of channel invite list"
		break;
	case 348: // RPL_EXCEPTLIST "<channel> <exceptionmask>"
		break;
	case 349: // RPL_ENDOFEXCEPTLIST "<channel> :End of channel exception list"
		break;
	case 351: // RPL_VERSION "<version>.<debuglevel> <server> :<comments>"
		break;
	case 352: // RPL_WHOREPLY "<channel> <user> <host> <server> <nick> ( "H" / "G" > ["*"] [ ( "@" / "+" ) ] :<hopcount> <real name>"
		break;
	case 353: // RPL_NAMREPLY "( "=" / "*" / "@" ) <channel>  :[ "@" / "+" ] <nick> *( " " [ "@" / "+" ] <nick> )  @ secret, * private, = other (pubic)
		break;
	case 364: // RPL_LINKS "<mask> <server> :<hopcount> <server info>"
		break;
	case 365: // RPL_ENDOFLINKS "<mask> :End of LINKS list"
		break;
	case 366: // RPL_ENDOFNAMES "<channel> :End of NAMES list"
		break;
	case 367: // RPL_BANLIST "<channel> <banmask>"
		break;
	case 368: // RPL_ENDOFBANLIST "<channel> :End of channel ban list"
		break;
	case 369: // RPL_ENDOFWHOWAS "<nick> :End of WHOWAS"
		break;
	case 371: // RPL_INFO ":<string>" ":End of INFO list"
		break;
	case 372: // RPL_MOTD ":- <text>"
		break;
	case 374: // RPL_ENDOFINFO ":End of INFO list"
		break;
	case 375: // RPL_MOTDSTART ":- <server> Message of the day - "
		break;
	case 376: // RPL_ENDOFMOTD ":End of MOTD command"
		if (_ircClient.loginStatus() == IrcClient::lMotdWait)
			emit setLoginStatus((int)IrcClient::lMotdDone);
		else
			DebugLog(dlWarn, "IrcHandler::handleNumericCommandPacket not expecting MOTD end. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		break;
	case 381: // RPL_YOUREOPER ":You are now an IRC operator"
		break;
	case 382: // RPL_REHASHING  "<config file> :Rehashing"
		break;
	case 383: // RPL_YOURESERVICE  "You are service <servicename>"
		break;
	case 391: // RPL_TIME "<server> :<string showing server's local time>"
		break;
	case 392: // RPL_USERSSTART  ":UserID   Terminal  Host"
		break;
	case 393: // RPL_USERS ":<username> <ttyline> <hostname>"
		break;
	case 394: // RPL_ENDOFUSERS ":End of users"
		break;
	case 395: // RPL_NOUSERS ":Nobody logged in"
		break;
	// ----------- Error Replies -------------------------------
	case 400:
		break;
	case 401: // ERR_NOSUCHNICK "<nickname> :No such nick/channel"
		break;
	case 402: // ERR_NOSUCHSERVER  "<server name> :No such server"
		break;
	case 403: // ERR_NOSUCHCHANNEL "<channel name> :No such channel"
		break;
	case 404: // ERR_CANNOTSENDTOCHAN "<channel name> :Cannot send to channel"
		break;
	case 405: // ERR_TOOMANYCHANNELS "<channel name> :You have joined too many channels"
		break;
	case 406: // ERR_WASNOSUCHNICK "<nickname> :There was no such nickname"
		break;
	case 407: // ERR_TOOMANYTARGETS "<target> :<error code> recipients. <abort message>"
		break;
	case 408: // ERR_NOSUCHSERVICE "<service name> :No such service"
		break;
	case 409: // ERR_NOORIGIN ":No origin specified"
		break;
	case 411: // ERR_NORECIPIENT  ":No recipient given (<command>)"
		break;
	case 412: // ERR_NOTEXTTOSEND ":No text to send"
		break;
	case 413: // ERR_NOTOPLEVEL "<mask> :No toplevel domain specified"
		break;
	case 414: // ERR_WILDTOPLEVEL "<mask> :Wildcard in toplevel domain"
		break;
	case 415: // ERR_BADMASK "<mask> :Bad Server/host mask"
		break;
	case 421: // ERR_UNKNOWNCOMMAND "<command> :Unknown command"
		break;
	case 422: // ERR_NOMOTD ":MOTD File is missing"
		if (_ircClient.loginStatus() == IrcClient::lMotdWait)
			emit setLoginStatus((int)IrcClient::lMotdDone);
		else
			DebugLog(dlWarn, "IrcHandler::handleNumericCommandPacket not expecting MOTD error, file missing. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		break;
	case 423: // ERR_NOADMININFO "<server> :No administrative info available"
		break;
	case 424: // ERR_FILEERROR ":File error doing <file op> on <file>"
		break;
	case 431: // ERR_NONICKNAMEGIVEN ":No nickname given"
		break;
	case 432: // ERR_ERRONEUSNICKNAME "<nick> :Erroneous nickname"
		break;
	case 433: // ERR_NICKNAMEINUSE "<nick> :Nickname is already in use"
		break;
	case 436: // ERR_NICKCOLLISION  "<nick> :Nickname collision KILL from <user>@<host>"
		break;
	case 437: // ERR_UNAVAILRESOURCE "<nick/channel> :Nick/channel is temporarily unavailable"
		break;
	case 441: // ERR_USERNOTINCHANNEL "<nick> <channel> :They aren't on that channel"
		break;
	case 442: // ERR_NOTONCHANNEL "<channel> :You're not on that channel"
		break;
	case 443: // ERR_USERONCHANNEL "<user> <channel> :is already on channel"
		break;
	case 444: // ERR_NOLOGIN "<user> :User not logged in"
		break;
	case 445: // ERR_SUMMONDISABLED ":SUMMON has been disabled"
		break;
	case 446: // ERR_USERSDISABLED ":USERS has been disabled"
		break;
	case 451: // ERR_NOTREGISTERED ":You have not registered"
		break;
	case 461: // ERR_NEEDMOREPARAMS "<command> :Not enough parameters"
		break;
	case 462: // ERR_ALREADYREGISTRED ":Unauthorized command (already registered)"
		break;
	case 463: // ERR_NOPERMFORHOST ":Your host isn't among the privileged"
		break;
	case 464: // ERR_PASSWDMISMATCH ":Password incorrect"
		break;
	case 465: // ERR_YOUREBANNEDCREEP ":You are banned from this server"
		break;
	case 466: // ERR_YOUWILLBEBANNED
		break;
	case 467: // ERR_KEYSET "<channel> :Channel key already set"
		break;
	case 471: // ERR_CHANNELISFULL "<channel> :Cannot join channel (+l)"
		break;
	case 472: // ERR_UNKNOWNMODE "<char> :is unknown mode char to me for <channel>"
		break;
	case 473: // ERR_INVITEONLYCHAN "<channel> :Cannot join channel (+i)"
		break;
	case 474: // ERR_BANNEDFROMCHAN "<channel> :Cannot join channel (+b)"
		break;
	case 475: // ERR_BADCHANNELKEY "<channel> :Cannot join channel (+k)"
		break;
	case 476: // ERR_BADCHANMASK "<channel> :Bad Channel Mask"
		break;
	case 477: // ERR_NOCHANMODES "<channel> :Channel doesn't support modes"
		break;
	case 478: // ERR_BANLISTFULL "<channel> <char> :Channel list is full"
		break;
	case 481: // ERR_NOPRIVILEGES ":Permission Denied- You're not an IRC operator"
		break;
	case 482: // ERR_CHANOPRIVSNEEDED  "<channel> :You're not channel operator"
		break;
	case 483: // ERR_CANTKILLSERVER ":You can't kill a server!"
		break;
	case 484: // ERR_RESTRICTED ":Your connection is restricted!"
		break;
	case 485: // ERR_UNIQOPPRIVSNEEDED ":You're not the original channel operator"
		break;
	case 491: // ERR_NOOPERHOST  ":No O-lines for your host"
		break;
	case 501: // ERR_UMODEUNKNOWNFLAG ":Unknown MODE flag"
		break;
	case 502: // ERR_USERSDONTMATCH  ":Cannot change mode for other users"
		break;
	case 900: // SASL logged in 900 <Nick> <NickUserHost> <LoginNick> :You are now logged in as <LoginNick>.
		break;
	case 903: // RPL_SASLSUCCESS 903 Doge_Soaker2 :SASL authentication successful
		if (_ircClient.loginStatus() == IrcClient::lPreAuthWait)
		{
			emit setLoginStatus((int)IrcClient::lPreAuthDone);
			emit send("CAP END");
		}
		else
			DebugLog(dlWarn, "IrcHandler::handleNumericCommandPacket not expecting SASL success. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		break;
	case 904: // ERR_SASLFAIL
	case 905: // ERR_SASLTOOLONG
	case 906: // ERR_SASLABORTED
	case 907: // ERR_SASLALREADY
		DebugLog(dlWarn, "IrcHandler::handleNumericCommandPacket SASL error received. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		if (_ircClient.loginStatus() == IrcClient::lPreAuthWait)
			emit send("CAP END");
		break;
	default:
		DebugLog(dlDebug, "IrcHandler::handleNumericCommandPacket undocumented numeric packet command: %d packet: %s", _ircPacket.commandInt(), qPrintable(_ircPacket.packet()));
		break;
	} // switch (_ircPacket.commandInt())
}

void IrcHandler::handleTextCommandPacket()
{
	if (_ircPacket.command() == "PING")
	{
		emit send(QString("PONG :%1").arg(_ircPacket.trailing().toString()));
	}
	else if (_ircPacket.command() == "PONG")
	{
		emit receivedPong(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "ERROR")
	{

	}
	else if (_ircPacket.command() == "CAP") // caps (for sasl)
	{
		if (!_ircClient.config().accountConfig().useSASL())
		{
			DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket received unexpected server Caps, but account not using SASL");
		}
		else if (_ircPacket.params() == "LS") // CAP * LS :account-notify extended-join identify-msg multi-prefix sasl
		{
			QStringList lstCaps = _ircPacket.trailing().toString().split(QChar(' '));
			if (!lstCaps.contains("sasl"))
			{
				DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket SASL Caps not supported. Cap list: '%s'' Joined:[%s]", qPrintable(_ircPacket.trailing().toString()), qPrintable(lstCaps.join(",")));
				emit send("CAP END");
			}
			else emit send("CAP REQ :sasl");
		}
		else if (_ircPacket.params() == "ACK") //  CAP * ACK :sasl
		{
			QStringList lstCaps = _ircPacket.trailing().toString().split(QChar(' '));
			if (!lstCaps.contains("sasl"))
			{
				DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket Caps Ack'd non-sasl support. Cap Ack list: %s", qPrintable(_ircPacket.trailing().toString()));
				emit send("CAP END");
			}
			else emit send("AUTHENTICATE PLAIN"); // Another options: DH-BLOWFISH

		}
		else DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket unknown CAP packet: %s", qPrintable(_ircPacket.packet()));
	}
	else if (_ircPacket.command() == "AUTHENTICATE") // SASL Auth requset
	{
		if (!_ircClient.config().accountConfig().useSASL())
			DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket received unexpected server Authenticate request, but account not using SASL");
		else if (_ircClient.loginStatus() != IrcClient::lPreAuthWait)
			DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket not expecting Authenticate request. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		else
		{
			if (_ircPacket.middle() != "+")
				DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket Authenticate request (sasl) not formatted as expected, trying anyways");
			DebugLog(dlDebug, "IrcHandler::handleTextCommandPacket sending Auth SASL");
			emit sendAuthenticateSASL();
		}
	}
	else if (_ircPacket.command() == "PRIVMSG") // :Shadow007!~bugtraq@180.254.141.228 PRIVMSG #qt :thats the matter...is it wrong if i'm begginer in C++ and OOP trying creating project with Qt ??
	{
		emit receivedPrivmsg(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "JOIN")
	{
		emit receivedJoin(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString());
	}
	else if (_ircPacket.command() == "PART")
	{
		emit receivedPart(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "QUIT")
	{
		emit receivedQuit(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "KICK")
	{
		emit receivedKick(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.params().toString(),_ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "TOPIC")
	{
		emit receivedTopic(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "MODE")
	{
		emit receivedMode(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.params().toString());
	}
	else if (_ircPacket.command() == "NICK")
	{
		emit receivedNick(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command() == "NOTICE")
	{
		if (_ircPacket.prefix() == "NickServ!NickServ@services.")
		{
			if (_ircPacket.trailing() ==  QString("You are now identified for %1").arg(_ircClient.config().accountConfig().nick()))
			{
				if (_ircClient.loginStatus() != IrcClient::lNickservAuthWait)
					emit setLoginStatus((int)IrcClient::lNickservAuthDone);
				else
					DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket Nickserv identified not expected. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
			}
			else if (_ircPacket.trailing() != "This nickname is registered.") // ignoring this message only
				DebugLog(dlWarn, "IrcHandler::handleTextCommandPacket Nickserv notice not expected. LoginStatus: %s, packet: %s", qPrintable(_ircClient.loginStatusString()), qPrintable(_ircPacket.packet()));
		}
		emit receivedNotice(_ircPacket.nick().toString(), _ircPacket.prefix().toString(), _ircPacket.middle().toString(), _ircPacket.trailing().toString());
	}
	else if (_ircPacket.command().isEmpty())
	{
		DebugLog(dlError, "IrcHandler::handleTextCommandPacket command part of packet was empty, packet: %s", qPrintable(_ircPacket.packet()));
	}
	else
	{
		DebugLog(dlDebug, "IrcHandler::handleTextCommandPacket unhandled text command, packet: %s", qPrintable(_ircPacket.packet()));
	}
}
