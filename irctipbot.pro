#-------------------------------------------------
#
# Project created by QtCreator 2014-06-04T17:47:18
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

QTPLUGIN += qsqlmysql
CONFIG += qsqlmysql

TARGET = IRCTipbot
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DEFINES *= JSON_CONFIG

message("$$QTDIR")
message("$$QT_PLUGIN_PATH")
message("$$QT")
message($$CONFIG)
message($$LIBS)
message($$QMAKE_LIBDIR)

SOURCES +=  main.cpp \
            logging/logbase.cpp \
    logging/debuglogger.cpp \
    logging/timestampedlog.cpp \
    networking/socketconnection.cpp \
    networking/persistentsocket.cpp \
    networking/proxyconfig.cpp \
    networking/serverconfig.cpp \
    networking/httpjsonclient.cpp \
    networking/httpjsonclientreply.cpp \
    wallet/coinwalletclient.cpp \
    wallet/coinwalletclientconfig.cpp \
    wallet/coinwalletapi.cpp \
    database/sqldatabase.cpp \
    database/sqldatabaseconfig.cpp \
    testing/testing.cpp \
    database/databasetabledesign.cpp \
    database/databasedatatype.cpp \
    irc/ircclient.cpp \
    irc/ircaccountconfig.cpp \
    irc/irchandler.cpp \
    irc/ircpacket.cpp \
    ircbots/irctipbot/irctipbot.cpp \
    networking/proxyserverconfig.cpp \
    files/jsonconfigfile.cpp \
    irc/ircclientconfig.cpp \
    ircbots/irctipbot/irctipbotdatabasedesign.cpp \
    ircbots/irctipbot/irctipbotsystem.cpp \
    ircbots/irctipbot/irctipbotirchandler.cpp \
    ircbots/irctipbot/irctipbotwalletmanager.cpp \
    wallet/coinwalletrequest.cpp \
    wallet/walletreplyhandler.cpp

HEADERS += logging/logbase.h \
    logging/debuglogger.h \
    logging/timestampedlog.h \
    networking/socketconnection.h \
    networking/persistentsocket.h \
    networking/proxyconfig.h \
    networking/serverconfig.h \
    networking/httpjsonclient.h \
    networking/httpjsonclientreply.h \
    wallet/coinwalletclient.h \
    wallet/coinwalletclientconfig.h \
    wallet/coinwalletapi.h \
    database/sqldatabase.h \
    database/sqldatabaseconfig.h \
    testing/testing.h \
    database/databasetabledesign.h \
    database/databasedatatype.h \
    irc/ircclient.h \
    irc/ircaccountconfig.h \
    irc/irchandler.h \
    irc/ircpacket.h \
    ircbots/irctipbot/irctipbot.h \
    networking/proxyserverconfig.h \
    files/jsonconfigfile.h \
    irc/ircclientconfig.h \
    ircbots/irctipbot/irctipbotdatabasedesign.h \
    ircbots/irctipbot/irctipbotsystem.h \
    ircbots/irctipbot/irctipbotirchandler.h \
    ircbots/irctipbot/irctipbotwalletmanager.h \
    wallet/coinwalletrequest.h \
    wallet/walletreplyhandler.h

OTHER_FILES += \
    LICENSE.txt \
    README.txt
