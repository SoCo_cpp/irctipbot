#ifndef IRCTIPBOTWALLETMANAGER_H
#define IRCTIPBOTWALLETMANAGER_H

#include <QObject>
#include "wallet/walletreplyhandler.h"

class IrcTipBot; // predefined
class IrcTipbotWalletManager : public WalletReplyHandler
{
	Q_OBJECT
public:
	typedef enum {wrtCreateWallet = 0} WalletRequestType;

	explicit IrcTipbotWalletManager(QObject *parent, IrcTipBot& tipbot);

	bool dbAddWallet(const QString& account, const QString& role, const QString& roleUser);
	bool dbDeleteWalletNoAddress(const QString& account);
	bool dbSetNewWalletAddress(const QString& account, const QString& address);

protected:
	IrcTipBot& _tipbot;

	virtual bool handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest);

signals:

public slots:
};

#endif // IRCTIPBOTWALLETMANAGER_H
