#include "ircbots/irctipbot/irctipbotdatabasedesign.h"
#include "database/databasedatatype.h"
#include "logging/debuglogger.h"
#include <QSqlQuery>
#include <QStringList>
#include <QSqlError>

IrcTipbotDatabaseDesign::IrcTipbotDatabaseDesign(QObject *parent, const QString& databaseType /*= QString()*/) :
	QObject(parent),
	_dbType(databaseType),
	_adminUserTable(this,		databaseType, "TBL_ADMIN_USER"),
	_adminHistoryTable(this,	databaseType, "TBL_ADMIN_HISTORY"),
	_accountTable(this,			databaseType, "TBL_ACCOUNT"),
	_transactionTable(this,		databaseType, "TBL_TRANSACTIONS"),
	_walletTable(this,			databaseType, "TBL_WALLET")
{
	_tableList.append(&_adminUserTable);
	_tableList.append(&_adminHistoryTable);
	_tableList.append(&_accountTable);
	_tableList.append(&_transactionTable);
	_tableList.append(&_walletTable);

	DatabaseDataType dtIndex	= DatabaseDataType(0, DatabaseDataType::tInt64, 0, true/*unsigned*/, true/*unique*/, true/*NotNull*/, true/*AutoInc*/ );
	DatabaseDataType dtText 	= DatabaseDataType(0, DatabaseDataType::tText);
	DatabaseDataType dtTextNN	= DatabaseDataType(0, DatabaseDataType::tText, 0, false/*unsigned*/, false/*unique*/, true/*NotNull*/);
	DatabaseDataType dtUIntNN	= DatabaseDataType(0, DatabaseDataType::tInt16, 0, true/*unsigned*/, false/*unique*/, true/*NotNull*/);
	DatabaseDataType dtValue	= DatabaseDataType(0, DatabaseDataType::tDecimal32, 0);
	DatabaseDataType dtValueNN	= DatabaseDataType(0, DatabaseDataType::tDecimal32, 0, false/*unsigned*/, false/*unique*/, true/*NotNull*/);
	DatabaseDataType dtUValue	= DatabaseDataType(0, DatabaseDataType::tDecimal32, 0, true/*unsigned*/, false/*unique*/, false/*NotNull*/);
	//DatabaseDataType dtUValueNN	= DatabaseDataType(0, DatabaseDataType::tDecimal32, 0, true/*unsigned*/, false/*unique*/, true/*NotNull*/);
	DatabaseDataType dtDate		= DatabaseDataType(0, DatabaseDataType::tDateTime, 0);
	DatabaseDataType dtDateNN	= DatabaseDataType(0, DatabaseDataType::tDateTime, 0, false/*unsigned*/, false/*unique*/, true/*NotNull*/);
	DatabaseDataType dtBool		= DatabaseDataType(0, DatabaseDataType::tBool);

	_adminUserTable.appendFieldDef("IDX",			dtIndex);
	_adminUserTable.appendFieldDef("NICK",			dtText);
	_adminUserTable.appendFieldDef("PRIVILEGES",	dtText);
	_adminUserTable.appendFieldDef("DATE_FIRST",	dtDate);

	_adminHistoryTable.appendFieldDef("IDX",		dtIndex);
	_adminHistoryTable.appendFieldDef("NICK",		dtText);
	_adminHistoryTable.appendFieldDef("USER",		dtText);
	_adminHistoryTable.appendFieldDef("DATE",		dtDate);
	_adminHistoryTable.appendFieldDef("ACTION",		dtText);

	_accountTable.appendFieldDef("IDX",					dtIndex);
	_accountTable.appendFieldDef("USER",				dtText);
	_accountTable.appendFieldDef("BALANCE",				dtValue);
	_accountTable.appendFieldDef("DATE_FIRST",			dtDate);
	_accountTable.appendFieldDef("UPDATE_IDX",			dtUValue);
	_accountTable.appendFieldDef("UPDATE_DATE",			dtDate);
	_accountTable.appendFieldDef("UPDATE_LOCK",			dtText);
	_accountTable.appendFieldDef("UPDATE_LOCK_DATE",	dtDate);


	_transactionTable.appendFieldDef("IDX",			dtIndex);
	_transactionTable.appendFieldDef("TYPE",		dtUIntNN); // TransactionType
	_transactionTable.appendFieldDef("SOURCE",		dtText);
	_transactionTable.appendFieldDef("DESTINATION",	dtText);
	_transactionTable.appendFieldDef("AMOUNT",		dtValueNN);
	_transactionTable.appendFieldDef("DATE",		dtDate);
	_transactionTable.appendFieldDef("CONFIRMED",	dtBool);


	_walletTable.appendFieldDef("IDX",			dtIndex);
	_walletTable.appendFieldDef("ADDRESS",		dtText);
	_walletTable.appendFieldDef("ACCOUNT",		dtTextNN);
	_walletTable.appendFieldDef("ROLE",			dtTextNN);
	_walletTable.appendFieldDef("ROLE_USER",	dtText);
	_walletTable.appendFieldDef("DATE_FIRST",	dtDateNN);
	_walletTable.appendFieldDef("DATE_LAST",	dtDate);

}

DatabaseTableDesign& IrcTipbotDatabaseDesign::adminUserTable()
{
	return _adminUserTable;
}

DatabaseTableDesign& IrcTipbotDatabaseDesign::adminHistoryTable()
{
	return _adminHistoryTable;
}

DatabaseTableDesign& IrcTipbotDatabaseDesign::accountTable()
{
	return _accountTable;
}

DatabaseTableDesign& IrcTipbotDatabaseDesign::transactionTable()
{
	return _transactionTable;
}

DatabaseTableDesign& IrcTipbotDatabaseDesign::walletTable()
{
	return _walletTable;
}

void IrcTipbotDatabaseDesign::setDatabaseType(const QString& databaseType)
{
	_dbType = databaseType;
	for (int i = 0;i < _tableList.size();i++)
		_tableList[i]->setDatabaseType(databaseType);
}

bool IrcTipbotDatabaseDesign::createTables(SqlDatabase& db)
{
	for (int i = 0;i < _tableList.size();i++)
	{
		if (!db.execQuery(_tableList[i]->sqlCreateTable(true/*bIfNotExists*/)))
		{
			DebugLog(dlWarn, "IrcTipbotDatabaseDesign::createTables create table query failed for: %s", qPrintable(_tableList[i]->tableName()));
			return false;
		}
	}
	return true;
}

bool IrcTipbotDatabaseDesign::dropTables(SqlDatabase& db)
{
	for (int i = 0;i < _tableList.size();i++)
	{
		if (!db.execQuery(_tableList[i]->sqlDropTable(true/*bIfExists*/)))
		{
			DebugLog(dlWarn, "IrcTipbotDatabaseDesign::dropTables drop table query failed for: %s", qPrintable(_tableList.at(i)->tableName()));
			return false;
		}
	}
	return true;
}

bool IrcTipbotDatabaseDesign::clearDatabase(SqlDatabase& db)
{
	if (!db.isConnected())
	{
		DebugLog(dlWarn, "IrcTipbotDatabaseDesign::isVerifyDatabase database not open");
		return false;
	}
	DatabaseTableDesign tmpTable;
	tmpTable.setDatabaseType(db.databaseConfig().driverName());

	QStringList dbTables = db.tables();
	for (int i = 0;i < dbTables.size();i++)
	{
		tmpTable.setTableName(dbTables.at(i));
		if (!db.execQuery(tmpTable.sqlDropTable(true/*bIfExists*/)))
		{
			DebugLog(dlWarn, "IrcTipbotDatabaseDesign::clearDatabase drop table query failed for: %s", qPrintable(dbTables.at(i)));
			return false;
		}
	}
	return true;
}

bool IrcTipbotDatabaseDesign::isVerifyDatabase(SqlDatabase& db, bool& bIsInstalled)
{
	if (!db.isConnected())
	{
		DebugLog(dlWarn, "IrcTipbotDatabaseDesign::isVerifyDatabase database not open");
		return false;
	}
	bIsInstalled = true;
	QStringList dbTables = db.tables();
	for (int i = 0;i < _tableList.size();i++)
	{
		if (!dbTables.contains(_tableList.at(i)->tableName()))
		{
			DebugLog(dlWarn, "IrcTipbotDatabaseDesign::isVerifyDatabase table '%s' not found", qPrintable(_tableList.at(i)->tableName()));
			bIsInstalled = false;
		}
	}
	return true;
}
