#ifndef IRCTIPBOT_H
#define IRCTIPBOT_H

#include <QObject>
#include <QTimer>
#include <QJsonObject>
#include <QList>
#include "database/sqldatabase.h"
#include "irc/ircclient.h"
#include "wallet/coinwalletclient.h"
#include "wallet/coinwalletrequest.h"
#include "wallet/walletreplyhandler.h"
#include "ircbots/irctipbot/irctipbotdatabasedesign.h"
#include "ircbots/irctipbot/irctipbotirchandler.h"
#include "ircbots/irctipbot/irctipbotwalletmanager.h"

class IrcTipBot : public WalletReplyHandler
{
	Q_OBJECT
public:
	const static int cNotReadyStatusTimerInterval	= 50; // msec
	const static int cReadyStatusTimerInterval		= 250; // msec

	typedef enum {mStop = 0, mStart, mPause} BotMode;
	typedef enum {bNone = 0,
				  bWaitingConnect, bConnected, bConnectionLost,
				  bCheckWallet, bCheckWalletWait, bCheckWalletDone,
				  bCheckDB, bCheckDBDone,
				  bReady, bError} BotStatus;
	typedef enum {cNone = 0, cDisconnecting, cLostConnection, cDisconnected, cConnecting, cConnected, cLogin, cReady} ConnectionStatus;

	typedef enum {wrtCheck = 0 } WalletRequestType;

	explicit IrcTipBot(QObject *parent = 0);
	~IrcTipBot();

	void setBotMode(BotMode botMode);
	const BotMode& botMode() const;
	QString botModeString() const;

	void setConnectionStatus(ConnectionStatus connectionStatus);
	const ConnectionStatus& connectionStatus() const;
	QString connectionStatusString() const;

	void setBotStatus(BotStatus botStatus);
	const BotStatus& botStatus() const;
	QString botStatusString() const;

	bool isConnected() const;
	bool isDisconnected(bool bFullDisconnect = false) const;
	bool isReady() const;

	const SqlDatabase& database() const;
	SqlDatabase& database();

	const IrcTipbotDatabaseDesign& databaseDesign() const;
	IrcTipbotDatabaseDesign& databaseDesign();

	const IrcClient& irc() const;
	IrcClient& irc();

	const CoinWalletClient& wallet() const;
	CoinWalletClient& wallet();

	const IrcTipbotWalletManager& walletManager() const;
	IrcTipbotWalletManager& walletManager();


	const IrcTipbotIrcHandler& ircHandler() const;
	IrcTipbotIrcHandler& ircHanlder();


	void start();
	void stop();
	void pause();
	void resume();

	bool loadConfig(QJsonObject& mainObj, bool bSharedDatabaseConfig = false, bool bRequireAllFields = false);
	bool saveConfig(QJsonObject& mainObj, bool bSharedDatabaseConfig = false);
	void defaultConfig();

	void debugDumpStatus();

protected:
	BotMode _botMode;
	BotStatus _botStatus;
	ConnectionStatus _connectionStatus;
	SqlDatabase _database;
	IrcTipbotDatabaseDesign _databaseDesign;
	IrcClient _irc;
	CoinWalletClient _wallet;
	IrcTipbotWalletManager _walletManager;
	IrcTipbotIrcHandler _ircHandler;

	QTimer _statusTimer;
	QTimer _timeout;

	void checkMode();
	void checkWallet();
	void checkDatabase();
	virtual bool handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest);

signals:
	void botModeChanged();
	void connectionStatusChanged();
	void botStatusChanged();

public slots:
	void subsystemStatusChanged();
	void statusTimerTick();

};

#endif // IRCTIPBOT_H
