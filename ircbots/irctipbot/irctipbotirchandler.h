#ifndef IRCTIPBOTIRCHANDLER_H
#define IRCTIPBOTIRCHANDLER_H

#include <QObject>
#include "wallet/walletreplyhandler.h"

class IrcTipBot; // predeclared
class IrcTipbotIrcHandler : public WalletReplyHandler
{
	Q_OBJECT
public:
	typedef enum {wrtDiff = 0, wrtCreateDepositWallet} WalletRequestType;
	explicit IrcTipbotIrcHandler(QObject *parent, IrcTipBot& ircTipbot);

	void handleChannelMessage(const QString& nick, const QString& user, const QString& channel, const QString& message);
	void handlePrivateMessage(const QString& nick, const QString& user, const QString& message);
protected:
	IrcTipBot& _ircTipbot;

	virtual bool handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest);
	bool createDepositWallet(const QString& nick);

signals:

public slots:
	void receivedPrivmsg(QString nick, QString user, QString target, QString message);
};

#endif // IRCTIPBOTIRCHANDLER_H
