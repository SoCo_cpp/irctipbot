#ifndef IRCTIPBOTSYSTEM_H
#define IRCTIPBOTSYSTEM_H

#include <QObject>
#include <QList>
#include <QStringList>
#include "ircbots/irctipbot/irctipbot.h"

#define C_IrcTipBot_SystemName			"Irc Tip Bot"
#define C_IrcTipBot_SystemNameShort		"irctipbot"
#define C_IrcTipBot_Version_Major		0
#define C_IrcTipBot_Version_Minor		0
#define C_IrcTipBot_Version_Release		0
#define C_IrcTipBot_Version_Build		1

#define C_IrcTipBot_DebugLog_Filename	"debuglog.txt"
#define C_IrcTipBot_Config_Filename     "irctipbot.config"

class QCoreApplication; // predeclared
class Testing; // predeclared
class IrcTipbotSystem : public QObject
{
	Q_OBJECT
public:
	explicit IrcTipbotSystem();
	~IrcTipbotSystem();

	static IrcTipbotSystem& instance();

	const CoinWalletClient& wallet() const;
	CoinWalletClient& wallet();

	QString dataPath() const;
	QString debugLogFilename() const;
	QString configFilename() const;

	bool start(QCoreApplication* pApp);
	bool initDebugLog();
	bool handlePreConfigArguments(const QStringList& argList);
	bool handlePostConfigArguments(const QStringList& argList);

	bool installPreConfig();
	bool installPostConfig();

	bool installDefaultConfig();
	bool installDatabase();

	bool loadConfig();
	bool saveConfig();
	void defaultConfig();

	void safeQuit(int exitCode = 0);

	void startBots();
	void stopBots();
	void pauseBots();
	void resumeBots();
	bool allBotsIdle();
	void clearBots();

protected:
	QCoreApplication* _pApp;
	int _exitCode;
	IrcTipbotDatabaseDesign _databaseDesign;
	SqlDatabase _database;
	QList<IrcTipBot*> _bots;
	Testing* _pTesting;
	QString _versionString;
	QString _lastVersionString;
	CoinWalletClient _wallet;
	bool _installing;
	bool _testing;


signals:

public slots:
	void databaseConnectionStatusChanged();
	void checkReadyQuit();
	void applicationAboutToQuit();

};

#endif // IRCTIPBOTSYSTEM_H
