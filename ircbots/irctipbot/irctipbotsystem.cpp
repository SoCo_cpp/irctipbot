#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QTimer>
#include <QDebug>
#include "ircbots/irctipbot/irctipbotsystem.h"
#include "logging/debuglogger.h"
#include "testing/testing.h"
#include "files/jsonconfigfile.h"

IrcTipbotSystem::IrcTipbotSystem() :
	QObject(0),
	_exitCode(0),
	_database(0, "System"),
	_installing(false),
	_testing(false)
{
	connect(&_database, SIGNAL(connectionStatusChanged()), this, SLOT(databaseConnectionStatusChanged()));
}

IrcTipbotSystem::~IrcTipbotSystem()
{
}

/*static*/ IrcTipbotSystem& IrcTipbotSystem::instance()
{
	static IrcTipbotSystem instance;
	return instance;
}

QString IrcTipbotSystem::dataPath() const
{
	return QString("%1/.%2").arg(QDir::homePath()).arg(C_IrcTipBot_SystemNameShort);
}

QString IrcTipbotSystem::debugLogFilename() const
{
	return QString("%1/%2").arg(dataPath()).arg(C_IrcTipBot_DebugLog_Filename);
}

QString IrcTipbotSystem::configFilename() const
{
	return QString("%1/%2").arg(dataPath()).arg(C_IrcTipBot_Config_Filename);
}

bool IrcTipbotSystem::start(QCoreApplication* pApp)
{
	Q_ASSERT(pApp);
	_pApp = pApp;
	connect(_pApp, SIGNAL(aboutToQuit()), this, SLOT(applicationAboutToQuit()));
	_versionString = QString("%1.%2.%3.%4").arg(C_IrcTipBot_Version_Major).arg(C_IrcTipBot_Version_Minor).arg(C_IrcTipBot_Version_Release).arg(C_IrcTipBot_Version_Build);
	if (!initDebugLog())
	{
		qDebug() << C_IrcTipBot_SystemName << " initDebugLog failed";
		return false;
	}
	DebugLog(dlNone, "------- %s %s -------", qPrintable(C_IrcTipBot_SystemName), qPrintable(_versionString));

	if (!handlePreConfigArguments(pApp->arguments()))
	{
		DebugLog(dlError, "IrcTipbotSystem::start handlePreConfigArguments failed");
		return false;
	}
	if (!loadConfig())
	{
		DebugLog(dlError, "IrcTipbotSystem::start loadConfig failed");
		return false;
	}
	if (!handlePostConfigArguments(pApp->arguments()))
	{
		DebugLog(dlError, "IrcTipbotSystem::start handlePostConfigArguments failed");
		return false;
	}
	if (!_installing)
		startBots();
	return true;
}

bool IrcTipbotSystem::initDebugLog()
{
	DebugLogInstance().setFileName(debugLogFilename());
	if (!DebugLogInstance().makePath())
	{
		qDebug() << C_IrcTipBot_SystemName << " Failed to make data path for debug log: " << debugLogFilename();
		return false;
	}
	return true;
}

bool IrcTipbotSystem::handlePreConfigArguments(const QStringList& argList)
{
	if (argList.contains("-install"))
	{
		_installing = true;
		DebugLog(dlInfo, "IrcTipbotSystem::handlePreConfigArguments got install command from arguments");
		if (!installPreConfig())
		{
			DebugLog(dlWarn, "IrcTipbotSystem::handlePreConfigArguments installPreConfig failed");
			return false;
		}
	}
	return true;
}

bool IrcTipbotSystem::handlePostConfigArguments(const QStringList& argList)
{
	if (argList.contains("-install"))
	{
		if (!installPostConfig())
		{
			DebugLog(dlWarn, "IrcTipbotSystem::handlePostConfigArguments installPostConfig failed");
			return false;
		}

	}
	if (argList.contains("-test"))
	{
		_testing = true;
		if (!_bots.size())
			DebugLog(dlWarn, "IrcTipbotSystem::installPreConfig test command found, but no tipbots to test with, skipping...");
		else
		{
			_pTesting = new Testing();
			_pTesting->startTest(_bots[0]);
		}
	}
	return true;
}

bool IrcTipbotSystem::installPreConfig()
{
	if (!QFile::exists(configFilename()))
	{
		if (!installDefaultConfig())
		{
			DebugLog(dlWarn, "IrcTipbotSystem::installDefaultConfig installDefaultConfig failed");
			return false;
		}
		DebugLog(dlInfo, "IrcTipbotSystem::installPreConfig config file not found, creating default. Please fill in required fields: %s", qPrintable(configFilename()));
		return false;
	}
	else DebugLog(dlInfo, "IrcTipbotSystem::installPreConfig config file found continuing to load config for rest of install");
	return true;
}

bool IrcTipbotSystem::installPostConfig()
{
	if (!_database.connectDatabase())
	{
		DebugLog(dlError, "IrcTipbotSystem::installPostConfig connectDatabase failed");
		return false;
	}
	return true;
}

bool IrcTipbotSystem::installDefaultConfig()
{
	defaultConfig();
	if (!saveConfig())
	{
		DebugLog(dlError, "IrcTipbotSystem::installDefaultConfig config file not found, save created default failed: %s", qPrintable(configFilename()));
		return false;
	}
	return true;
}

bool IrcTipbotSystem::installDatabase()
{
	bool isValid;
	if (!_databaseDesign.isVerifyDatabase(_database, isValid))
	{
		DebugLog(dlWarn, "IrcTipbotSystem::installDatabase database design failed to verify database tables: check failed");
		return false;
	}
	if (isValid)
	{
		DebugLog(dlInfo, "IrcTipbotSystem::installDatabase database design verified all tables exist: uninstall to reinstall tables");
		return true;
	}
	DebugLog(dlInfo, "IrcTipbotSystem::installDatabase database design not verified, resinstalling");
	if (!_databaseDesign.clearDatabase(_database))
	{
		DebugLog(dlError, "IrcTipbotSystem::installDatabase database design failed to clearDatabase");
		return false;
	}
	if (!_databaseDesign.createTables(_database))
	{
		DebugLog(dlError, "IrcTipbotSystem::installDatabase database design failed to create tables");
		return false;
	}
	if (!_databaseDesign.isVerifyDatabase(_database, isValid))
	{
		DebugLog(dlWarn, "IrcTipbotSystem::installDatabase database design failed to verify newly installed database tables: check failed");
		return false;
	}
	if (!isValid)
	{
		DebugLog(dlInfo, "IrcTipbotSystem::installDatabase database design failed to verify newly created database tables: verify failed");
		return true;
	}
	return true;
}

bool IrcTipbotSystem::loadConfig()
{
	QJsonObject mainObj;
	QJsonArray tipbotArray;
	JsonConfigFile cfgFile;
	cfgFile.setFileName(configFilename());
	if (!cfgFile.exists())
	{
		DebugLog(dlWarn, "IrcTipbotSystem::loadConfig config file doesn't exist");
		return false;
	}
	if (!cfgFile.load())
	{
		DebugLog(dlWarn, "IrcTipbotSystem::loadConfig config file load failed");
		return false;
	}
	if (cfgFile.json().isEmpty())
	{
		DebugLog(dlWarn, "IrcTipbotSystem::loadConfig config file load failed, read empty data");
		return false;
	}
	if (!cfgFile.json().isObject())
	{
		DebugLog(dlWarn, "IrcTipbotSystem::loadConfig config file load failed, read non json object");
		return false;
	}
	mainObj = cfgFile.json().object();
	if (!mainObj.contains("config-version"))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'config-version' not found");
		return false;
	}
	if (!mainObj.contains("system-name"))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-name' not found");
		return false;
	}
	if (mainObj["system-name"].toString() != C_IrcTipBot_SystemNameShort)
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file 'system-name' does not match");
		return false;
	}
	if (!mainObj.contains("system-version"))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-name' not found");
		return false;
	}
	if (!mainObj.contains("system-database"))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-database' not found");
		return false;
	}
	if (!mainObj["system-database"].isObject())
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-database' is not a proper Json object");
		return false;
	}
	if (!mainObj.contains("system-tipbots"))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-tipbots' not found");
		return false;
	}
	if (!mainObj["system-tipbots"].isArray())
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig config file required field 'system-tipbots' is not a proper Json array");
		return false;
	}

	_lastVersionString = mainObj["system-version"].toString();

	if (!_database.databaseConfig().loadJson(mainObj["system-database"].toObject(), true/*bRequireAllFields*/))
	{
		DebugLog(dlError, "IrcTipbotSystem::loadConfig database config failed to loadJson");
		return false;
	}

	tipbotArray = mainObj["system-tipbots"].toArray();
	DebugLog(dlInfo, "IrcTipbotSystem::loadConfig detected %d tipbots in configuration", tipbotArray.size());
	for (int i = 0;i < tipbotArray.size();i++)
	{
		if (!tipbotArray.at(i).isObject())
		{
			DebugLog(dlError, "IrcTipbotSystem::loadConfig tipbot %d is not a proper Json object", i);
			return false;
		}
		QJsonObject obj = tipbotArray[i].toObject();
		IrcTipBot* pTipbot = new IrcTipBot(this);
		if (!pTipbot->loadConfig(obj, true/*bSharedDatabaseConfig*/, true/*bRequireAllFields*/))
		{
			pTipbot->deleteLater();
			DebugLog(dlError, "IrcTipbotSystem::loadConfig tipbot %d failed to load from config file", i);
			return false;
		}
		pTipbot->database().databaseConfig().copy(_database.databaseConfig());
		_bots.append(pTipbot);
	}
	_databaseDesign.setDatabaseType(_database.databaseConfig().driverName());
	DebugLog(dlInfo, "IrcTipbotSystem::loadConfig load complete");
	return true;
}

bool IrcTipbotSystem::saveConfig()
{
	QJsonArray tipbotArray;
	JsonConfigFile cfgFile;
	QJsonObject mainObj;
	QJsonObject dbObj;

	_database.databaseConfig().saveJson(dbObj);

	for (int i = 0;i < _bots.size();i++)
	{
		QJsonObject obj;
		if (!_bots[i]->saveConfig(obj, true/*bSharedDatabaseConfig*/))
		{
			DebugLog(dlError, "IrcTipbotSystem::saveConfig tipbot %d failed to save to config file", i);
			return false;
		}
		tipbotArray.append(QJsonValue(obj));
	}

	mainObj["config-version"]	= 1;
	mainObj["system-name"]		= QString(C_IrcTipBot_SystemNameShort);
	mainObj["system-version"]	= _versionString;
	mainObj["system-database"]	= QJsonValue(dbObj);
	mainObj["system-tipbots"]	= QJsonValue(tipbotArray);

	cfgFile.json().setObject(mainObj);
	cfgFile.setFileName(configFilename());
	cfgFile.setCompactJson(false);
	if (!cfgFile.save())
	{
		DebugLog(dlError, "IrcTipbotSystem::saveConfig config file  save failed");
		return false;
	}
	return true;
}

void IrcTipbotSystem::defaultConfig()
{
	IrcTipBot* pTipbot = new IrcTipBot(this);
	_bots.append(pTipbot);
	pTipbot->defaultConfig();

	_database.databaseConfig() = SqlDatabaseConfig();
	_database.databaseConfig().serverConfig() = ServerConfig();

	_database.databaseConfig().serverConfig().set("localhost", 3306, false/*SSL*/, "Required_DB_User", "Required_DB_Password");
	_database.databaseConfig().setDriverName("MYSQL");

}

void IrcTipbotSystem::databaseConnectionStatusChanged()
{
	DebugLog(dlInfo, "IrcTipbotSystem::databaseConnectionStatusChanged instaling: %c, status: %s", BoolYN(_installing), qPrintable(_database.connectionStatusString()));
	if (_installing)
	{
		if (_database.isConnected())
		{
			DebugLog(dlInfo, "IrcTipbotSystem::databaseConnectionStatusChanged database connected, starting install");
			if (installDatabase())
			{
				DebugLog(dlInfo, "IrcTipbotSystem::databaseConnectionStatusChanged database install successful, terminating");
				safeQuit(0);
			}
			else
			{
				DebugLog(dlError, "IrcTipbotSystem::databaseConnectionStatusChanged installDatabase failed");
				safeQuit(1);
			}
		}
		else if (_database.connectionStatus() == SqlDatabase::cError)
		{
			DebugLog(dlError, "IrcTipbotSystem::databaseConnectionStatusChanged database connection failed");
			safeQuit(1);
		}
	} // if (_installing)
}

void IrcTipbotSystem::safeQuit(int exitCode /*= 0*/)
{
	_exitCode = exitCode;
	if (!_database.disconnectDatabase())
		DebugLog(dlError, "IrcTipbotSystem::safeQuit disconnectDatabase failed");
	stopBots();
	QTimer::singleShot(500, this, SLOT(checkReadyQuit()));
}

void IrcTipbotSystem::startBots()
{
	for (int i = 0;i < _bots.size();i++)
		_bots[i]->start();
}

void IrcTipbotSystem::stopBots()
{
	for (int i = 0;i < _bots.size();i++)
		_bots[i]->stop();
}

void IrcTipbotSystem::pauseBots()
{
	for (int i = 0;i < _bots.size();i++)
		_bots[i]->pause();
}

void IrcTipbotSystem::resumeBots()
{
	for (int i = 0;i < _bots.size();i++)
		_bots[i]->resume();
}

bool IrcTipbotSystem::allBotsIdle()
{
	for (int i = 0;i < _bots.size();i++)
		if (_bots.at(i)->botMode() != IrcTipBot::mStop || !_bots.at(i)->isDisconnected(true/*bFullDisconnect*/))
			return false;
	return true;
}

void IrcTipbotSystem::clearBots()
{
	DebugLog(dlDebug, "IrcTipbotSystem::clearBots clearing...");
	for (int i = 0;i < _bots.size();i++)
		delete _bots[i];
	_bots.clear();
	DebugLog(dlDebug, "IrcTipbotSystem::clearBots clearing...done");
}

void IrcTipbotSystem::checkReadyQuit()
{
	if (_database.isDisconnected(true/*bFullDisconnect*/) && allBotsIdle())
	{
		DebugLog(dlDebug, "IrcTipbotSystem::checkReadyQuit emitting quit...");
		emit QCoreApplication::quit();
	}
	else QTimer::singleShot(500, this, SLOT(checkReadyQuit()));
}


void IrcTipbotSystem::applicationAboutToQuit()
{
	DebugLog(dlInfo, "IrcTipbotSystem::applicationAboutToQuit stopping subsystems...");
	DebugLog(dlInfo, "IrcTipbotSystem::applicationAboutToQuit stopping subsystems...DONE");
}
