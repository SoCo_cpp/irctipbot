#include "ircbots/irctipbot/irctipbot.h"
#include "logging/debuglogger.h"
#include "ircbots/irctipbot/irctipbotsystem.h"
#include <QUuid>

IrcTipBot::IrcTipBot(QObject *parent) :
	WalletReplyHandler(parent, _wallet),
	_botMode(mStop),
	_botStatus(bNone),
	_connectionStatus(cNone),
	_walletManager(0, *this),
	_ircHandler(0, *this)
{
	_database.setConnectionName(QUuid::createUuid().toString());
	connect(&_irc, SIGNAL(clientStatusChanged()), this, SLOT(subsystemStatusChanged()));
	connect(&_irc, SIGNAL(loginStatusChanged()), this, SLOT(subsystemStatusChanged()));
	connect(&_database, SIGNAL(connectionStatusChanged()), this, SLOT(subsystemStatusChanged()));
	connect(&_statusTimer, SIGNAL(timeout()), this, SLOT(statusTimerTick()));
	_statusTimer.start(cNotReadyStatusTimerInterval);
}

IrcTipBot::~IrcTipBot()
{
}

void IrcTipBot::setBotMode(BotMode botMode)
{
	_botMode = botMode;
	emit botModeChanged();
}

const IrcTipBot::BotMode& IrcTipBot::botMode() const
{
	return _botMode;
}

QString IrcTipBot::botModeString() const
{
	switch (_botMode)
	{
		case mStop:		return "Stop";
		case mStart:	return "Start";
		case mPause:	return "Pause";
		default:		return QString("Unknown(%1)").arg(_botMode);
	} // switch
}

void IrcTipBot::setConnectionStatus(ConnectionStatus connectionStatus)
{
	_connectionStatus = connectionStatus;
	emit connectionStatusChanged();
}

const IrcTipBot::ConnectionStatus& IrcTipBot::connectionStatus() const
{
	return _connectionStatus;
}

QString IrcTipBot::connectionStatusString() const
{
	switch (_connectionStatus)
	{
		case cNone:				return "None";
		case cDisconnecting:	return "Disconnecting";
		case cLostConnection:	return "LostConnection";
		case cDisconnected:		return "Disconnected";
		case cConnecting:		return "Connecting";
		case cConnected:		return "Connected";
		case cLogin:			return "Login";
		case cReady:			return "Ready";
		default:				return QString("Unknown(%1)").arg(_connectionStatus);
	} // switch
}

void IrcTipBot::setBotStatus(BotStatus botStatus)
{
	_botStatus = botStatus;
	emit botStatusChanged();
}

const IrcTipBot::BotStatus& IrcTipBot::botStatus() const
{
	return _botStatus;
}

QString IrcTipBot::botStatusString() const
{
	switch (_botStatus)
	{
		case bNone:				return "None";
		case bWaitingConnect:	return "WaitingConnect";
		case bConnected:		return "Connected";
		case bConnectionLost:	return "ConnectionLost";
		case bCheckWallet:		return "CheckWallet";
		case bCheckWalletWait:	return "CheckWalletWait";
		case bCheckWalletDone:	return "CheckWalletDone";
		case bCheckDB:			return "CheckDB";
		case bCheckDBDone:		return "CheckDBDone";
		case bReady:			return "Ready";
		case bError:			return "Error";
		default:				return QString("Unknown(%1)").arg(_botStatus);
	} // switch
}

bool IrcTipBot::isConnected() const
{
	switch (_connectionStatus)
	{
		case cConnected:
		case cLogin:
		case cReady:
			return true;
		default:
			return false;

	} // switch (_clientStatus)
}

bool IrcTipBot::isDisconnected(bool bFullDisconnect /*= false*/) const
{
	switch (_connectionStatus)
	{
		case cNone:
		case cDisconnected:
			return true;
		case cDisconnecting:
			return !bFullDisconnect;
		default:
			return false;
	} // switch (_clientStatus)
}

bool IrcTipBot::isReady() const
{
	return (_botStatus == bReady);
}

const SqlDatabase& IrcTipBot::database() const
{
	return _database;
}

SqlDatabase& IrcTipBot::database()
{
	return _database;
}

const IrcTipbotDatabaseDesign& IrcTipBot::databaseDesign() const
{
	return _databaseDesign;
}

IrcTipbotDatabaseDesign& IrcTipBot::databaseDesign()
{
	return _databaseDesign;
}

const IrcClient& IrcTipBot::irc() const
{
	return _irc;
}

IrcClient& IrcTipBot::irc()
{
	return _irc;
}

const CoinWalletClient& IrcTipBot::wallet() const
{
	return _wallet;
}

CoinWalletClient& IrcTipBot::wallet()
{
	return _wallet;
}

const IrcTipbotWalletManager& IrcTipBot::walletManager() const
{
	return _walletManager;
}

IrcTipbotWalletManager& IrcTipBot::walletManager()
{
	return _walletManager;
}

const IrcTipbotIrcHandler& IrcTipBot::ircHandler() const
{
	return _ircHandler;
}

IrcTipbotIrcHandler& IrcTipBot::ircHanlder()
{
	return _ircHandler;
}

void IrcTipBot::start()
{
	setBotMode(mStart);
}

void IrcTipBot::stop()
{
	setBotMode(mStop);
}

void IrcTipBot::pause()
{
	setBotMode(mPause);
}

void IrcTipBot::resume()
{
	setBotMode(mStart);
}

bool IrcTipBot::loadConfig(QJsonObject& mainObj, bool bSharedDatabaseConfig /*= false*/, bool bRequireAllFields /*= false*/)
{
	if (bRequireAllFields)
		if ( !mainObj.contains("irc") || !mainObj.contains("wallet") || (!bSharedDatabaseConfig && !mainObj.contains("db"))  )
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig all fields required, bot not all found");
			return false;
		}

	if (!mainObj.contains("ver"))
	{
		DebugLog(dlWarn, "IrcTipBot::loadConfig version field missing");
		return false;
	}

	if (mainObj.contains("irc"))
	{
		if (!mainObj["irc"].isObject())
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig irc field is not an object");
			return false;
		}
		if (!_irc.config().loadJson(mainObj["irc"].toObject(), bRequireAllFields))
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig irc config failed to loadJson");
			return false;
		}
	}

	if (!bSharedDatabaseConfig && mainObj.contains("db"))
	{
		if (!mainObj["db"].isObject())
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig db field is not an object");
			return false;
		}
		if (!_database.databaseConfig().loadJson(mainObj["db"].toObject(), bRequireAllFields))
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig db config failed to loadJson");
			return false;
		}
	}

	if (mainObj.contains("wallet"))
	{
		if (!mainObj["wallet"].isObject())
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig wallet field is not an object");
			return false;
		}
		if (!_wallet.config().loadJson(mainObj["wallet"].toObject(), bRequireAllFields))
		{
			DebugLog(dlWarn, "IrcTipBot::loadConfig wallet config failed to loadJson");
			return false;
		}
	}
	return true;
}

bool IrcTipBot::saveConfig(QJsonObject& mainObj, bool bSharedDatabaseConfig /*= false*/)
{
	QJsonObject ircObj;
	QJsonObject dbObj;
	QJsonObject walletObj;

	_irc.config().saveJson(ircObj);
	_database.databaseConfig().saveJson(dbObj);
	_wallet.config().saveJson(walletObj);

	mainObj["ver"]		= 1;
	mainObj["irc"]		= QJsonValue(ircObj);
	mainObj["wallet"]	= QJsonValue(walletObj);
	if (!bSharedDatabaseConfig)
		mainObj["db"]	= QJsonValue(dbObj);
	return true;
}

void IrcTipBot::defaultConfig()
{
	_irc.config() = IrcClientConfig();
	_irc.config().accountConfig().set("Required_IRC_Nick", "Required_IRC_User", "Required_IRC_RealName", "Required_IRC_NickservPassword");
	_irc.config().serverConfig().set("Required_IRC_Host", 6667);

	_database.databaseConfig() = SqlDatabaseConfig();
	_database.databaseConfig().serverConfig() = ServerConfig();

	_database.databaseConfig().serverConfig().set("localhost", 3306, false/*SSL*/, "Required_DB_User", "Required_DB_Password");
	_database.databaseConfig().setDriverName("QMYSQL");

	_wallet.config() = CoinWalletClientConfig();
	_wallet.config().serverConfig().set("localhost", 9332, false /*SSL*/, "Required_Wallet_RPC_User", "Required_Wallet_RPC_Password");
}

void IrcTipBot::subsystemStatusChanged()
{
	DebugLog(dlDebug, "IrcTipBot::subsystemStatusChanged irc status: %s irc login: %s database status: %s",
			 qPrintable(_irc.clientStatusString()), qPrintable(_irc.loginStatusString()), qPrintable(_database.connectionStatusString()));

	if (_irc.isConnected() && _database.isConnected())
	{
		DebugLog(dlDebug, "IrcTipBot::subsystemStatusChanged all connected, Irc ready: %c", BoolYN(_irc.isReady()));
		if (_irc.isReady())
		{
			setConnectionStatus(cReady);
			setBotStatus(bConnected);
		}
		else
			setConnectionStatus(cConnected);
	}
	else
	{
		if (_irc.isDisconnected() && _database.isDisconnected())
		{
			setConnectionStatus(cDisconnected);
			setBotStatus(bNone);
		}
		else
		{
			if ((_connectionStatus != cConnecting && _connectionStatus != cDisconnecting) && (_irc.isDisconnected() || _database.isDisconnected()))
				setConnectionStatus(cLostConnection);
			setBotStatus(bWaitingConnect);
		}
	}
}

void IrcTipBot::statusTimerTick()
{
	checkMode();
	if (isReady())
	{
		if (_statusTimer.interval() != cReadyStatusTimerInterval)
			_statusTimer.start(cReadyStatusTimerInterval);
	}
	else // isReady()
	{
		switch (_botStatus)
		{
			case bNone:
				break;
			case bWaitingConnect:
				break;
			case bConnected:
				_timeout.stop();
				setBotStatus(bCheckWallet);
				break;
			case bConnectionLost:
				break;
			case bCheckWallet:
				checkWallet();
				break;
			case bCheckWalletWait:
				break;
			case bCheckWalletDone:
				_timeout.stop();
				setBotStatus(bCheckDB);
				break;
			case bCheckDB:
				checkDatabase();
				break;
			case bCheckDBDone:
				setBotStatus(bReady);
				break;
			default:
				// ignore rest
				break;
		} // switch (_botStatus)
		if (_statusTimer.interval() != cNotReadyStatusTimerInterval)
			_statusTimer.start(cNotReadyStatusTimerInterval);
	} // else // isReady()
}

void IrcTipBot::checkMode()
{
	if (isDisconnected())
	{
		if (_botMode == mStart || _botMode == mPause)
		{
			if (_connectionStatus != cLostConnection)
			{
				DebugLog(dlWarn, "IrcTipBot::checkMode connecting...(db status: %s) ", qPrintable(_database.connectionStatusString()));
				debugDumpStatus();
				setConnectionStatus(cConnecting);
				setBotStatus(bWaitingConnect);
				_databaseDesign.setDatabaseType(_database.databaseConfig().driverName());
				if (!_irc.connectClient())
				{
					DebugLog(dlWarn, "IrcTipBot::checkMode irc client connectClient failed");
					setBotStatus(bError);
				}
				else if (!_database.connectDatabase())
				{
					DebugLog(dlWarn, "IrcTipBot::checkMode database client connectDatabase failed");
					setBotStatus(bError);
				}
				else
				{
					//_timeout.singleShot(15000, this, SLOT(timeout()));
				}
			}
		} // botMode start/pause
	}
	else if (isConnected())
	{
		if (_botMode == mStop)
		{
			DebugLog(dlWarn, "IrcTipBot::checkMode disconnecting...");
			debugDumpStatus();
			setConnectionStatus(cDisconnecting);
			if (!_irc.disconnectClient("Stopping"))
				DebugLog(dlWarn, "IrcTipBot::checkMode irc client disconnectClient failed");
			if (!_database.disconnectDatabase())
				DebugLog(dlWarn, "IrcTipBot::checkMode database client disconnectDatabase failed");
		}
	} // else // if (isDisconnected())
}

void IrcTipBot::checkWallet()
{
	setBotStatus(bCheckWalletWait);
	addWalletRequest((int)wrtCheck)->getInfo();
}

void IrcTipBot::checkDatabase()
{
	bool isValid;
	if (!_databaseDesign.isVerifyDatabase(_database, isValid))
	{
		DebugLog(dlWarn, "IrcTipBot::checkDatabase database design failed to verify database tables: check failed");
		setBotStatus(bError);
	}
	else if (!isValid)
	{
		DebugLog(dlWarn, "IrcTipBot::checkDatabase database design failed to verify database tables: invalid or not installed");
		setBotStatus(bError);
	}
	else setBotStatus(bCheckDBDone);
}

void IrcTipBot::debugDumpStatus()
{
	DebugLog(dlDebug, "IrcTipBot::debugDumpStatus Tipbot mode: %s, status: %s, con: %s", qPrintable(botModeString()), qPrintable(botStatusString()), qPrintable(connectionStatusString()));

}

bool IrcTipBot::handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest)
{
	switch ((WalletRequestType)requestType)
	{
		case wrtCheck:
			if (_botStatus != bCheckWalletWait)
			{
				DebugLog(dlWarn, "IrcTipBot::handleWalletRequestReply got unexpected wallet check reply. BotStatus: %s", qPrintable(botStatusString()));
				return false;
			}
			if (success)
				setBotStatus(bCheckWalletDone);
			else
			{
				DebugLog(dlWarn, "IrcTipBot::handleWalletRequestReply wallet check replied with error: %s", qPrintable(pRequest->errorString()));
				setBotStatus(bError);
			}
			break;
		default:
			DebugLog(dlWarn, "IrcTipBot::handleWalletRequestReply invalid request type: %d", requestType);
			return false;
	} // switch (reqType)
	return true;
}
