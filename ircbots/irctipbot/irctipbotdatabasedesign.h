#ifndef IRCTIPBOTDATABASEDESIGN_H
#define IRCTIPBOTDATABASEDESIGN_H

#include <QObject>
#include <QList>
#include "database/sqldatabase.h"
#include "database/databasetabledesign.h"



class IrcTipbotDatabaseDesign : public QObject
{
	Q_OBJECT
public:
	typedef enum {ttDeposit = 0, ttWithdraw, ttTip} TransactionType;

	explicit IrcTipbotDatabaseDesign(QObject *parent = 0, const QString& databaseType = QString());

	DatabaseTableDesign& adminUserTable();
	DatabaseTableDesign& adminHistoryTable();
	DatabaseTableDesign& accountTable();
	DatabaseTableDesign& transactionTable();
	DatabaseTableDesign& walletTable();


	void setDatabaseType(const QString& databaseType);

	bool createTables(SqlDatabase& db);
	bool dropTables(SqlDatabase& db);
	bool clearDatabase(SqlDatabase& db);
	bool isVerifyDatabase(SqlDatabase& db, bool& bIsInstalled);


protected:
	QString _dbType;
	DatabaseTableDesign _adminUserTable;
	DatabaseTableDesign _adminHistoryTable;
	DatabaseTableDesign _accountTable;
	DatabaseTableDesign _transactionTable;
	DatabaseTableDesign _walletTable;
	QList<DatabaseTableDesign*> _tableList;


signals:

public slots:

};

#endif // IRCTIPBOTDATABASEDESIGN_H
