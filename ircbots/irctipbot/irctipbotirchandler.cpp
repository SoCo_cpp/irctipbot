#include "ircbots/irctipbot/irctipbotirchandler.h"
#include "ircbots/irctipbot/irctipbot.h"
#include "logging/debuglogger.h"
#include "ircbots/irctipbot/irctipbotsystem.h"

IrcTipbotIrcHandler::IrcTipbotIrcHandler(QObject *parent, IrcTipBot& ircTipbot) :
	WalletReplyHandler(parent, ircTipbot.wallet()),
	_ircTipbot(ircTipbot)
{
	connect(&_ircTipbot.irc().handler(), SIGNAL(receivedPrivmsg(QString,QString,QString,QString)), this, SLOT(receivedPrivmsg(QString,QString,QString,QString)));

}

void IrcTipbotIrcHandler::receivedPrivmsg(QString nick, QString user, QString target, QString message)
{
	if (target.startsWith(QChar('#'))) // Channel message
		handleChannelMessage(nick, user, target, message);
	else if (target.toLower() == _ircTipbot.irc().config().accountConfig().nick().toLower())
		handlePrivateMessage(nick, user, message);
	else
		DebugLog(dlWarn, "IrcTipbotIrcHandler::receivedPrivmsg target is my nick and doesn't look like a channel name. nick: %s, user: %s, target: %s, message: %s", qPrintable(nick), qPrintable(user), qPrintable(target), qPrintable(message));
}

void IrcTipbotIrcHandler::handleChannelMessage(const QString& nick, const QString& user, const QString& channel, const QString& message)
{
	Q_UNUSED(nick);
	Q_UNUSED(user);
	Q_UNUSED(channel);
	if (message.startsWith("!diff"))
	{
		CoinWalletRequest* pRequest = addWalletRequest((int)wrtDiff);
		pRequest->params().append(QVariant(nick));
		pRequest->params().append(QVariant(channel));
		pRequest->getDifficulty();
	}

}

void IrcTipbotIrcHandler::handlePrivateMessage(const QString& nick, const QString& user, const QString& message)
{
	Q_UNUSED(nick);
	Q_UNUSED(user);
	if (message.startsWith("!quit"))
	{
		IrcTipbotSystem::instance().safeQuit();
	}
	else if (message.startsWith("!deposit"))
	{
		if (!createDepositWallet(nick))
		{
			DebugLog(dlWarn, "IrcTipbotIrcHandler::handlePrivateMessage deposit, walletManager failed to createDepositWallet");
		}
	}

}

bool IrcTipbotIrcHandler::createDepositWallet(const QString& nick)
{
	QString account = QUuid::createUuid().toString();
	if (!_ircTipbot.walletManager().dbAddWallet(account, "deposit", nick))
	{
		DebugLog(dlWarn, "IrcTipbotIrcHandler::createDepositWallet dbAddWallet failed");
		return false;
	}
	CoinWalletRequest* pRequest = addWalletRequest((int)wrtCreateDepositWallet);
	pRequest->params().append(QVariant(nick));
	pRequest->params().append(QVariant(nick));
	pRequest->params().append(QVariant(account));
	pRequest->getNewAddress(account);
	return true;
}

bool IrcTipbotIrcHandler::handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest)
{
	QString source, destination, message;
	QJsonValue jsonValue = pRequest->jsonValue();
	switch (requestType)
	{
		case wrtDiff:
			if (pRequest->params().size() < 3)
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply diff, request doesn't have enough parameters. Param count: %d", pRequest->params().size());
				return false;
			}

			source		= pRequest->params().at(1).toString();
			destination = pRequest->params().at(2).toString();
			if (source.isEmpty() || destination.isEmpty())
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply diff, source and distination not verified. source: '%s', dest: '%s'", qPrintable(source), qPrintable(destination));
				return false;
			}
			if (!jsonValue.isDouble())
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply diff, reply not a double as expected: %s", qPrintable(pRequest->dumpJsonValue("diff result")));
				return false;
			}
			if (!success)
				message = "Difficulty: <error>";
			else
				message = QString("Difficulty: %1").arg(jsonValue.toDouble());
			emit _ircTipbot.irc().sendPrivateMessage(destination, message);
			break;
		case wrtCreateDepositWallet:
			if (pRequest->params().size() < 4)
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, request doesn't have enough parameters. Param count: %d", pRequest->params().size());
				return false;
			}
			source		= pRequest->params().at(1).toString();
			destination = pRequest->params().at(2).toString();
			if (source.isEmpty() || destination.isEmpty())
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, source and distination not verified. source: '%s', dest: '%s'", qPrintable(source), qPrintable(destination));
				return false;
			}
			if (!jsonValue.isString() || jsonValue.toString().isEmpty())
			{
				DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, reply is not valid or empty: %s", qPrintable(pRequest->dumpJsonValue("create wallet reply")));
				return false;
			}
			if (!success)
				message = "Difficulty: <error>";
			else
				message = QString("Difficulty: %1").arg(jsonValue.toDouble());


			if (pRequest->params().size() < 4 )
				DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, not enough parameters: %d", pRequest->params().size());
			else
			{
				QString account = pRequest->params().at(3).toString();
				source		= pRequest->params().at(1).toString();
				destination = pRequest->params().at(2).toString();
				if (account.isEmpty())
				{
					DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, account parameter required but empty");
					return false;
				}
				if (source.isEmpty() || destination.isEmpty())
				{
					DebugLog(dlError, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, source and distination not verified. source: '%s', dest: '%s'", qPrintable(source), qPrintable(destination));
					success = false;
				}
				else
				{
					if (!success)
						DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply ccreate deposit wallet, request failed");
					else if (!jsonValue.isString() || jsonValue.toString().isEmpty())
					{
						DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, reply is not valid or empty: %s", qPrintable(pRequest->dumpJsonValue("create wallet reply")));
						success = false;
					}
					else
					{
						DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, repplied with account: %s, address: %s", qPrintable(account), qPrintable(jsonValue.toString()));
						if (!_ircTipbot.walletManager().dbSetNewWalletAddress(account, jsonValue.toString()))
						{
							DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, walletManager dbSetNewWalletAddress failed");
							return false;
						}
						else
						{
							QString message = QString("Deposit address: %1").arg(jsonValue.toString());
							emit _ircTipbot.irc().sendPrivateMessage(destination, message);
						}
					}
				}
				if (!success)
					if (!_ircTipbot.walletManager().dbDeleteWalletNoAddress(account))
						DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply create deposit wallet, walletManager dbDeleteWalletNoAddress failed");
			}
			break;
		default:
			DebugLog(dlWarn, "IrcTipbotIrcHandler::handleWalletRequestReply invalid request type: %d", requestType);
			return false;
	} // switch (reqType)
	return true;
}

