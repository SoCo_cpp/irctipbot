#include "ircbots/irctipbot/irctipbotwalletmanager.h"
#include "ircbots/irctipbot/irctipbot.h"
#include "wallet/coinwalletclient.h"
#include "logging/debuglogger.h"
#include <QUuid>

IrcTipbotWalletManager::IrcTipbotWalletManager(QObject *parent, IrcTipBot& tipbot) :
	WalletReplyHandler(parent, tipbot.wallet()),
	_tipbot(tipbot)
{
}

bool IrcTipbotWalletManager::dbAddWallet(const QString& account, const QString& role, const QString& roleUser)
{
	DatabaseTableDesign& table = _tipbot.databaseDesign().walletTable();
	table.clearValues();
	table.setValue("ACCOUNT", account);
	table.setValue("ROLE", role);
	table.setValue("ROLE_USER", roleUser);
	table.setValue("DATE_FIRST", "NOW()", true/*noQuoteValue*/);
	if (!_tipbot.database().execQuery(table.sqlInsert()))
	{
		DebugLog(dlWarn, "IrcTipbotWalletManager::dbAddWallet database execQuery failed");
		return false;
	}
	return true;
}

bool IrcTipbotWalletManager::dbDeleteWalletNoAddress(const QString& account)
{
	DatabaseTableDesign& table = _tipbot.databaseDesign().walletTable();
	if (!_tipbot.database().execQuery(table.sqlDelete(QString("ACCOUNT='%1' AND ADDRESS IS NULL").arg(account))))
	{
		DebugLog(dlWarn, "IrcTipbotWalletManager::dbDeleteWalletNoAddress database execQuery failed. account: %s", qPrintable(account));
		return false;
	}
	return true;
}

bool IrcTipbotWalletManager::dbSetNewWalletAddress(const QString& account, const QString& address)
{
	DatabaseTableDesign& table = _tipbot.databaseDesign().walletTable();
	table.clearValues();
	table.setValue("ADDRESS", address);
	QString sql = table.sqlUpdate(QString("ACCOUNT='%1' AND ADDRESS IS NULL").arg(account), 1/*limit*/);
	DebugLog(dlDebug, " IrcTipbotWalletManager::dbSetNewWalletAddress sql: %s", qPrintable(sql));
	if (!_tipbot.database().execQuery(sql))
	{
		DebugLog(dlWarn, "IrcTipbotWalletManager::dbSetNewWalletAddress database execQuery failed. account: %s", qPrintable(account));
		return false;
	}
	return true;
}

bool IrcTipbotWalletManager::handleWalletRequestReply(int requestType, bool success, CoinWalletRequest* pRequest)
{
	Q_UNUSED(success);
	Q_UNUSED(pRequest);
	//QJsonValue jsonValue = pRequest->jsonValue();
	switch (requestType)
	{
		default:
			DebugLog(dlWarn, "IrcTipbotWalletManager::handleWalletRequestReply invalid request type: %d", requestType);
			return false;
	} // switch (reqType)
	return true;

}
