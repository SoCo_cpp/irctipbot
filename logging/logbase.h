#ifndef LOGBASE_H
#define LOGBASE_H

#include <QObject>

class LogBase : public QObject
{
    Q_OBJECT
public:
	explicit LogBase(QObject *parent = 0, const QString& sLogName = "Unnamed", const QString& sFileName = "", const QString& sLineDelimiter = "\r\n");

	void setLogName(const QString& sFileName);
	const QString& logName() const;

	void setFileName(const QString& sFileName);
	bool isFileNameSet() const;
    const QString& fileName() const;
	QString filePath() const;

	bool fileExists() const;
	bool filePathExists() const;
	bool makePath(bool failIfExists = false) const;

	void setLineDelimiter(const QString& sLineDelimiter);
	const QString& lineDelimiter() const;

	bool outputRaw(const QString& sRaw) const;
	bool outputRawLine(const QString& sLine) const;

	void consoleWarn(const char* strFmt, ...) const;
	void consoleWarn(const QString& sMsg) const;

protected:
	QString _logName;
	QString _fileName;
    QString _lineDelimiter;

signals:
	void logNameChanged();
    void fileNameChanged();
    void lineDelimiterChanged();

public slots:


};

#endif // LOGBASE_H
