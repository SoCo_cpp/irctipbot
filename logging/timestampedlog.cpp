#include "logging/timestampedlog.h"
#include <QDateTime>

TimestampedLog::TimestampedLog(QObject *parent /*= 0*/, const QString& sLogName /*= "TimestampedLog"*/, const QString& sFileName /*= ""*/, const QString& sLineDelimiter /*= "\r\n"*/, const QString& sTimestampFormat /*= ""*/) :
	LogBase(parent, sLogName, sFileName, sLineDelimiter),
	_timestampFormat(sTimestampFormat)
{
}

void TimestampedLog::setTimestampFormat(const QString& sFormat)
{
	_timestampFormat = sFormat;
	emit timestampFormatChanged();
}

const QString& TimestampedLog::timestampFormat() const
{
	return _timestampFormat;
}

QString TimestampedLog::formattedTimestamp() const
{
	if (timestampFormat().isEmpty())
		return QDateTime::currentDateTime().toString(Qt::ISODate);
	else
		return QDateTime::currentDateTime().toString(timestampFormat());
}
