#ifndef DEBUGLOGGER_H
#define DEBUGLOGGER_H

#include <QObject>
#include <QString>
#include "logging/timestampedlog.h"

class DebugLogger;

DebugLogger& DebugLogInstance();
void DebugLog(int level, const char* strFmt, ...);
void DebugLog(int level, const QString& sMsg);

// Convinience DebugLevel macros
#define dlNone	DebugLogger::None
#define dlError	DebugLogger::Error
#define dlWarn	DebugLogger::Warn
#define dlInfo	DebugLogger::Info
#define dlDebug	DebugLogger::Debug
#define dlAll	DebugLogger::All

// utility debug functions

char BoolYN(bool b);
char IsNullYN(const void* p);
const char* CStrOrNull(const char* str);


class DebugLogger : public TimestampedLog
{
	Q_OBJECT
public:
	typedef enum {None = 0, Error = 10, Warn = 20, Info = 30, Debug = 40, All = 100} DebugLogLevel;

	explicit DebugLogger(QObject *parent = 0, const QString& sLogName = "DebugLog", const QString& sFileName = "", const QString& sLineDelimiter = "\r\n", const QString& sTimestampFormat = "");

	static DebugLogger& instance();

	void setMaxLevel(int level);
	int maxLevel() const;
	bool isIncludedLeve(int level) const;

	void log(int level, const char* strFmt, ...);
	void log(int level, const char* strFmt, va_list arglist);
	void log(int level, const QString& sMsg);

protected:
	int _maxLevel;

	QString stampMessage(int level, const QString& sMsg);

signals:
	void maxLevelChanged();
	void DebugLogSignal(QString sMsg);

public slots:
	void DebugLogSlot(QString sMsg);

};

#endif // DEBUGLOGGER_H
