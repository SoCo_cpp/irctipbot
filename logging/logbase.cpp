#include "logging/logbase.h"
#include <QFile>
#include <QTextStream>
#include <QDir>

LogBase::LogBase(QObject *parent /*= 0*/, const QString& sLogName /*= "Unnamed"*/, const QString& sFileName /*= ""*/, const QString& sLineDelimiter /*= "\r\n"*/) :
    QObject(parent),
	_logName(sLogName),
    _lineDelimiter(sLineDelimiter)
{
	setFileName(sFileName); // use set function to clean up file name
}

void LogBase::setLogName(const QString& sFileName)
{
	_logName = sFileName;
	emit logNameChanged();
}

const QString& LogBase::logName() const
{
	return _logName;
}

const QString& LogBase::fileName() const
{
	return _fileName;
}

QString LogBase::filePath() const
{
	return QFileInfo(fileName()).absolutePath();
}

void LogBase::setFileName(const QString& sFileName)
{
	QString sTempFileName = sFileName; // make copy to accept const
	if (sTempFileName.startsWith ("~/"))
		   sTempFileName.replace (0, 1, QDir::homePath());
	QDir dirFileName(QDir::cleanPath(sTempFileName));
	_fileName = dirFileName.absolutePath();
    emit fileNameChanged();
}

bool LogBase::isFileNameSet() const
{
	return !_fileName.isEmpty();
}

bool LogBase::fileExists() const
{
	return isFileNameSet() && QFile::exists(fileName());
}

bool LogBase::filePathExists() const
{
	return isFileNameSet() && QDir("/").exists(filePath());
}

bool LogBase::makePath(bool failIfExists /*= false*/) const
{
	if (failIfExists && filePathExists())
		return false; // path exists
	return QDir("/").mkpath(filePath()); // QDir::mkpath returns true if exists
}

void LogBase::setLineDelimiter(const QString& sLineDelimiter)
{
    _lineDelimiter = sLineDelimiter;
    emit lineDelimiterChanged();
}

const QString& LogBase::lineDelimiter() const
{
    return _lineDelimiter;
}

void LogBase::consoleWarn(const char* strFmt, ...) const
{
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = sMsg.vsprintf(strFmt, arglist);
	va_end(arglist);
	consoleWarn(sMsg);
}

void LogBase::consoleWarn(const QString& sMsg) const
{
	qWarning("%s: %s", qPrintable(logName()), qPrintable(sMsg)); // do this for security since there are no args
}

bool LogBase::outputRaw(const QString& sRaw) const
{
	if (!isFileNameSet())
	{
		consoleWarn("LogBase file name not set, fail");
		return false;
	}
	QFile file(fileName());
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
	{
		consoleWarn("LogBase open failed: %s", qPrintable(fileName()));
		return false;
	}
	QTextStream outstream(&file);
    outstream << sRaw;
    if (outstream.status() != QTextStream::Ok)
	{
		consoleWarn("LogBase write failed");
        return false;
	}
	return true;
}

bool LogBase::outputRawLine(const QString& sLine) const
{
    QString delimitedLine = sLine + lineDelimiter();
    return outputRaw(delimitedLine);
}
