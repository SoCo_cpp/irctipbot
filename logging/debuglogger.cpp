#include "logging/debuglogger.h"

DebugLogger& DebugLogInstance()
{
	return DebugLogger::instance();
}

void DebugLog(int level, const char* strFmt, ...)
{
	va_list arglist;
	va_start(arglist, strFmt);
	DebugLogInstance().log(level, strFmt, arglist);
	va_end(arglist);
}

void DebugLog(int level, const QString& sMsg)
{
	DebugLogInstance().log(level, sMsg);
}

char BoolYN(bool b)
{
	return (b ? 'Y' : 'N');
}

char IsNullYN(const void* p)
{
	return (p == NULL ? 'Y' : 'N');
}

const char* CStrOrNull(const char* str)
{
	return (str == NULL ? "<null>" : str);
}


DebugLogger::DebugLogger(QObject *parent /*= 0*/, const QString& sLogName /*= "DebugLog"*/, const QString& sFileName /*= ""*/, const QString& sLineDelimiter /*= "\r\n"*/, const QString& sTimestampFormat /*= ""*/) :
	TimestampedLog(parent, sLogName, sFileName, sLineDelimiter, sTimestampFormat),
	_maxLevel(DebugLogger::All)
{
	connect(this, SIGNAL(DebugLogSignal(QString)), this, SLOT(DebugLogSlot(QString)), Qt::QueuedConnection);
}

/*static*/DebugLogger& DebugLogger::instance()
{
	static DebugLogger instance;
	return instance;
}

void DebugLogger::setMaxLevel(int level)
{
	_maxLevel = level;
	emit maxLevelChanged();
}

int DebugLogger::maxLevel() const
{
	return _maxLevel;
}

bool DebugLogger::isIncludedLeve(int level) const
{
	return (level <= maxLevel());
}

void DebugLogger::log(int level, const char* strFmt, ...)
{
	if (!isIncludedLeve(level))
		return;
	QString sMsg;
	va_list arglist;
	va_start(arglist, strFmt);
	sMsg = QString().vsprintf(strFmt, arglist);
	va_end(arglist);
	emit DebugLogSignal(stampMessage(level, sMsg));
}

void DebugLogger::log(int level, const char* strFmt, va_list arglist)
{
	if (!isIncludedLeve(level))
		return;
	emit DebugLogSignal(stampMessage(level, QString().vsprintf(strFmt, arglist)));
}

void DebugLogger::log(int level, const QString& sMsg)
{
	if (!isIncludedLeve(level))
		return;
	emit DebugLogSignal(stampMessage(level, sMsg));
}

QString DebugLogger::stampMessage(int level, const QString& sMsg)
{
	QString sStamped = formattedTimestamp();
	switch (level)
	{
		case None:	sStamped += " [None]> ";		break;
		case Error:	sStamped += " [Error]> ";	break;
		case Warn:	sStamped += " [Warn]> ";		break;
		case Info:	sStamped += " [Info]> ";		break;
		case Debug:	sStamped += " [Debug]> ";	break;
		case All:	sStamped += " [All]> ";		break;
		default:
			sStamped += " [Level ";
			sStamped += level;
			sStamped += "]> ";
			break;
	} // witch (level)
	sStamped += sMsg;
	return sStamped;
}

void DebugLogger::DebugLogSlot(QString sMsg)
{
	outputRawLine(sMsg); // ignore failure, not much to do, log name will be reported
}
