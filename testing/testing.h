#ifndef TESTING_H
#define TESTING_H

#include <QObject>
#include "database/sqldatabase.h"
#include "wallet/coinwalletclient.h"
#include "database/sqldatabase.h"
#include "irc/ircclient.h"
#include "ircbots/irctipbot/irctipbot.h"

class Testing : public QObject
{
	Q_OBJECT
public:
	explicit Testing(QObject *parent = 0);

	void startTest(IrcTipBot* pTipBot);
	void testIrc();
	void testDatabase();
	void testParsing();
	void testParsingPacket(const QString& _packet);
	void testWallet();
	void testTipbot();
	void testWalletManager();

protected:
	int _step;
	SqlDatabase _database;
	CoinWalletClient _wallet;
	IrcClient _ircClient;
	IrcTipBot* _pTipbot;

signals:

public slots:
	void testTimerTick();
	void testIrcTimerTick();
	void testDatabaseTimerTick();
	void walletReplyReady(QString sMethod, QJsonValue jsonValue, QString sErrorString);
	void testTipbotTimerTick();
	void testWalletManagerReadyWaitTick();
};

#endif // TESTING_H
