#include "testing/testing.h"
#include "logging/debuglogger.h"
#include "wallet/coinwalletapi.h"
#include <QJsonDocument>
#include "networking/httpjsonclientreply.h"
#include <QSqlQuery>
#include <QSqlError>
#include "irc/ircpacket.h"

Testing::Testing(QObject *parent) :
	QObject(parent),
	_step(0)
{
}

void Testing::startTest(IrcTipBot* pTipBot)
{
	Q_ASSERT(pTipBot);
	_pTipbot = pTipBot;
	QTimer::singleShot(2000, this, SLOT(testTimerTick()));

}

void Testing::testTimerTick()
{

	DebugLog(dlNone, "Starting test");

	//if(!_pTipbot->loadConfig("/home/soco/.irctipbot/irctipbot.config", false/*bRequireAllFields*/))
	//	DebugLog(dlNone, "Testing::testTimerTick tibBot loadConfig failed");
	//else
	//	DebugLog(dlNone, "TipBot config loaded");
	//testParsing();
	//testIrc();
	//testDatabase();
	//testWallet();
	testWalletManager();
	//testTipbot();

	//_pTipbot->defaultConfig();
	//if(!_pTipbot->saveConfig("/home/soco/.irctipbot/irctipbot.config"))
	//	DebugLog(dlNone, "Testing::testTimerTick tibBot saveConfig failed");
	//else
	//	DebugLog(dlNone, "TipBot config saved");



	DebugLog(dlNone, "main test dropped out");

}
void Testing::testIrc()
{
	if (!_pTipbot->irc().connectClient())
		DebugLog(dlError, "TipBot IRC connect client failed");
	else
	{
		DebugLog(dlError, "TipBot IRC connect client connecting");
		QTimer::singleShot(500, this, SLOT(testIrcTimerTick()));
	}
}

void Testing::testIrcTimerTick()
{
	//DebugLog(dlDebug, "IrcClient status: (%d) %s, Login status: (%d) %s", (int)_pTipbot->irc().clientStatus(), qPrintable(_pTipbot->irc().clientStatusString()), (int)_pTipbot->irc().loginStatus(), qPrintable(_pTipbot->irc().loginStatusString()));
	qDebug("IrcClient status: (%d) %s, Login status: (%d) %s", (int)_pTipbot->irc().clientStatus(), qPrintable(_pTipbot->irc().clientStatusString()), (int)_pTipbot->irc().loginStatus(), qPrintable(_pTipbot->irc().loginStatusString()));
	QTimer::singleShot(500, this, SLOT(testIrcTimerTick()));
}

void Testing::testDatabase()
{
	DebugLog(dlDebug, "Testing::testDatabase...");
	if (!_pTipbot->database().connectDatabase())
		DebugLog(dlNone, "main test database connect failed");
	else QTimer::singleShot(500, this, SLOT(testDatabaseTimerTick()));
}

void Testing::testDatabaseTimerTick()
{
	DebugLog(dlDebug, "status: %d", (int)_pTipbot->database().connectionStatus());
	qDebug("status: %d", (int)_pTipbot->database().connectionStatus());
	if (_pTipbot->database().connectionStatus() == SqlDatabase::cConnected && _step == 0)
	{
		QSqlQuery qry = _pTipbot->database().query();
		qry.exec("CREATE TABLE IRCTIPBOT.test(a INT, b INT, c INT);");
		DebugLog(dlDebug, "executed reply: %s", qPrintable(qry.lastError().text()));
		_step = 1;
	}
	else QTimer::singleShot(500, this, SLOT(testDatabaseTimerTick()));
}

void Testing::testParsing()
{
	DebugLog(dlDebug, "Testing::testParsing...");

	testParsingPacket(":zelazny.freenode.net CAP * LS :account-notify extended-join identify-msg multi-prefix sasl");
	testParsingPacket(":heathkid!~heathkid@unaffiliated/heathkid PRIVMSG #dogecoin :nice!");
	testParsingPacket(":ChanServ!ChanServ@services. MODE #shibesgonewild +v raedchen");
	testParsingPacket(":zelazny.freenode.net PONG zelazny.freenode.net :LAG1402163280980074");
	testParsingPacket(":zelazny.freenode.net CAP * LS :account-notify extended-join identify-msg multi-prefix sasl");
	testParsingPacket(":zelazny.freenode.net CAP Doge_Soaker2 ACK :sasl");
	testParsingPacket(":zelazny.freenode.net 352 Doge_Soaker2 #dogesoaker-ops2 ~doge_soak gateway/tor-sasl/sococpp/x-48425895 zelazny.freenode.net Doge_Soaker2 H@ :0 Doge Soaker 2");
	testParsingPacket("AUTHENTICATE +");
}

void Testing::testParsingPacket(const QString& _packet)
{
	IrcPacket packet(0, _packet);
	DebugLog(dlDebug, "testParsing testing: %s", qPrintable(packet.packet()));
	DebugLog(dlDebug, "testParsing _Nick: '%s'", qPrintable(packet.nick().toString()));
	DebugLog(dlDebug, "testParsing _prefix: '%s'", qPrintable(packet.prefix().toString()));
	DebugLog(dlDebug, "testParsing _command: '%s'", qPrintable(packet.command().toString()));
	DebugLog(dlDebug, "testParsing _middle: '%s'", qPrintable(packet.middle().toString()));
	DebugLog(dlDebug, "testParsing _params: '%s'", qPrintable(packet.params().toString()));
	DebugLog(dlDebug, "testParsing _trailing: '%s'", qPrintable(packet.trailing().toString()));
}

void Testing::testWallet()
{
	DebugLog(dlDebug, "Testing::testWallet...");
	connect(&_pTipbot->wallet(), SIGNAL(replyReady(QString,QJsonValue,QString)), this, SLOT(walletReplyReady(QString,QJsonValue,QString)));
	//DebugLog(dlDebug, "testWallet Url: %s", qPrintable(_pTipbot->wallet().config().serverConfig().httpUrl().toString()));

	//_pTipbot->wallet().getAccountAddress(0, "");
	//_pTipbot->wallet().validateAddress(0, "DSoCoDwXVGgfAjpm1PQsYeXr5HCvJMnFEj");
	//_pTipbot->wallet().validateAddress(0, "Dinvalid");
	//_pTipbot->wallet().listAccounts(0);
	//_pTipbot->wallet().getInfo(0);
	//_pTipbot->wallet().getBalance(0); // not working needs address param?
	//_pTipbot->wallet().getDifficulty(0);
	//_pTipbot->wallet().listReceivedByAccount(0);
	//_pTipbot->wallet().listReceivedByAddress(0); // not working replies: Not Found
	//_pTipbot->wallet().listTransactions(0, QString(), 30);
	//_pTipbot->wallet().dumpPrivKey(0, "");
}

void Testing::walletReplyReady(QString sMethod, QJsonValue jsonValue, QString sErrorString)
{
	if (!sErrorString.isNull())
		DebugLog(dlNone, "Testing::walletReplyReady error: %s", qPrintable(sErrorString));
	else
	{
		DebugLog(dlNone, "Testing::walletReplyReady success method: %s", qPrintable(sMethod));
		DebugLog(dlNone, "Dumping reply:\n%s", qPrintable(CoinWalletClient::dumpJSONValue("reply", jsonValue)));
	}

}

void Testing::testTipbot()
{
	_pTipbot->start();
	QTimer::singleShot(1000, this, SLOT(testTipbotTimerTick()));
}

void Testing::testTipbotTimerTick()
{
	DebugLog(dlDebug, "Tipbot mode: %s, status: %s, con: %s", qPrintable(_pTipbot->botModeString()), qPrintable(_pTipbot->botStatusString()), qPrintable(_pTipbot->connectionStatusString()));
	qDebug("Tipbot mode: %s, status: %s, con: %s", qPrintable(_pTipbot->botModeString()), qPrintable(_pTipbot->botStatusString()), qPrintable(_pTipbot->connectionStatusString()));
	QTimer::singleShot(1000, this, SLOT(testTipbotTimerTick()));
}

void Testing::testWalletManager()
{
	QTimer::singleShot(1000, this, SLOT(testWalletManagerReadyWaitTick()));
}

void Testing::testWalletManagerReadyWaitTick()
{
	/*
	DebugLog(dlDebug, "Tipbot mode: %s, status: %s, con: %s", qPrintable(_pTipbot->botModeString()), qPrintable(_pTipbot->botStatusString()), qPrintable(_pTipbot->connectionStatusString()));
	qDebug("Tipbot mode: %s, status: %s, con: %s", qPrintable(_pTipbot->botModeString()), qPrintable(_pTipbot->botStatusString()), qPrintable(_pTipbot->connectionStatusString()));
	if (_pTipbot->botStatus() == IrcTipBot::bReady)
	{
		if (!_pTipbot->ircHandler().createWallet("testAccount", "testRole", "testUser"))
		{
			DebugLog(dlNone, "Testing::testWalletManagerReadyWaitTick  walletManager createWallet failed");
		}
		else DebugLog(dlNone, "Testing::testWalletManagerReadyWaitTick  walletManager createWallet success");

	}
	else QTimer::singleShot(1000, this, SLOT(testWalletManagerReadyWaitTick()));
	*/
}
